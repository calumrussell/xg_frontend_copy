const next = require('next')
const http = require("http")
const fs = require("fs")
const express = require('express')

const routes = require('./routes')
const app = next({ dev: process.env.NODE_ENV !== 'production' })
const handler = routes.getRequestHandler(app)

app.prepare()
  .then(() => {
    const server = express()

    server.get('/robots.txt', (req, res)=> {
      res.type('text/plain')
      const test = fs.readFileSync('./config/robots.txt')
      res.send(test)
    })

    server.use(handler)
    http.createServer(server)
      .listen(3000, err => console.log(err))
  }).catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })
