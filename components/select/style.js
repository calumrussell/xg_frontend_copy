import styled from 'styled-components'

import { colorScheme } from '@Theme'

export const SelectStyle = styled.select`
  border-color: ${colorScheme.grey[500]};
  &:active{
    border-color: ${colorScheme.grey[500]};
  }
  &:focus{
    border-color: ${colorScheme.grey[500]};
  }
  &:not(.is - multiple):: after{
    border: 1px solid ${colorScheme.grey[500]};
    border-right: 0;
    border-top: 0;
  }
`

export const SelectAltStyle = styled.select`
  color: ${colorScheme.grey[800]};
  width: 100px;
  padding: 0.5rem;
  font-size: 0.8rem;
  border: none;
  border-radius: 5px;
  background: transparent;
  background-repeat: no-repeat;
  background-position-x: 100%;
  background-position-y: 50%;
  background-image: url("data:image/svg+xml;utf8,<svg fill='orange' height='24' viewBox='0 0 24 24' width='24' xmlns='http://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
  -webkit-appearance: none;
  &:focus{
    outline: none;
  }
`

export const SelectSimpleStyle = styled.select`
  background:transparent;
  outline: none;
  border: none;
  border-bottom: 1px solid ${colorScheme.bluegrey[700]};
  padding: 0.5rem;
`
