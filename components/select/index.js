import React from 'react'
import uniqueId from "lodash.uniqueid"

import { SmallText } from "@Components/text"
import {
  SelectStyle,
  SelectAltStyle,
  SelectSimpleStyle
} from "./style.js"

export const Select = ({ values, selected, onChange, firstnull, name, label }) => {
  return (
    <div>
      <label>{label}</label>
      <SelectStyle name={name} value={selected} onChange={onChange}>
        {
          firstnull ?
            <option value=""></option> :
            null
        }
        {
          values.map((v, i) => <option value={v} key={i}>{v}</option>)
        }
      </SelectStyle>
    </div>
  )
}

export const SelectAlt = ({ values, selected, onChange, firstnull, title, name, disabled = false }) => {
  return (
    <SelectAltStyle name={name} value={selected} onChange={onChange} disabled={disabled}>
      {
        firstnull ?
          <option value=""></option> :
          null
      }
      {
        values.map((v, i) => <option value={v} key={i}>{v}</option>)
      }
    </SelectAltStyle>
  )
}

export const SelectSimple = ({ values, selected, onChange, firstnull, name, label }) => {
  const id = uniqueId("prefix-")
  return (
    <div>
      <label htmlFor={id}><SmallText>{label}</SmallText></label>
      <SelectSimpleStyle id={id} name={name} value={selected} onChange={onChange}>
        {
          firstnull ?
            <option value=""></option> :
            null
        }
        {
          values.map((v, i) => <option value={v} key={i}>{v}</option>)
        }
      </SelectSimpleStyle>
    </div>
  )
}

export const LastNSelect = ({
  options = [5, 10, 15],
  selected,
  changeFunc }) => {
  return (
    <SelectAlt
      values={options}
      onChange={changeFunc}
      selected={selected}
      title={"Period"}
      name={"lastN"} />
  )
}

export const VenueSelect = ({
  options = ['All', 'Home', 'Away'],
  selected,
  changeFunc }) => {
  return (
    <SelectAlt
      values={options}
      onChange={changeFunc}
      selected={selected}
      title={"Venue"}
      name={"isHome"} />
  )
}

export const PositionSelect = ({
  options = ['Goalkeeper', 'Defender', 'Midfielder', 'Striker'],
  selected,
  changeFunc }) => {
  return (
    <SelectAlt
      values={options}
      onChange={changeFunc}
      selected={selected}
      title={"Position"}
      name={"position"} />
  )
}

export const StatGroupSelect = ({
  options = ['Off', 'Def', 'Passes', 'Touches', 'Shots'],
  selected,
  changeFunc }) => {
  return (
    <SelectAlt
      values={options}
      onChange={changeFunc}
      selected={selected}
      title={"Stat Category"}
      name={"statGroup"} />
  )
}
