import styled from 'styled-components'
import { colorScheme } from '@Theme'

export const MainWrapper = styled.div`
  display: block;
  margin-top: 1rem;
`

export const SpecialMessage = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  font-size: 1.25rem;
  padding: 0.5rem 0;
  margin-bottom: 0.5rem;
  border-bottom: 1px solid ${colorScheme.grey[300]};
`
