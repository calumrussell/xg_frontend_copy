import React from "react"

import { NavBar } from '@Components/nav'
import { Footer } from '@Components/footer'
import { Message } from '@Components/message'
import { SectionTitle } from '@Components/text'
import { MainWrapper, SpecialMessage } from './style.js'

export const ViewWrapper = ({ children }) => {
  const siteWideMessage = null

  return (
    <div>
      <NavBar />
      <MainWrapper>
        {siteWideMessage == null || <SpecialMessage><SectionTitle>{siteWideMessage}</SectionTitle></SpecialMessage>}
        <Message />
        <div>
          {children}
        </div>
      </MainWrapper>
      <Footer />
    </div>
  )
}
