import styled from 'styled-components'

import { mq } from "@Theme"

export const LeaguesWrapper = styled.div`
  display: flex;
  overflow-x: hidden;
  &&:hover{
    overflow-x: auto;
  }
  ${mq[0]}{
    &&{
      overflow-x:scroll;
    }
  }
`

export const RegionsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  div {
    margin-right: 0.5rem;
  }
`

export const RegionSelector = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  header{
    white-space: nowrap;
    font-weight: ${props => props.isActive ? 'bold' : 'normal'};
  }
`
