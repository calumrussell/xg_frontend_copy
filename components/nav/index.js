import React from 'react';
import { connect } from 'react-redux'
import { Link, Router } from '@Routes'

import { toggleNav } from '@Redux/actions/Nav'
import { logoutUser } from '@Redux/actions/User'
import { updateTourns } from '@Redux/actions/User'
import { tokenParser } from '@Services/token'

import { Button } from '@Components/button'
import { SettingsIcon } from '@Components/icon'
import { SubTitle } from '@Components/text'
import { NavItem, NavWrap, NavLinks, NavButton, MenuItem } from '@Components/common'

import {
  RegionsWrapper,
  LeaguesWrapper,
  RegionSelector
} from './style.js'

class Nav extends React.Component {

  state = {
    active: false,
    region: null
  }

  componentDidMount = async () => {
    if (this.props.currentTourns.length == 0) {
      const token = await tokenParser()
      this.props.refreshTourns(token)
    }
  }

  handleClick = (event) => {
    let curr = this.state.active
    this.setState({ active: !curr })
  }

  handleLogOut = async (event) => {
    event.preventDefault()
    const token = await tokenParser(undefined)
    this.props.userLogOut(token)
    Router.pushRoute('/')
  }

  selectRegion = region => {
    if (region == this.state.region) {
      this.setState({ region: null })
    } else {
      this.setState({ region })
    }
  }

  renderLeagues = () => {
    if (this.props.currentTourns.length < 4) {
      return this.props.currentTourns.map((l, i) => <MenuItem key={i} {...l} />)
    } else {
      if (this.state.region != null) {
        const filteredLeagues = this.props.currentTourns.filter(v => v.regionname == this.state.region)
        return filteredLeagues.map((l, i) => <MenuItem key={i} {...l} />)
      } else {
        return this.props.liveTourns.map((l, i) => <MenuItem key={i} {...l} />)
      }
    }
  }

  renderRegions = () => {
    if (this.props.currentTourns.length > 3) {
      const unique = this.props.currentTourns.map(v => v.regionname)
        .filter((v, i, self) => self.indexOf(v) == i)
      return unique.map((v, i) => (
        <RegionSelector
          key={i}
          onClick={() => this.selectRegion(v)}
          isActive={v == this.state.region}>
          <SubTitle>{v}</SubTitle>
        </RegionSelector>
      ))
    } else {
      return null
    }
  }

  render() {
    const { active } = this.state
    const { isLoggedIn, isSubscribed } = this.props

    return (
      <NavWrap>
        <NavLinks active={active}>
          <NavItem onClick={this.handleClick}><Link route={'/'}><a>Home</a></Link></NavItem>
          {
            isLoggedIn ?
              <NavItem onClick={this.handleLogOut}><a>Log Out</a></NavItem> :
              <NavItem onClick={this.handleClick}><Link route={'/login'}><a>Login</a></Link></NavItem>
          }
          <NavItem onClick={this.handleClick}><Link route={'/settings'}><SettingsIcon /></Link></NavItem>
        </NavLinks>
        <RegionsWrapper>{this.renderRegions()}</RegionsWrapper>
        <LeaguesWrapper>{this.renderLeagues()}</LeaguesWrapper>
      </NavWrap>
    )
  }
}

const mapStateToProps = state => ({
  liveTourns: state.user.liveTourns,
  currentTourns: state.user.currentTourns,
  isOpen: state.nav.isOpen,
  isLoggedIn: state.user.isLoggedIn,
})

const mapDispatchToProps = dispatch => {
  return {
    refreshTourns: token => dispatch(updateTourns(token)),
    toggleNav: () => dispatch(toggleNav()),
    userLogOut: token => dispatch(logoutUser(token))
  }
}

export const NavBar = connect(mapStateToProps, mapDispatchToProps)(Nav)
