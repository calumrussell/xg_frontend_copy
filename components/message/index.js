import React from "react"
import { connect } from "react-redux"

import { MessageWrap } from './style.js'

const InnerMessage = ({ text, mood }) => text != null ? <MessageWrap mood={mood}>{text}</MessageWrap> : null

const mapStateToProps = (state) => {
  return ({ ...state.message })
}

export const Message = connect(mapStateToProps)(InnerMessage)
