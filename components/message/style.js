import styled from "styled-components"

import { colorScheme } from '@Theme'
import { DefaultText } from '@Components/text'

export const MessageWrap = styled(DefaultText)`
  background-color: ${({ mood }) => mood == 1 ? colorScheme.success : mood == -1 ? colorScheme.failure : colorScheme.neutral};
  width:100%;
  text-align: center;
  padding: 5px;
  color: white;
`
