import React from 'react';

import { SubTitle } from "@Components/text"
import { MatchResult } from "@Components/match"
import { MatchesWrapper, LeagueHeader } from "./style.js"

const Matches = ({ league, matches }) => {
  return (
    <div>
      {league == undefined || <LeagueHeader><SubTitle>{league}</SubTitle></LeagueHeader>}
      <MatchesWrapper>
        {
          matches.map((match, j) => <MatchResult {...match} key={j} />)
        }
      </MatchesWrapper>
    </div>
  )
}

export const RecentMatchesByDate = ({ matches }) => <Matches matches={matches} />

export const RecentMatchesByLeague = ({ matches }) => {
  return (
    <React.Fragment>
      {
        Object.keys(matches).map((league, i) => (
          <Matches key={i}
            league={league}
            matches={matches[league]} />
        ))
      }
    </React.Fragment>
  )
}
