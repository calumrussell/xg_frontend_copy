import styled from 'styled-components'

import { colorScheme, mq } from '@Theme'

export const MatchesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  ${mq[1]}{
    flex-wrap: nowrap;
    flex-direction: row;
    overflow-x: auto;
  }
`

export const LeagueHeader = styled.div`
  padding: 0.5rem;
  border-bottom: 1px solid ${colorScheme.grey[200]};
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin:0px;
`
