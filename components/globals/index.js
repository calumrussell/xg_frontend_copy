import styled from "styled-components"
import { colorScheme, mq, textFont } from '@Theme'

export const BaseContainer = styled.div`
  padding: 0.5rem 0rem;
  font-family: ${textFont};
  color: ${colorScheme.grey[800]};
  font-size: 0.85rem;
  .simple-table-main-table-wrap{
    overflow-x: hidden;
    &&:hover{
      overflow-x: auto;
    }
    ${mq[1]}{
      overflow-x: auto;
    }
  }

  table tr th{
    font-size: 0.8rem;
    letter-spacing: 0.25px;
    border-bottom: 1px solid ${colorScheme.grey[400]};
    font-weight:normal;
  }

  table td, th{
    text-align:right;
  }

  tbody td p{
    margin:0;
    font-size: 0.9rem;
  }

  table td a{
    font-size: 0.9rem;
    text-decoration: none;
  }

  table td:nth-child(1){
    text-align: left;
    padding-left:10px;
    white-space: nowrap;
  }
}
`
export const TableContainer = styled(BaseContainer)`
  .simple-table-main-table{
    min-width: 600px;
  }
  table{
    margin: 0.25rem 0rem;
    td {
      padding: 0.25rem;
    }
  }
`
