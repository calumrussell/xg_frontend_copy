import React from "react"
import { select } from 'd3-selection'
import groupBy from 'lodash.groupby'

import {
  getPositions,
  getLocationX,
  getLocationY,
  textBoxBuilder,
  highlightLine,
  reverseHighlightLine
} from './helpers'
import { titleFont, colorScheme } from '@Theme'

import { PitchWrapper } from './style.js'

class HalfPitchWithShots extends React.Component {

  clearPitch = () => {
    selectAll(".latestseasonshots-shot").remove()
  }

  pitchInit = () => {
    const pitch = select(".pitch")

    const handleMouseOver = function (d, i) {
      textBoxBuilder(pitch, d)
      highlightLine(d, this)
    }

    const handleMouseOut = function (d, i) {
      select("#latestseasonshots-shot-detail-box").remove()
      reverseHighlightLine(d, this)
    }

    const shots = pitch.selectAll("g")
      .data(this.props.data, d => {
        return d
      })
      .enter()
      .append("g")
      .attr('class', 'latestseasonshots-shot')
      .on('mouseover', handleMouseOver)
      .on('mouseout', handleMouseOut)

    shots.selectAll("rect")
      .data(d => [d])
      .enter()
      .filter(d => d.goal === 1)
      .append("rect")
      .attr("x", d => getLocationX(d) - 4)
      .attr("y", d => getLocationY(d) - 3)
      .attr("fill", "green")
      .attr("width", 7)
      .attr("height", 7)

    shots.selectAll("circle")
      .data(d => [d])
      .enter()
      .filter(d => d.goal != 1)
      .append("circle")
      .attr('cx', d => getLocationX(d) - 4)
      .attr('cy', d => getLocationY(d) - 3)
      .attr("r", 5)
      .style("fill", "blue")

    shots.selectAll("line")
      .data(d => [d])
      .enter()
      .filter(d => d.goalmouthy != -1.0)
      .append("line")
      .style("stroke", `${colorScheme.grey[50]}`)
      .style("stroke-width", "0.5")
      .style("stroke-opacity", '0.2')
      .attr("x1", d => getLocationX(d))
      .attr("y1", d => getLocationY(d))
      .attr("y2", 18)
      .attr("x2", d => getLocationX({ y: d['goalmouthy'] }))
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.filters.length != prevProps.filters.length) {
      this.clearPitch()
      this.pitchInit()
    }
  }

  componentDidMount = () => {
    this.pitchInit()
  }

  render() {
    return (
      <div>
        <button onClick={this.clearPitch}>Clear</button>
        <HalfPitch ref={node => this.node = node} />
      </div>
    )
  }
}

class PitchWithPlayers extends React.Component {

  addSide = (pitch, isHome, positions) => {
    const side = isHome ? 'home' : 'away'
    const position = pitch.selectAll(".position")
      .data(positions)

    const circleEnter = position.enter()
      .append("g")
      .attr("class", `${side}Player`)

    circleEnter.append("circle")
      .attr("cx", d => d[0])
      .attr("cy", d => d[1])
      .attr("r", 10)
      .style("fill", isHome ? "blue" : "red")

    circleEnter.append("text")
      .attr("dx", d => d[0])
      .attr("dy", d => d[1] + 30)
      .text(d => {
        return d[3].includes(" ")
          ? d[3].split(" ").length === 3
            ? d[3].split(" ")[1] + " " + d[3].split(" ")[2]
            : d[3].split(" ")[1]
          : d[3]
      })
      .style("fill", "black")
      .style("stroke", "none")
      .style("text-anchor", "middle")
  }

  pitchInit = (homePositions, awayPositions) => {
    const pitch = select(".pitch").select("g")
    this.addSide(pitch, true, homePositions)
    this.addSide(pitch, false, awayPositions)
  }

  componentDidMount = () => {
    const groupByHome = groupBy(this.props.players, 'ishome')
    const homePositions = getPositions(groupByHome[1], 452, 683, true)
    const awayPositions = getPositions(groupByHome[0], 452, 684, false)
    this.pitchInit(homePositions, awayPositions)
  }

  render = () => {
    return <Pitch ref={node => this.node = node} />
  }
}

class Pitch extends React.Component {

  pitchInit = () => {
    const pitch = select(this.node)
      .select(".pitch")
  }

  componentDidMount = () => {
    this.pitchInit()
  }

  render() {
    const width = 400
    const height = width * 1.525

    return (
      <PitchWrapper>
        <svg ref={node => this.node = node}
          className="pitch-svg"
          width={width + 100}
          height={height + 100}
          viewBox={`0 0 ${width + 100} ${height + 100}`}
          preserveAspectRatio="xMidYMid meet">

          <g>
            <svg className="pitch" xmlns="http://www.w3.org/2000/svg" height="684" width="452">
              <rect opacity=".7" height="684" width="452" fill="#0b0" ry="6" />
              <g stroke="#efe" strokeWidth="3" fill="none">
                <path d="m11.22 22.62v638.8h429.6v-638.8z" />
                <path d="m11.26 342h429.4" />

                <circle cy="342" cx="226" r="54.8" />
                <circle cy="342" cx="226" r="2" />
                <g id="a">
                  <path d="m9.9 30.07c4.85 0 8.82-4 8.82-8.9m162.5 100.8a54.91 54.91 0 0 0 89.6 0m76.3-99.63v99.63h-242.2l.2-99.63m98.6.20v-15.6l44.6.003v15.6m-77.9-.20v34.4h111.2v-34.4m160.5 7.7c-4.9 0-8.8-4-8.8-8.9" />
                  <circle cy="90" cx="226" r="2" />
                </g>
                <use href="#a" transform="scale(1,-1)" y="-684" />
              </g>
            </svg>
          </g>

        </svg>
      </PitchWrapper>
    )
  }
}

class HalfPitch extends React.Component {

  pitchInit = () => {
    const pitch = select(this.node)
      .select(".pitch")
  }

  componentDidMount = () => {
    this.pitchInit()
  }

  render() {
    const width = 460
    const height = 380

    return (
      <PitchWrapper>
        <svg ref={node => this.node = node}
          className="test"
          width={width}
          height={height}
          viewBox={`0 0 ${width} ${height}`}
          preserveAspectRatio="xMidYMid meet">

          <g>
            <svg className="pitch" xmlns="http://www.w3.org/2000/svg" height="400" width="460">
              <rect opacity=".7" height="340" width="452" fill="#0b0" ry="6" />
              <g stroke="#efe" strokeWidth="3" fill="none">
                <path d="m11.22 22.62v300.8h429.6v-300.8z" />
                <path d="m11.26 342h429.4" />
                <g id="a">
                  <path d="m9.9 30.07c4.85 0 8.82-4 8.82-8.9m162.5 100.8a54.91 54.91 0 0 0 89.6 0m76.3-99.63v99.63h-242.2l.2-99.63m98.6.20v-15.6l44.6.003v15.6m-77.9-.20v34.4h111.2v-34.4m160.5 7.7c-4.9 0-8.8-4-8.8-8.9" />
                  <circle cy="90" cx="226" r="2" />
                </g>
              </g>
            </svg>
          </g>

        </svg>
      </PitchWrapper>
    )
  }
}

export { 
  Pitch,
  PitchWithPlayers,
  HalfPitch,
  HalfPitchWithShots
}
