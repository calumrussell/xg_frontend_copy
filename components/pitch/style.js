import styled from "styled-components"

import { titleFont } from "@Theme"

export const PitchWrapper = styled.div`
  text-align: center;
  margin: 20px 5px;
  overflow-y: hidden;
  font-family: ${titleFont};
`

