import { select } from 'd3-selection'

import { luSort, luSortRight } from '@Functions/sort'
import { toFixedAny } from '@Functions/transform'
import { titleFont, colorScheme } from "@Theme"

const isGK = position => position === 'GK'
const isDefence = position => ['DC', 'DL', 'DR'].includes(position)
const isMidfieldDef = position => ['DML', 'DMR', 'DMC'].includes(position)
const isMidfield = position => ['MC', 'ML', 'MR'].includes(position)
const isMidfieldAtt = position => ['AMC', 'AML', 'AMR'].includes(position)
const isForward = position => ['CF'].includes(position)

const getY = (position, height, isHome) => {
  const gkPosition = height * 0.058
  const defencePosition = height * 0.175
  const midfieldDefPosition = height * 0.263
  const midfieldPosition = height * 0.322
  const midfieldAttPosition = height * 0.38
  const forwardPosition = height * 0.44

  if (isGK(position)) return isHome ? gkPosition : height - gkPosition
  if (isDefence(position)) return isHome ? defencePosition : height - defencePosition
  if (isMidfieldDef(position)) return isHome ? midfieldDefPosition : height - midfieldDefPosition
  if (isMidfield(position)) return isHome ? midfieldPosition : height - midfieldPosition
  if (isMidfieldAtt(position)) return isHome ? midfieldAttPosition : height - midfieldAttPosition
  if (isForward(position)) return isHome ? forwardPosition : height - forwardPosition

}

const getX = (position, groupPosition, groupSize, width) => {
  const minus = width - 90
  const unit = minus / groupSize

  if (groupSize === 1) return width / 2
  if (groupSize === 2) return (minus / 4) * (groupPosition + 2)
  if (groupSize === 3) return width / 2 / 2 * (groupPosition + 1)
  if (groupSize === 4) return (minus / 4) * (groupPosition + 1)
  if (groupSize === 5) return width / 2 / 2 / 2 * (groupPosition + 2)
  return unit * (groupPosition + 1)
}

const getCoordsByPositionGroup = (playerGroup, width, height, isHome) => {
  return playerGroup.map((player, i) => {
    return [getX(player.position, i, playerGroup.length, width),
    getY(player.position, height, isHome), player.position, player.formatname]
  })
}

const groupByPosition = players => {
  const gk = []
  const def = []
  const middef = []
  const mid = []
  const midatt = []
  const fwd = []
  players.forEach(v => {
    if (isGK(v.position)) gk.push(v)
    if (isDefence(v.position)) def.push(v)
    if (isMidfieldDef(v.position)) middef.push(v)
    if (isMidfield(v.position)) mid.push(v)
    if (isMidfieldAtt(v.position)) midatt.push(v)
    if (isForward(v.position)) fwd.push(v)
  })
  return [gk,
    def,
    middef,
    mid,
    midatt,
    fwd]
}

export const getPositions = (players, width, height, isHome) => {
  if (players === undefined || players.length === 0) return []
  const sortedPlayers = isHome ? luSort(players) : luSortRight(players)
  const positionGroup = groupByPosition(sortedPlayers)
  return [].concat.apply([], positionGroup.map(group => getCoordsByPositionGroup(group, width, height, isHome)))
}

//this orientation is opta formatted i.e. reversed
//top- 11, bottom- 441 (y) 
//right- 23, left- 323 (x)
export const getLocationX = ({ y }) => (((100 - y) * 4.27) + 11)
export const getLocationY = ({ x }) => (((100 - x) * 5.76) + 23)

export const textBoxBuilder = (pitch, d) => {
  const features = ['formatname', 'league', 'opp', 'bodypart', 'patternofplay']
  const root = pitch.append("g")
    .attr("id", "latestseasonshots-shot-detail-box")
  const basePosition = 250

  features.map((f, i) => {
    root.append("text")
      .attr("font-family", `${titleFont}`)
      .attr("font-size", "1rem")
      .attr("stroke-width", "0")
      .attr("fill", "red")
      .attr("x", 20)
      .attr("dy", basePosition + (i * 15))
      .text(() => toFixedAny(d[f], 2))
  })
}

export const highlightLine = (d, el) => {
  select(el).select("line")
    .style("stroke-width", '2')
    .style("stroke", `${colorScheme.grey[800]}`)
    .style("stroke-opacity", "1")
}

export const reverseHighlightLine = (d, el) => {
  select(el).select("line")
    .style("stroke-width", "0.5")
    .style("stroke", `${colorScheme.grey[50]}`)
    .style("stroke-opacity", "0.2")
}

