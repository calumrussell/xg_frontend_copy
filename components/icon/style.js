import styled from "styled-components"

import { colorScheme } from "@Theme"

export const SVGWrapper = styled.div`
  height: 30px;
  width: 30px;
  svg{
    fill: ${colorScheme.bluegrey[700]};
    cursor: pointer;
  }
`
