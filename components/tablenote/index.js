import React from "react"
import styled from "styled-components"

import { DefaultText } from '@Components/text'

const TableNoteWrap = styled(DefaultText)`
  font-size:0.75rem;
  margin-top: 2.5px;
  font-style: italic;
`

export const TableNote = ({ children }) => <TableNoteWrap>{children}</TableNoteWrap>
