import React from 'react'
import { render } from 'react-testing-library'

import { Filter } from "./index.js"

describe('define Filters', () => {

  const sampleData = [{ "test1": 2 }, { "test1": 3 }]
  const ComponentToRender = ({ data }) => {
    return <div>{data.map((d, i) => <p key={i} data-testid="render">{d['test1']}</p>)}</div>
  }

  it('filters the first row from the data object', () => {
    const Component = props => {
      return (
        <Filter filters={[v => v['test1'] == 2]} >
          <ComponentToRender data={sampleData} />
        </Filter>
      )
    }
    const { queryByTestId } = render(<Component />)
    expect(queryByTestId('render')).toHaveTextContent("2")
  })

  it('filters nothing with no filters', () => {
    const Component = props => {
      return (
        <Filter filters={[]}>
          <ComponentToRender data={sampleData} />
        </Filter>
      )
    }
    const { queryAllByTestId } = render(<Component />)
    expect(queryAllByTestId('render')).toHaveLength(2)
  })

  it('filters nothing with filter is undefined', () => {
    const Component = props => {
      return (
        <Filter>
          <ComponentToRender data={sampleData} />
        </Filter>
      )
    }
    const { queryAllByTestId } = render(<Component />)
    expect(queryAllByTestId('render')).toHaveLength(2)
  })

  it('filters when a different name is passed', () => {
    const ComponentToRenderDiffProps = ({ data1 }) => {
      return <div>{data1.map((d, i) => <p key={i} data-testid="render">{d['test1']}</p>)}</div>
    }
    const Component = props => {
      return (
        <Filter names={['data1']} filters={[v => v['test1'] == 2]} >
          <ComponentToRenderDiffProps data1={sampleData} />
        </Filter>
      )
    }
    const { queryByTestId } = render(<Component />)
    expect(queryByTestId('render')).toHaveTextContent("2")
  })

  it('filters multipledatasets when passed more than one name', () => {
    const ComponentToRenderDiffProps = ({ data1, data2 }) => {
      return (
        <div>
          {
            data1.map((d, i) => <p key={i} data-testid="render1">{d['test1']}</p>)
          }
          {
            data2.map((d, i) => <p key={i} data-testid="render2">{d['test1']}</p>)
          }
        </div>
      )
    }
    const Component = props => {
      return (
        <Filter names={['data1', 'data2']} filters={[v => v['test1'] == 2]} >
          <ComponentToRenderDiffProps data1={sampleData} data2={sampleData} />
        </Filter>
      )
    }
    const { queryByTestId } = render(<Component />)
    expect(queryByTestId('render1')).toHaveTextContent("2")
    expect(queryByTestId('render2')).toHaveTextContent("2")
  })
})
