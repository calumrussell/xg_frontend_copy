import React from "react"

import { SelectSimple } from "@Components/select"
import { SmallText } from "@Components/text"
import { filterDS } from "@Functions/filter"

import { 
  StyledButton,
  ExistingFilterWrapper,
  FilterWrapper,
  NewFilterWrapper,
  FilterSelectors
} from "./style.js"

const ExistingFilter = ({ stat, cond, condval, removeFilter, id }) => {
  return (
    <ExistingFilterWrapper>
      <StyledButton onClick={() => removeFilter(id)}>Drop</StyledButton>
      <span><SmallText>{stat}</SmallText></span>
      <span><SmallText>{cond}</SmallText></span>
      <span><SmallText>{condval}</SmallText></span>
    </ExistingFilterWrapper>
  )
}

export class FilterCreator extends React.Component {

  /*
    This is formatted to go in a row under ButtonsWrapper
  */

  state = {
    stat: '',
    cond: '',
    condval: '',
  }

  onSelectChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  addFilter = () => {
    const { stat, cond, condval } = this.state
    const id = this.props.existingFilters.length + 100
    const strColumns = ['league', 'opp', 'formatname', 'bodypart', 'patternofplay', 'shotlocation']
    if (strColumns.includes(stat)) {
      this.props.addFilter(filterDS(stat, cond, condval, id, false))
    } else {
      this.props.addFilter(filterDS(stat, cond, condval, id))
    }
    this.setState({ stat: '', cond: '', condval: '' })
  }

  renderFilterSelectors = ({ stat, cond, condval }, { statChoices }) => {
    const statsOptions = statChoices.sort()
    const condOptions = ['>', '<', '=']

    return (
      <FilterSelectors>
        <SelectSimple
          label={'Stat'}
          name={'stat'}
          selected={stat}
          onChange={this.onSelectChange}
          values={statsOptions}
          firstnull={true} />
        <SelectSimple
          label={'Condition'}
          name={'cond'}
          selected={cond}
          onChange={this.onSelectChange}
          values={condOptions}
          firstnull={true} />
        <div>
          <label><SmallText>Value</SmallText>
            <input
              name={'condval'}
              value={condval}
              onChange={this.onSelectChange} />
          </label>
        </div>
      </FilterSelectors>
    )
  }

  renderExisingFilters = existingFilters => {
    const existingFiltersList = existingFilters
      .map((e, i) => (
        <ExistingFilter
          key={e.id}
          {...e}
          removeFilter={this.props.removeFilter} />
      ))

    return !existingFilters && !existingFilters.length > 0 ||
      <React.Fragment>
        <SmallText>Existing Filters:</SmallText>
        {existingFiltersList}
      </React.Fragment>
  }

  render() {

    const {
      existingFilters,
      isOpen
    } = this.props

    return (
      <FilterWrapper>
        {!isOpen ||
          <React.Fragment>
            {this.renderExisingFilters(existingFilters)}
            <div>
              <StyledButton onClick={this.addFilter}>Add Filter</StyledButton>
              {this.renderFilterSelectors(this.state, this.props)}
              <SmallText>Note: Multiple filters run as AND conditions</SmallText>
            </div>
          </React.Fragment>
        }
      </FilterWrapper>
    )
  }
}
