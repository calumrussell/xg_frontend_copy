import styled from "styled-components"

import { Button } from "@Components/button"

export const StyledButton = styled(Button)`
  background: transparent;
  font-size: 0.8rem;
  margin: 0.5rem 0;
`

export const ExistingFilterWrapper = styled.div`
  margin: 0.25rem 0;
  display: flex;
  align-items: center;
  span:nth-child(2){
    padding-left: 0.5rem;
  }
  span{
    padding-right: 0.25rem;
  }
`

export const FilterWrapper = styled.div``

export const NewFilterWrapper = styled.div`
  display: flex;
`

export const FilterSelectors = styled.fieldset`
  border: none;
`
