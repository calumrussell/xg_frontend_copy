import React from "react"
import pick from "lodash.pick"

export const WithFilter = (WrappedComponent, filterFunc) => {
  return class WithFilter extends React.Component {
    render(){
       const { data, ...passThroughProps } = this.props
       const filteredData = data.map(row => filterFunc(row))
       return <WrappedComponent data={filteredData} {...passThroughProps} />
    }
  }
}

export const WithColumns = (Component, columns) => {

  const columnFunc = (data, columns) => {
      return data.map(row => pick(row, columns)
  }

  return class WithColumns extends React.Component {
    render(){
       const { data, ...passThroughProps } = this.props
       const filteredData = columnFunc(data, columns)
       return <Component data={filteredData} {...passThroughProps} />
    }
  }
}
