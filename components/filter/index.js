import React from "react"

/*
  Wraps around a Component that has a data property,
  filters the data according to a list of filters that
  are passed as props.

  Filters operate as OR. When one condition passes then
  that row will return true.
*/

export class Filter extends React.Component {

  applyFilters = data => {
    if (this.props.filters == undefined || this.props.filters == null || this.props.filters.length == 0) return data
    const filterFunc = d => this.props.filters.map(filter => filter(d)).filter(v => v == true).length == this.props.filters.length
    return data.filter(filterFunc)
  }

  render() {

    const dataNames = this.props.names == undefined
      ? ['data']
      : this.props.names

    const appliedFilter = child => dataNames.reduce((p, c) => {
      p[c] = this.applyFilters(child.props[c])
      return p
    }, {})

    return (
      <React.Fragment>
        {
          React.Children.map(this.props.children, child => {
            return React.cloneElement(child, appliedFilter(child))
          })
        }
      </React.Fragment>
    )
  }
}

import { Portal } from "./components/portal"
import { FilterCreator } from "./components/creator"
import { Button } from "@Components/button"

export class FilterButton extends React.Component {

  state = {
    isOpen: false
  }

  toggleIsOpen = event => {
    this.setState(state => ({ isOpen: !state.isOpen }))
  }

  render() {
    //Need FilterCreator outside Wrap
    //it is a portal and if inside the Wrap any click
    //will come back to this Component and toggle isOpen
    const statChoices = this.props.statData.length != undefined
      ? Object.keys(this.props.statData[0]) 
      : Object.keys(this.props.statData)
      
    return (
      <React.Fragment>
        <Button onClick={this.toggleIsOpen}>
          Filter
        </Button>
        <Portal selector={this.props.selector} isOpen={this.state.isOpen}>
          <FilterCreator
            isOpen={this.state.isOpen}
            {...this.props}
            statChoices={statChoices} />
        </Portal>
      </React.Fragment>
    )
  }
}
