import React from "react"
import moment from 'moment'

import { LinksWrapper } from "../style.js"
import { MatchLink, UpcomingMatchLink } from "@Components/link"

export const BottomSectionLinks = props => {
  return (
    <LinksWrapper>
      {
        moment().isAfter(props.starttime * 1000) ?
          <MatchLink {...props}>Match Stats</MatchLink> :
          <UpcomingMatchLink {...props}>Expected</UpcomingMatchLink>
      }
    </LinksWrapper>
  )
}

