import React from "react"
import moment from 'moment'

import { SmTLogo } from "@Components/icon"
import { TeamLink } from "@Components/link"
import { Score } from "@Components/score"
import { OffsetText } from "@Components/text"
import {
  Wrap,
  TeamsWrap,
  LiveText,
  TeamWrap,
  TeamWrapFirst,
  TeamWrapSecond,
  DateWrapper,
  MatchDescription
} from "./style.js"
import { BottomSectionLinks } from "./components/bottomlinks.js"

export const MatchResult = props => {
  const {
    homeftscore,
    awayftscore,
    homeid,
    awayid,
    home,
    away,
    hlogo,
    alogo,
    hbrief,
    abrief,
    starttime,
    matchstatus,
    matchtype,
    roundname,
    roundid,
    groupid,
    homestscore,
    awaystscore,
    match_time,
    period,
    hwin,
    awin
  } = props

  const hteamname = hbrief && hbrief.length < home.length ? hbrief : home
  const ateamname = abrief && abrief.length < away.length ? abrief : away

  const roundnameforrmatted = roundname == "Group" && groupid == 99
    ? `R${roundid}`
    : `${roundname}`

  const matchtypeformatted = matchtype == 'Cup Short'
    ? '- Cup'
    : matchtype == '2nd Leg Cup Short'
      ? '- 2nd Leg'
      : matchtype == 'Regular'
        ? ''
        : matchtype == '2nd Leg Away Goal'
          ? '- 2nd Leg'
          : `- ${matchtype}`

  const groupidformatted = groupid == 99
    ? ''
    : `- ${groupid}`

  return (
    <Wrap>
      <MatchDescription>
        <OffsetText>{!roundname || `${roundnameforrmatted} ${matchtypeformatted} ${groupidformatted}`}</OffsetText>
      </MatchDescription>
      <TeamsWrap>
        <DateWrapper>
          <OffsetText>{moment.unix(starttime).format('D/M')}</OffsetText>
          {match_time != -1 && period != "FullTime" ? <LiveText>{match_time}'</LiveText> : null}
        </DateWrapper>
        <TeamWrapFirst>
          <span>
            <TeamLink
              teamid={homeid}
              team={home}>
              {hteamname}
            </TeamLink>
          </span>
          <span>
            <SmTLogo
              teamid={homeid}
              logo={hlogo} />
          </span>
          <span>
            <Score
              matchStatus={matchstatus}
              goalsScored={homeftscore}
              goalsConceded={awayftscore}
              odds={hwin}
              etGoalsScored={homestscore}
              etGoalsConceded={awaystscore} />
          </span>
        </TeamWrapFirst>
        <TeamWrapSecond>
          <span>
            <Score
              matchStatus={matchstatus}
              goalsConceded={homeftscore}
              goalsScored={awayftscore}
              odds={awin}
              etGoalsScored={awaystscore}
              etGoalsConceded={homestscore} />
          </span>
          <span>
            <SmTLogo
              teamid={awayid}
              logo={alogo} />
          </span>
          <span>
            <TeamLink
              teamid={awayid}
              team={away}>
              {ateamname}
            </TeamLink>
          </span>
        </TeamWrapSecond>
        <BottomSectionLinks {...props} />
      </TeamsWrap>
    </Wrap>
  )
}
