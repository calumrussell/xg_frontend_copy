import styled from "styled-components"

import { mq, colorScheme } from "@Theme"
import { OffsetText } from "@Components/text"

export const Wrap = styled.div`
  width: 100%;
  ${mq[1]}{
    width: inherit;
    padding-left: 0px;
  }
`

export const TeamsWrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  ${mq[1]}{
    flex-direction: column;
  }
`

export const LiveText = styled(OffsetText)`
  color: ${colorScheme.green[700]};
`

export const TeamWrap = styled.div`
  display: flex;
  align-items: center;
  width: 50%;
  span{
    padding: 0 0.25rem;
  }
`

export const TeamWrapFirst = styled(TeamWrap)`
  justify-content: flex-end; 
  ${mq[1]}{
    width:100%;
    justify-content: flex-start;
    margin-bottom: 0.25rem;
    span:nth-child(1){
      order:10;
    }
    span:nth-child(2){
      order:5;
    }
  }`

export const TeamWrapSecond = styled(TeamWrap)`
  justify-content: flex-start;
  width: 40%;
  ${mq[1]}{
    width:100%;
  }
`

export const DateWrapper = styled.span`
  display: flex;
  width: 10%;
  p{
    margin: 0.25rem;

  }
  ${mq[1]}{
    width: 100%;
  }
`

export const MatchDescription = styled.div`
  display: flex;
  p{ margin: 0.25rem 0 ; }
`

export const LinksWrapper = styled.span`
  width: 10%;
  display: flex;
  justify-content: flex-end;
  margin: 0.25rem 0;

  ${mq[1]}{
    width: 100%;
    justify-content: flex-start;
  }
`
