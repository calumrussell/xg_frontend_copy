import React from "react"

import { SubTitle, DefaultText, OffsetText } from "@Components/text"
import { Wrapper, Text, TitleBar } from "./style.js"

export class Accordion extends React.Component {
  state = { open: false }

  changeState = (ev) => {
    ev.preventDefault()
    let curr = this.state.open
    this.setState({ open: !curr })
  }

  render() {
    const { open } = this.state
    return (
      <Wrapper>
        <TitleBar onClick={this.changeState}><SubTitle>{this.props.title}</SubTitle><OffsetText>Click to Expand</OffsetText></TitleBar>
        <Text open={open}><DefaultText>{this.props.text}</DefaultText></Text>
      </Wrapper>
    )
  }
}
