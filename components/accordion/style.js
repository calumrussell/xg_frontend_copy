import styled from "styled-components"

import { colorScheme } from '@Theme'

export const Text = styled.div`
  padding:10px;
  margin-top: 10px;
  display: ${({ open }) => open ? 'block' : 'none'};
`

export const TitleBar = styled.div`
  background-color: ${colorScheme.grey[400]};
  color: white;
  padding: 10px;
  font-size: 1.5rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const Wrapper = styled.div`
  margin-top: 10px;
`
