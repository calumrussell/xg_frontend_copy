import React from 'react'
import styled from 'styled-components'

import { mq } from '@Theme'

const Main = styled.div`
  margin: 1rem auto;
  width:900px;
  ${mq[1]}{
    width: 100%
  }
`

export const Section = ({ children }) => <Main>{children}</Main>
