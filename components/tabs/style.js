import styled from 'styled-components'

import { colorScheme, mq } from '@Theme'
import { Tab } from "./components/tab.js"

export const TabsWrapper = styled.div`
  display: flex;
  justify-content:center;
  margin: 0.5rem;
  overflow-x: hidden;
  &&:hover{
    overflow-x: auto;
  }
  ${mq[1]}{
    &&{
      overflow-x: auto;
    }
    justify-content: start;
  }
`

export const TabStyled = styled(Tab)`
  border-bottom: ${({ active }) => active ? `2px solid ${colorScheme.bluegrey[700]}` : 'none'};
  &:hover {
    border-bottom: 2px solid ${colorScheme.bluegrey[500]};
  }
  white-space: nowrap;
  text-align:center;
  padding: 0.75rem 0.5rem;
  cursor: pointer;
  margin: 0.25rem 0.5rem;
  header {
    color: ${({ active }) => active ? colorScheme.grey[700] : colorScheme.grey[500]};
    min-width: 120px;
  }
`
