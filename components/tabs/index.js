import React from "react"

import { TabsWrapper, TabStyled } from "./style.js"
 
export const Tabs = props => {

   const {
     active,
     choices,
     switchActive,
     className
  } = props

  return (
    <TabsWrapper className={className}>
      {
        choices.map((choice, i) => <TabStyled key={i}
          active={active == choice}
          length={choices.length}
          switchActive={switchActive}
          choice={choice} />)
      }
    </TabsWrapper>
  )
}
