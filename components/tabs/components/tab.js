import React from "react"

import { SubTitle } from "@Components/text"

export const Tab = ({ className, length, switchActive, active, choice }) => {
  return <div className={className} onClick={() => switchActive(choice)}><SubTitle>{choice}</SubTitle></div>
}
