import React from 'react'

import { MaybeData } from '@Components/monad'
import { SectionTitle } from '@Components/text'
import { Section } from '@Components/section'
import {
  EmptyDivFirst,
  EmptyDivSecond,
  EmptyDivThird
} from "./style.js"

/*
  Visually the same as StatPanel, but this wraps the MaybeData with
  full WhenEmpty and Default rendering behaviour and wrapping inside Section.
*/

const DefaultPanel = ({ title, children }) => {
  return (
    <React.Fragment>
      <div>
        <SectionTitle>{title}</SectionTitle>
      </div>
      <div>
        {children}
      </div>
    </React.Fragment>
  )
}

const IsLoadingPanel = props => {
  return (
    <div>
      <EmptyDivFirst />
      <EmptyDivSecond />
      <EmptyDivThird />
    </div>
  )
}

export const Panel = ({ children, title, requiredData }) => {
  return (
    <Section>
      <MaybeData data={requiredData}>
        <MaybeData.WhenTrue>
          <DefaultPanel
            title={title}>
            {children}
          </DefaultPanel>
        </MaybeData.WhenTrue>
        <MaybeData.Default>
          <DefaultPanel
            title={title}>
            <IsLoadingPanel />
          </DefaultPanel>
        </MaybeData.Default>
        <MaybeData.WhenEmpty><div /></MaybeData.WhenEmpty>
      </MaybeData>
    </Section>
  )
}
