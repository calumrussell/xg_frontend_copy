import styled from "styled-components"

import { colorScheme } from "@Theme"

const EmptyDiv = styled.div`
  height: 1.5rem;
  margin: 1rem 0;
  background: ${colorScheme.grey[300]};
`

export const EmptyDivFirst = styled(EmptyDiv)`
  width: 80%;
`

export const EmptyDivSecond = styled(EmptyDiv)`
  width: 60%;
`

export const EmptyDivThird = styled(EmptyDiv)`
  width: 40%;
`
