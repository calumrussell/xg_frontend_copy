import styled from "styled-components"

import { colorScheme } from "@Theme"

export const FooterWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  margin: 0.5rem 0;
  li{
    a,p{
      color: ${colorScheme.bluegrey[900]};
      text-decoration: none;
      &:hover{
        color: ${colorScheme.orange};
      }
    }
  
    margin: 0.5rem 0;
    list-style-type: none;
    cursor: pointer;
    color: ${colorScheme.grey[700]};
  }
`
