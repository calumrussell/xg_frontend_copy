import React from "react"
import { Link } from "@Routes"

import { NavItem } from '@Components/common'
import { FooterWrapper } from './style.js'

export const Footer = props => (
  <FooterWrapper>
    <NavItem><Link route={'/faq'}><a>Faq</a></Link></NavItem>
    <NavItem><Link route={'/legend'}><a>Legend</a></Link></NavItem>
    <NavItem><Link route={'/contact'}><a>Contact</a></Link></NavItem>
    <NavItem><Link route={'/apidoc'}><a>Api</a></Link></NavItem>
  </FooterWrapper>
)
