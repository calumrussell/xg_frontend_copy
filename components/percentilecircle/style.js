import styled from 'styled-components'

export const PercentileWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const PercentileDot = styled.div`
  background: ${({ percentile }) => {
    return percentile == 90
      ? '#009900'
      : percentile == 75
      ? '#66C166'
      : percentile == 50
      ? '#ffff32'
      : percentile == 25
      ? '#ff6666'
      : '#ff000'
  }}
  border-radius: 50%;
  height: 15px;
  width: 15px;
`

