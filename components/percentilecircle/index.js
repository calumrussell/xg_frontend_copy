import React from "react"

import { PercentileWrapper, PercentileDot } from "./style.js"

export const PercentileCircle = ({ percentile, children }) => {
  return (
    <PercentileWrapper>
      <PercentileDot percentile={percentile} />
      {children}
    </PercentileWrapper>
  )
}
