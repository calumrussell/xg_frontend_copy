import React from "react"
import Plot from "react-plotly.js"
import orderby from "lodash.orderby"
import moment from "moment"

import { ButtonsWrapper } from "@Components/button"
import { SelectAlt } from "@Components/select"

export class LineChart extends React.Component {

  state = {
    selectedStat: 'xg',
  }

  switchStatGroup = event => this.setState({ selectedStat: event.target.value })

  render(){

    const {
      data,
      columns
    } = this.props

    const {
      selectedStat
    } = this.state

    const dates = data.map(d => moment(d['starttime']*1000).format())
    const colData = data.map(d => d[selectedStat])
    const chartData = [{
      x: dates,
      y: colData,
      type: 'scatter',
      line: {'shape': 'spline', 'smoothing': 1}
    }]
   
    return(
      <div>
        <ButtonsWrapper>
          <SelectAlt
            values={columns}
            onChange={this.switchStatGroup}
            selected={selectedStat}
            title={"Stat Category"} />
        </ButtonsWrapper>
        <Plot
          data={chartData}
          layout={{
            hovermode: 'closest',
            margin: {
              t:40,
              b:40,
              l:40,
              r:40
            },
            xaxis: { zeroline: false, fixedrange: true },
            yaxis: { hoverformat: '.2r', fixedrange: true },
            paper_bgcolor: 'transparent',
            plot_bgcolor: 'transparent',
          }}
          useResizeHandler
          style= {{
            width: '100%',
            height: '100%'
          }}
          config={{
            scrollZoom: false,
            displayModeBar: false
          }}
        />
      </div>
   )
  }
}

export class LiveOddsChart extends React.Component{

  render(){

    const {
      data,
      homeTeam,
      awayTeam
    } = this.props

    const ordered = orderby(data, 'time') 
    const dates = ordered.map(d => moment(d['time']*1000).format())
    
    const chartData = [
      {
        x: dates,
        y: ordered.map(d => d['hwin']),
        type: 'scatter',
        name: homeTeam
      },
      {
        x: dates,
        y: ordered.map(d => d['awin']),
        type: 'scatter',
        name: awayTeam
      },
      {
        x: dates,
        y: ordered.map(d => d['dwin']),
        type: 'scatter',
        name: 'Draw'
      }
    ]
   
    return(
      <div>
        <Plot
          data={chartData}
          layout={{
            hovermode: 'closest',
            margin: {
              t:40,
              b:40,
              l:40,
              r:40
            },
            xaxis: { zeroline: false, fixedrange: true },
            yaxis: { type: 'log', autorange: true, fixedrange: true },
            paper_bgcolor: 'transparent',
            plot_bgcolor: 'transparent',
            legend: {
              orientation: "h",
              itemclick: false
            }
          }}
          useResizeHandler
          style= {{
            width: '100%',
            height: '100%'
          }}
          config={{
            scrollZoom: false,
            displayModeBar: false
          }}
        />
      </div>
   )
  }
}
