import React from "react"
import { Link } from '@Routes';

import { stringConverter } from '@Functions/transform'
import { LinkWrapper } from "./style.js"

export const MatchLinkFromTeam = ({
  matchid,
  team,
  opp,
  ishome,
  starttime,
  children
}) => {
  const linkString = ishome
    ? stringConverter(
      `/matches/${matchid}/${team}-vs-${opp}-stats-${starttime}`)
    : stringConverter(
      `/matches/${matchid}/${opp}-vs-${team}-stats-${starttime}`)
  return <LinkWrapper><Link route={linkString}><a>{children}</a></Link></LinkWrapper>
}

export const MatchLink = ({
  matchid,
  home,
  away,
  starttime,
  children
}) => {
  const linkString = stringConverter(
    `/matches/${matchid}/${home}-vs-${away}-stats-${starttime}`
  )
  return <LinkWrapper><Link route={linkString}><a>{children}</a></Link></LinkWrapper>
}

export const TeamLink = ({
  teamid,
  team,
  children,
}) => {
  const linkString = stringConverter(
    `/team/${teamid}/${team}-stats`
  )
  return <LinkWrapper><Link route={linkString}><a>{children}</a></Link></LinkWrapper>
}

export const PlayerLink = ({
  playerid,
  formatname,
  children
}) => {
  const linkString = stringConverter(
    `/player/${playerid}/${formatname}-stats`
  )
  return <LinkWrapper><Link route={linkString}><a>{children}</a></Link></LinkWrapper>
}

export const UpcomingMatchLink = ({
  league,
  matchid,
  homeid,
  awayid,
  home,
  away,
  year,
  children
}) => {
  const linkString = stringConverter(
    `/h2h/${league}/${matchid}/${homeid}/${awayid}/${home}-vs-${away}-stats-${year}`
  )
  return <LinkWrapper><Link route={linkString}><a>{children}</a></Link></LinkWrapper>
}
