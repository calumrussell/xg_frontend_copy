import styled from "styled-components"

import { colorScheme, textFont } from "@Theme"

export const LinkWrapper = styled.span`
  a{
    white-space: nowrap;
    color: ${colorScheme.bluegrey[900]};
    text-decoration: none;
    font-family: ${textFont};
    font-size: 0.9rem;
    cursor: pointer;
    &:hover{
      color: ${colorScheme.orange};
    }
  }
`
