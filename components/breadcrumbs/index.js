import React from "react"

import { DefaultText } from "@Components/text"
import { Wrapper } from "./style.js"

const Base = ({ children }) => <Wrapper>/  <Link route={'/'}><a>Home</a></Link>{children}</Wrapper>
const Blog = ({ post }) => <React.Fragment>  >> <Link route={'/blog'}><a>Blog</a></Link></React.Fragment>

export const BreadCrumbs = ({ post }) => {
  if (post == undefined || post == null) return <Base />
  else return <DefaultText><Base><Blog /></Base></DefaultText>
}
