import React from 'react'
import { render } from 'react-testing-library'

import { RequiresData, MaybeData } from './index.js'

describe('define RequiresData', () => {

  const emptyData = []
  const undefinedData = undefined
  const nullData = null

  it('renders Default when data is undefined', () => {
    const Component = props => {
      return (
        <RequiresData data={undefinedData} >
          <RequiresData.WhenTrue><div data-testid="not-present">WhenTrue</div></RequiresData.WhenTrue>
          <RequiresData.Default><div data-testid="default">Default</div></RequiresData.Default>
        </RequiresData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(getByTestId('default')).toHaveTextContent("Default")
    expect(queryByTestId('not-present')).toBeFalsy()
  })

  it('renders Default when data is null', () => {
    const Component = props => {
      return (
        <RequiresData data={nullData} >
          <RequiresData.WhenTrue><div data-testid="not-present">WhenTrue</div></RequiresData.WhenTrue>
          <RequiresData.Default><div data-testid="default">Default</div></RequiresData.Default>
        </RequiresData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(getByTestId('default')).toHaveTextContent("Default")
    expect(queryByTestId('not-present')).toBeFalsy()
  })

  it('renders Default when data is list with no values', () => {
    const Component = props => {
      return (
        <RequiresData data={emptyData} >
          <RequiresData.WhenTrue><div data-testid="not-present">WhenTrue</div></RequiresData.WhenTrue>
          <RequiresData.Default><div data-testid="default">Default</div></RequiresData.Default>
        </RequiresData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(getByTestId('default')).toBeTruthy()
    expect(queryByTestId('not-present')).toBeFalsy()
  })

  it('renders WhenTrue when data is a list with elements', () => {
    const Component = props => {
      return (
        <RequiresData data={['2']} >
          <RequiresData.WhenTrue><p data-testid="whentrue">WhenTrue</p></RequiresData.WhenTrue>
          <RequiresData.Default><p data-testid="not-present">Test</p></RequiresData.Default>
        </RequiresData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(queryByTestId('not-present')).toBeFalsy()
    expect(getByTestId('whentrue')).toHaveTextContent('WhenTrue')
  })
})

describe('define MaybeData', () => {

  const emptyData = []
  const undefinedData = undefined
  const nullData = null

  it('renders Default when data is undefined', () => {
    const Component = props => {
      return (
        <MaybeData data={undefinedData} >
          <MaybeData.WhenTrue><div data-testid="not-present">WhenTrue</div></MaybeData.WhenTrue>
          <MaybeData.WhenEmpty><div data-testid="not-present">WhenTrue</div></MaybeData.WhenEmpty>
          <MaybeData.Default><div data-testid="default">Default</div></MaybeData.Default>
        </MaybeData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(getByTestId('default')).toHaveTextContent("Default")
    expect(queryByTestId('not-present')).toBeFalsy()
  })

  it('renders Default when data is null', () => {
    const Component = props => {
      return (
        <MaybeData data={nullData} >
          <MaybeData.WhenTrue><div data-testid="not-present">WhenTrue</div></MaybeData.WhenTrue>
          <MaybeData.WhenEmpty><div data-testid="not-present">WhenTrue</div></MaybeData.WhenEmpty>
          <MaybeData.Default><div data-testid="default">Default</div></MaybeData.Default>
        </MaybeData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(getByTestId('default')).toHaveTextContent("Default")
    expect(queryByTestId('not-present')).toBeFalsy()
  })

  it('renders WhenEmpty when data is an empty list', () => {
    const Component = props => {
      return (
        <MaybeData data={emptyData} >
          <MaybeData.WhenTrue><p data-testid="not-present">Shouldn't happen</p></MaybeData.WhenTrue>
          <MaybeData.WhenEmpty><div data-testid="expected">Default</div></MaybeData.WhenEmpty>
        </MaybeData >
      )
    }
    const { getByTestId, queryByTestId } = render(<div><Component /></div>)
    expect(queryByTestId('not-present')).toBeFalsy()
    expect(getByTestId('expected')).toHaveTextContent("Default")
  })

  it('renders WhenEmpty when data is an empty list with a Default option', () => {
    const Component = props => {
      return (
        <MaybeData data={emptyData}>
          <MaybeData.WhenTrue><p data-testid="not-present">Shouldn't happen</p></MaybeData.WhenTrue>
          <MaybeData.Default><p data-testid="not-present">Test</p></MaybeData.Default>
          <MaybeData.WhenEmpty><div data-testid="default">Default</div></MaybeData.WhenEmpty>
        </MaybeData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(queryByTestId('not-present')).toBeFalsy()
    expect(getByTestId('default')).toHaveTextContent("Default")
  })

  it('renders Default when data is undefined with a WhenEmpty option ', () => {
    const Component = props => {
      return (
        <MaybeData data={null} >
          <MaybeData.WhenTrue><p data-testid="not-present">Shouldn't happen</p></MaybeData.WhenTrue>
          <MaybeData.Default><div data-testid="default">Default</div></MaybeData.Default>
          <MaybeData.WhenEmpty><p data-testid="not-present">Shouldn't happen</p></MaybeData.WhenEmpty>
        </MaybeData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(queryByTestId('not-present')).toBeFalsy()
    expect(getByTestId('default')).toHaveTextContent("Default")
  })

  it('renders WhenTrue when there is a list of data with a WhenEmpty and Default option ', () => {
    const Component = props => {
      return (
        <MaybeData data={['2']} >
          <MaybeData.WhenTrue><div data-testid="default">Default</div></MaybeData.WhenTrue>
          <MaybeData.Default><p data-testid="not-present">Shouldn't happen</p></MaybeData.Default>
          <MaybeData.WhenEmpty><p data-testid="not-present">Shouldn't happen</p></MaybeData.WhenEmpty>
        </MaybeData >
      )
    }
    const { getByTestId, queryByTestId } = render(<Component />)
    expect(queryByTestId('not-present')).toBeFalsy()
    expect(getByTestId('default')).toHaveTextContent("Default")
  })
})
