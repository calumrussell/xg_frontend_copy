import React from "react"
import PropTypes from "prop-types"

export class MaybeData extends React.Component {

  whenTrueDefaultTest = data => data != null && data != undefined && (data && data.length != 0)
  whenEmptyDefaultTest = data => data != null && data.length == 0

  static WhenTrue = ({ data, whenTrueTest, children }) =>
    whenTrueTest(data) ? children : null
  static Default = ({ data, whenTrueTest, whenEmptyTest, children }) =>
    !whenTrueTest(data) && !whenEmptyTest(data) ? children : null
  static WhenEmpty = ({ data, whenEmptyTest, children }) =>
    whenEmptyTest(data) ? children : null

  render() {
    return (
      <div>
        {
          React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
              data: this.props.data,
              whenTrueTest: this.props.trueTest == undefined ? this.whenTrueDefaultTest : this.props.trueTest,
              whenEmptyTest: this.props.emptyTest == undefined ? this.whenEmptyDefaultTest : this.props.emptyTest
            })
          })
        }
      </div>
    )
  }
}

export class RequiresData extends React.Component {

  whenTrueDefaultTest = data => data != null && data != undefined && (data && data.length != 0)

  static WhenTrue = ({ data, whenTrueTest, children }) =>
    whenTrueTest(data) ? children : null
  static Default = ({ data, whenTrueTest, children }) =>
    !whenTrueTest(data) ? children : null

  render() {
    return (
      <div>
        {
          React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
              data: this.props.data,
              whenTrueTest: this.props.trueTest == undefined ? this.whenTrueDefaultTest : this.props.trueTest,
            })
          })
        }
      </div>
    )
  }
}
