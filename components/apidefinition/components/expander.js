import React from "react"

import { TitleBar, StyledAddIcon, StyledMinusIcon } from "../style.js"

class Expander extends React.Component {

  state = {
    isClosed: true
  }

  toggleVisibility = event => this.setState(state => ({ isClosed: !state.isClosed }))

  render() {
    const { title, children, className } = this.props
    const { isClosed } = this.state
    return (
      <div className={className}>
        <TitleBar onClick={this.toggleVisibility}>
          {title}
          {
            isClosed
              ? <StyledAddIcon />
              : <StyledMinusIcon />
          }
        </TitleBar>
        {isClosed || children}
      </div>
    )
  }
}

export default Expander
