import React from "react"

import { SubTitle, DefaultText, OffsetText } from "@Components/text"
import { colorScheme } from '@Theme'
import { RouteWrapper, RouteHeaderWrapper } from "../style.js"

const Route = ({ title, description, route, link, params }) => {
  return (
    <RouteWrapper>
      <Expander
        title={[
          <RouteHeaderWrapper>
            <SubTitle>{title}</SubTitle>
          </RouteHeaderWrapper>
        ]} >
        <OffsetText>{route}</OffsetText>
        <a href={link} target="_blank">Link</a>
        <p>{params}</p>
        <DefaultText>{description}</DefaultText>
      </Expander>
    </RouteWrapper>
  )
} 

export default Route
