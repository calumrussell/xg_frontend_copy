import styled from "styled-components"

import { colorScheme } from "@Theme"
import Expander from "./components/expander.js"
import { AddIcon, MinusIcon } from "@Components/icon"

export const RouteWrapper = styled.div`
  margin .5rem 0;
  padding: .5rem;
  background: ${colorScheme.grey[200]}
`

export const RouteHeaderWrapper = styled.div`
  display: flex;
`

export const APIDefinitionWrapper = styled.div`
  padding: 0.5rem 0;
`

export const StyledExpander = styled(Expander)`
  margin: 0.5rem 0;
`

export const TitleBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`

export const StyledAddIcon = styled(AddIcon)`
  height: 25px;
  width: 25px;
  svg{
    fill: ${colorScheme.grey[700]};
  }
  border-radius: 50%;
  padding: .25rem;
  background: ${colorScheme.grey[400]};
`

export const StyledMinusIcon = styled(MinusIcon)`
  height: 25px;
  width: 25px;
  svg{
    fill: ${colorScheme.grey[700]};
  }
  border-radius: 50%;
  padding: .25rem;
  background: ${colorScheme.grey[400]};
`
