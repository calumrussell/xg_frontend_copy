import React from "react"

import { apiDefinition } from "@Services/apidefinition"
import { SubTitle } from "@Components/text"
import Route from "./components/route.js"
import { APIDefinitionWrapper, StyledExpander } from "./style.js"

export const ApiDefinition = props => {
  return (
    <APIDefinitionWrapper>
      <SubTitle>Routes</SubTitle>
      {
        apiDefinition.map(d => {
          return (
            <StyledExpander title={[<SubTitle>{d.groupName}</SubTitle>]} >
              {
                d.routes.map((r, i) => <Route key={i} {...r} />)
              }
            </StyledExpander>
          )
        })
      }
    </APIDefinitionWrapper>
  )
}
