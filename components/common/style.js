import styled from "styled-components"

import { colorScheme } from "@Theme"

export const MenuItemWrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 0.5rem;
  img{
    height:1rem;
    width:1rem;
    margin-right: 0.5rem;
    border-radius: 50%;
    border: 2px solid ${colorScheme.grey[500]};
    background: white;
  }`

export const MenuItemLink = styled.div`
  cursor:pointer;
  a{
    text-decoration:none;
    font-size:1.1rem;
    color: ${colorScheme.grey[700]};
  }
  a:hover{
    color: ${colorScheme.orange};
  }
  color: inherit;
`
