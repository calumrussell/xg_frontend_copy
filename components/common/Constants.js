import { colorScheme } from '@Theme'

export const planNumbers = {
  0: 'Free',
  1: 'Basic'
}

const freeLeagues = ['English Prem', 'Bundesliga 1', 'Major League Soccer']
const basicLeagues = [...freeLeagues, 'La Liga', 'Serie A', 'Primeria Liga', 'Eredivise']

export const freePlan = {
  "title": 'Free',
  "price": 0,
  "leagues": freeLeagues,
  "apiLevel": 'Limited',
  "historical": '1 year',
  "contentBackground": '#707070',
  "headerBackground": '#707070'
}

export const basicPlan = {
  "title": 'Basic',
  "price": 15,
  "leagues": basicLeagues,
  "apiLevel": 'Full',
  "historical": "At least 3 years",
  "contentBackground": `${colorScheme.grey[300]}`,
  "headerBackground": `${colorScheme.grey[400]}`
}

export const plans = [freePlan, basicPlan]

export const stripePublicKey = 'pk_live_dBRxakqq5m9k6TYqFwSl0xnO'