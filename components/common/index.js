import React from "react"
import styled from "styled-components"
import { Link } from "@Routes"

import { SectionTitle, SubTitle } from '@Components/text'
import { colorScheme } from "@Theme"
import { MenuItemWrapper, MenuItemLink } from './style.js'

export const MenuItem = ({
  countrycode,
  tournamentid,
  seasonid,
  league
}) => {
  return (
    <MenuItemWrapper>
      <Link route={`/league/${league}/tournament/${tournamentid}/season/${seasonid}`}>
        <img src={`https://xg.football/static/img/flags/${countrycode}_sq.svg`} />
      </Link>
      <MenuItemLink>
        <Link route={`/league/${league}/tournament/${tournamentid}/season/${seasonid}`}>
          <SubTitle>{league}</SubTitle>
        </Link>
      </MenuItemLink>
    </MenuItemWrapper>
  )
}

export const NavItem = styled(SectionTitle)`
  margin-right: 0.5rem;
  cursor:pointer;
  text-decoration: none;
  a,p{
    color: ${colorScheme.bluegrey[900]};
    text-decoration: none;
    &:hover{
      color: ${colorScheme.orange};
    }
  }
`

export const NavWrap = styled.nav`
  border-bottom: 1px solid ${colorScheme.grey[300]};
`

export const NavLinks = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items:center;
`

export const NavButton = styled.div`
  cursor:pointer;
  text-decoration: none;
  button{
    margin-right: 0.5rem;
    background: ${colorScheme.green[500]};
    border: ${colorScheme.green[200]};
    a {
      color: ${colorScheme.grey[50]};
      font-size: 1rem;
    }
  }
  button:hover{
    background: ${colorScheme.green[700]};
  }
`
