import React from "react"

import { DefaultText } from "@Components/text"
import {
  HeaderWrap,
  BackgroundWrap,
  TextAlign,
  TextWrapper
} from "./style.js"

export const Promo = props => {
  return (
    <HeaderWrap>
      <BackgroundWrap>
        <img src='https://xg.football/static/img/img/ball.png' />
      </BackgroundWrap>
      <TextAlign>
        <TextWrapper>
          <DefaultText>
            Detailed player and team stats across <b>100+ categories</b><br/>
            Covering <b>15 leagues</b> around the world<br/>
            Custom categories that will add value to your research<br/>
          </DefaultText>
        </TextWrapper>
      </TextAlign>
    </HeaderWrap>
  )
}
