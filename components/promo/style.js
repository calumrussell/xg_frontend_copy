import styled from "styled-components"
 
import { colorScheme, mq } from "@Theme"

export const HeaderWrap = styled.div`
  margin:10px;
  position: relative;
  img{
    mix-blend-mode: hard-light;
    filter: grayscale(90%) blur(2px);
    width:100%;
  }
`

export const BackgroundWrap = styled.div`
  width:100%;
  background: ${colorScheme.grey[400]};
`

export const TextAlign = styled.div`
  position: absolute;
  top: 10%;
  left: 20px;
  opacity: 0.8;
  background: ${colorScheme.grey[100]};
  ${mq[1]}{
    top:0;
    left:0;
    width:100%;
    height:100%;
  }
`

export const TextWrapper = styled.div`
  padding: 10px 20px;
  color: ${colorScheme.grey[600]};
  p{
    margin: 2px 0;
    opacity: 1;
  }
`
