import styled from "styled-components"

import { colorScheme, textFont } from '@Theme'

export const ScoreWrap = styled.div`
  width:1.5rem;
  height:1.5rem;
  background: ${colorScheme.grey[200]};
  display: flex;
  align-items: center;
  justify-content:center;
  padding: 0.25rem;
  font-weight: bold;
`

export const MainScore = styled.span`
  padding: 0;
  font-size: 0.9rem;
  font-family: ${textFont};
`

