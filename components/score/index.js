import React from "react"

import { MainScore, ScoreWrap } from "./style.js"

export const Score = ({
  goalsScored,
  goalsConceded,
  matchStatus,
  odds
}) => {
  const isPostponed = matchStatus === "Postponed"
  const formattedScore = goalsScored === -1
    ? odds ? odds.toFixed(1) : '-'
    : goalsScored
  return (
    <ScoreWrap
      goalsScored={goalsScored}
      goalsConceded={goalsConceded}>
      {
        isPostponed
          ? "P"
          : <MainScore>{formattedScore}</MainScore>
      }
    </ScoreWrap>
  )
}
