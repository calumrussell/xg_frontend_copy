import React from "react";

import { headerConverter, titleConverter } from "@Functions/transform"
import { DefaultText } from "@Components/text"

export const Stat = ({ name }) => {
  return (
    <div>
      <DefaultText>
        <b>{headerConverter(name)}</b>: {titleConverter(name)}
      </DefaultText>
    </div>
  )
}
