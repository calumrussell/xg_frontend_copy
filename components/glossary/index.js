import React from "react";

import { QuestionIcon } from "@Components/icon"
import { Wrap } from "./style.js"
import { Stat } from "./components"

export class Glossary extends React.Component {

  state = {
    isShowing: false
  }

  toggleVisibility = () => this.setState(state => ({ isShowing: !state.isShowing }))

  render() {
    const { isShowing } = this.state
    const { columns, text } = this.props
    return (
      <React.Fragment>
        <QuestionIcon onClick={this.toggleVisibility} />
        {
          isShowing
            ? <Wrap>
              {
                columns.map((c, i) => <Stat key={i} name={c} />)
              }
              {
                text || null
              }
            </Wrap>
            : null
        }
      </React.Fragment>
    )
  }
}
