import styled from "styled-components"

import { mq } from "@Theme"

export const Wrap = styled.div`
  display: flex;
  overflow-x: hidden;
  &&:hover{
    overflow-x: auto;
  }
  ${mq[0]}{
    &&{
      overflow-x:scroll;
    }
  }
  div{
    padding: 0.5rem;
    white-space:nowrap;
  }
`
