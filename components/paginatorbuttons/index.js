import React from "react"

import { Button } from "@Components/button"
import { SelectSimple } from "@Components/select"
import { Wrap } from "./style.js"

export const PaginatorButtons = ({ moveBackward, moveForward, onSelectChange, gap }) => {
  return (
    <Wrap>
      <Button
        onClick={moveForward}>
        {'<'}
      </Button>
      <SelectSimple
        name={'gap'}
        selected={gap}
        onChange={onSelectChange}
        values={[5, 10, 15]} />
      <Button
        onClick={moveBackward}>
        {'>'}
      </Button>
    </Wrap>
  )
}
