import styled from "styled-components"

export const Wrap = styled.div`
  display: flex;
  button{
    margin: 0;
    font-size: 1.6rem;
    padding: 0.25rem;
  }
  div{
    display: flex;
    margin: 0;
  } 
`
