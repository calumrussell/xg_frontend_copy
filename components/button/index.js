import styled from 'styled-components'

import { colorScheme, titleFont } from '@Theme'

import { 
  InnerItem,
  StatPanelTextWrapper,
  Wrapper
} from "./style.js"

export const Button = styled.button`
  background: ${colorScheme.grey[200]};
  padding: 0.5rem;
  font-family: ${titleFont};
  font-size: 0.9rem;
  a{
    font-size: 0.9rem;
  }
  border-radius: 0.25rem;
  margin-right: 0.75rem;
  cursor: pointer;
  color: ${colorScheme.grey[800]};
  a{
    color: ${colorScheme.grey[800]};
  }
  border: 1px solid ${colorScheme.bluegrey[700]};
  &:hover{
    background-color: ${colorScheme.bluegrey[700]};
    color: ${colorScheme.grey[50]};
    a{
      color: ${colorScheme.grey[50]};
    }
  }
  &:active{
    background-color: orange;
  }
  &:disabled{
    background-color: pink;
  }
  a{
    text-decoration: none;
  }
`

export const ButtonRounded = styled.button`
  border-radius: 10px;
  border: none;
  background: ${colorScheme.grey[200]};
  padding: 0.25rem 0.75rem;
  color: ${colorScheme.grey[500]};
  cursor: pointer;
`

export const ButtonsWrapper = ({ children, selector }) => {
  return (
    <div>
      <Wrapper>
        {
          React.Children.map(children, (child) => <InnerItem>{child}</InnerItem>)
        }
      </Wrapper>
      <StatPanelTextWrapper id={selector || 'stat-panel-text'} />
    </div>
  )
}
