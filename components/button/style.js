import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
`

export const InnerItem = styled.div`
  border-right: none;
`

export const StatPanelTextWrapper = styled.div`
  padding: 1rem 0.5rem;
`
