import styled from "styled-components"

import { colorScheme, titleFont, textFont } from "@Theme"

const Title = styled.header`
  font-family: ${titleFont};
  letter-spacing: 0.02em;
  font-weight: bold;
  color: ${colorScheme.grey[700]};
`

const PageTitle = styled(Title)`
  font-size: 1.3rem;
  color: ${colorScheme.grey[600]};
`

const SectionTitle = styled(Title)`
  font-size: 1.1rem;
  font-weight: normal;
  margin: 0.25rem 0;
  color: ${colorScheme.grey[600]};
`

const SubTitle = styled(Title)`
  font-size: 1rem;
  font-weight: normal;
  color: ${colorScheme.grey[700]};
`

const LightTitle = styled(Title)`
  font-size: 0.85rem;
  font-weight: normal;
  color: ${colorScheme.grey[500]}:
`

const DefaultText = styled.p`
  font-size: 1rem;
  font-family: ${textFont};
  color: ${colorScheme.grey[800]};
  margin: 0;
  p{
    margin: 0;
    font-size: 1rem;
    font-family: ${textFont};
    color: ${colorScheme.grey[800]};
  }
`

const SmallText = styled.p`
  font-size: 0.8rem;
  font-family: ${textFont};
  color: ${colorScheme.grey[800]};
  margin: 0;
`

const OffsetText = styled.p`
  font-size:0.8rem;
  font-family: ${textFont};
  color: ${colorScheme.grey[500]};
`

export {
  PageTitle,
  SectionTitle,
  SubTitle,
  LightTitle,
  DefaultText,
  SmallText,
  OffsetText
}
