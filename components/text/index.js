import {
  PageTitle,
  SectionTitle,
  SubTitle,
  LightTitle,
  DefaultText,
  SmallText,
  OffsetText
} from "./style.js"

export {
  PageTitle,
  SectionTitle,
  SubTitle,
  LightTitle,
  DefaultText,
  SmallText,
  OffsetText
}
