import styled from "styled-components"

import { colorScheme } from '@Theme'

const LoadWrap = styled.div`
  display: flex;
  justify-content: center;
  max-height:100%;
  max-width:100%
  margin-bottom: 10px;
  margin-top:10px;
`

const LoaderInner = styled.div`
  border: 16px solid ${colorScheme.grey[300]}; /* Light grey */
  border-top: 16px solid ${colorScheme.grey[600]}; /* Blue */
  border-radius: 50%;
  width: 3rem;
  height: 3rem;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`

LoadWrap.displayName = "loader-wrap"
LoaderInner.displayName = "loader-loader"

export {
  LoadWrap,
  LoaderInner
}
