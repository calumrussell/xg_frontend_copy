import React from "react"

import { LoadWrap, LoaderInner } from "./style.js"

export const Loader = props => <LoadWrap><LoaderInner /></LoadWrap>
