import React from "react"
import styled from 'styled-components'

export const PercentileBorder = styled.div`
  border: 1px solid ${({ percentile }) => {
    return percentile == 90
      ? '#009900'
      : percentile == 75
      ? '#66C166'
      : percentile == 50
      ? '#ffff32'
      : percentile == 25
      ? '#ff6666'
      : '#ff000'
  }}
`
