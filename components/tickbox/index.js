import React from "react"
import uniqueId from "lodash.uniqueid"

import { SmallText } from "@Components/text"

export const TickBox = ({ 
  className, 
  label, 
  handleCheck, 
  isChecked 
}) => {
  const id = uniqueId("prefix-");
  return (
    <div className={className}>
      <label htmlFor={id}><SmallText>{label}</SmallText></label>
      <input id={id} type="checkbox" checked={isChecked} onChange={handleCheck} />
    </div>
  )
}


