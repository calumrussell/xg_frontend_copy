import React from "react"
import { connect } from "react-redux"

import { toggleNav } from "@Redux/actions/Nav"

const About = props => {
  return (
    <div>
      <button onClick={() => toggleNav()(props.dispatch)}>Toggle</button>
      <p>{props.nav.isOpen ? "true": "false"}</p>
      <p>This is about something {props.query.id}</p>
    </div>
  )
}

About.getInitialProps = async ({query}) => {
  return {query}
}

const mapStateToProps = state => {
  const { nav } = state
  return { nav }
}

export default connect(mapStateToProps)(About)
