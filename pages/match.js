import React from "react"
import styled from "styled-components"

import { SummaryComponent } from '@Containers/match/summary'
import { MatchLogComponent } from '@Containers/match/matchlog'
import { HeaderComponent } from '@Containers/match/header'
import { LiveOddsComponent } from '@Containers/match/liveodds'
import { LineupComponent } from '@Containers/match/lineup'
import { ShotsComponent } from '@Containers/match/shots'
import { Section } from "@Components/section"
import { Tabs } from "@Components/tabs"
import { ViewWrapper } from "@Components/wrapper"
import { RequiresData } from "@Components/monad"
import { Loader } from "@Components/loader"

const MainWrapper = styled.div`
  margin: 0.5rem 0;
`

class Match extends React.Component {

  state = {
    active: 'Summary'
  }

  switchActive = active => this.setState({ active })

  renderLogic = (active, props) => {
    switch (active) {
      case 'Summary':
        return <SummaryComponent {...props} />
      case 'Match Log':
        return <MatchLogComponent {...props} />
      case 'Live Odds':
        return <LiveOddsComponent {...props} />
      case 'Lineup':
        return <LineupComponent {...props} />
      case 'Shots':
        return <ShotsComponent {...props} />
      default:
        return <div />
    }
  }

  render() {
    const { active } = this.state
    const { id } = this.props

    return (
      <ViewWrapper>
        <Section>
          <HeaderComponent {...this.props} />
          <Tabs
            choices={['Summary', 'Match Log',
              'Live Odds', 'Lineup', 'Shots']}
            switchActive={this.switchActive}
            active={active} />

            <RequiresData data={id}>
              <RequiresData.WhenTrue>
                <MainWrapper>
                  {this.renderLogic(active, this.props)}
                </MainWrapper>
              </RequiresData.WhenTrue>
              <RequiresData.Default><Loader /></RequiresData.Default>
            </RequiresData>
        </Section>
      </ViewWrapper>
    )
  }
}

import {
  getTeamStatsByMatch,
  getMatchStatsByMatch,
  getMatchSummary,
  getLiveOddsByMatch,
  getSeasonAveragesBySeason
} from '@Services/api'
import { tokenParser } from '@Services/token'

Match.getInitialProps = async ctx => {

  const token = await tokenParser(ctx)

  const {
    id
  } = ctx.query

  const [
    teamStats,
    playerStats,
    matchSummary,
    liveOdds
  ] = await Promise.all([
    getTeamStatsByMatch(id, token),
    getMatchStatsByMatch(id, token),
    getMatchSummary(id, token),
    getLiveOddsByMatch(id, token)
  ])

  let seasonAverages = []
  if (matchSummary && matchSummary.length > 0) {
    seasonAverages = await getSeasonAveragesBySeason(matchSummary[0]['seasonid'], token)
  }

  return {
    id,
    data: {
      teamStats,
      playerStats,
      matchSummary,
      liveOdds,
      seasonAverages: seasonAverages[0]
    }
  }
}

export default Match

