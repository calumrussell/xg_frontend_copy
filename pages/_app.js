import App, { Container } from 'next/app'
import Head from 'next/head'
import styled from 'styled-components'
import React from 'react'
import { Provider } from 'react-redux'

import { colorScheme } from '@Theme'
import withReduxStore from '@Redux/withReduxStore.js'

const LightGrayBackground = styled.div`
  height: fit-content;
  min-height: 100vh;
  color: ${colorScheme.grey[700]};
`

class MyApp extends App {
  
  render() {
    const { Component, pageProps, reduxStore } = this.props
    return (
      <Container>
        <style jsx global>{`
          body{
            background-color: ${colorScheme.grey[100]};
          }
          ::-webkit-scrollbar {
            -webkit-apperance:none;
            width: 0.5rem;
            height: 0.5rem;
          }
          ::-webkit-scrollbar-thumb {
            border-radius: 3px;
            background: ${colorScheme.grey[300]}
          }
        `}</style>

        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans" rel="stylesheet" />
          <link rel="stylesheet" href="https://use.typekit.net/tzo6aoj.css"></link>
          <meta name="google-site-verification" content="-lpHSGN6-_7_Gcxnv9SfN5kYWgMt-FmlKj9j3m6cEag" />
          <link rel="stylesheet" href="https://use.typekit.net/tzo6aoj.css"></link>
          <script dangerouslySetInnerHTML={{
            __html: `
            !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
            analytics.load("jYREnGjGWm7dhqrQX9BC0NhNNmFKMqIm");
            analytics.page();
            }}();
          `}} />
        </Head>

        <LightGrayBackground>
          <Provider store={reduxStore}>
            <Component {...pageProps} />
          </Provider>
        </LightGrayBackground>
      </Container >
    )
  }
}

export default withReduxStore(MyApp)
