import React from "react"
import Prismic from 'prismic-javascript'
import sortby from 'lodash.sortby'

import { MatchSorter } from '@Containers/home/matchsorter'
import { TopPlayers } from '@Containers/league/topplayers'
import { ViewWrapper } from "@Components/wrapper"
import { Panel } from "@Components/panel"
import { tokenParser } from '@Services/token'

const Home = props => {
  const { data } = props

  return (
    <ViewWrapper>
      <Panel
        title={'Recent Matches'}
        requiredData={data.matches}>
        <MatchSorter {...props} />
      </Panel>
      <Panel
        title={'Top Players From Big Five'}
        requiredData={data.topPlayers}>
        <TopPlayers
          {...props} />
      </Panel>
    </ViewWrapper>
  )
}

import {
  getRecentMatches,
  getCurrentTournaments,
  getUpcomingMatches,
  getTopPlayersByTopLeagues
} from '@Services/api'

Home.getInitialProps = async ctx => {

  const token = await tokenParser(ctx)
  const endpoint = 'https://calumrussell.prismic.io/api/v2';

  const [
    recentMatches,
    upcomingMatches,
    leagues,
    topPlayers,
    posts
  ] = await Promise.all([
    getRecentMatches(token),
    getUpcomingMatches(token),
    getCurrentTournaments(token),
    getTopPlayersByTopLeagues(token),
    Prismic.api(endpoint).then(api => {
      return api.query(Prismic.Predicates.at('document.type', 'blog_post'), {
        fetch: ['blog_post.title', 'blog_post.uid', 'blog_post.header'],
        orderings: '[document.last_publication_date desc]'
      }).then(resp => resp.results)
    })
  ])

  const matches = recentMatches.concat(upcomingMatches)

  //Define "coreLeagues", not ideal way of doing this
  const coreLeaguesNames = ['EPL', 'BUN', 'LIGA', 'ISA', 'LIG1']
  const coreLeagues = leagues.filter(v => coreLeaguesNames.includes(v.league))
  const otherLeagues = leagues.filter(v => !coreLeaguesNames.includes(v.league))
  const filteredLeagues = [...coreLeagues, ...sortby(
    otherLeagues, ['regionname', 'countrycode']
  )]

  return {
    posts,
    data: {
      matches,
      leagues: filteredLeagues,
      topPlayers
    }
  }
}

export default Home

