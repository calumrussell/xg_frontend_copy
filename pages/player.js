import React from 'react';
import { connect } from 'react-redux'

import { MatchLogComponent } from '@Containers/player/matchlog'
import { SeasonAvgComponent } from '@Containers/player/seasonavg'
import { PlayerInfoComponent } from '@Containers/player/playerinfo'
import { PlayerHistoryComponent } from '@Containers/player/playerhistory'
import { LatestSeasonShotsComponent } from '@Containers/player/latestseasonshots'
import { RequiresData } from "@Components/monad"
import { Loader } from "@Components/loader"
import { Panel } from "@Components/panel"
import { ViewWrapper } from "@Components/wrapper"
import { Tabs } from "@Components/tabs"

const Summary = props => {
  const { data } = props
  return (
    <span>
      <Panel
        title={'Player History'}
        requiredData={data.playerHistory}>
        <PlayerHistoryComponent
          {...props} />
      </Panel>
      <Panel
        title={'Season Averages'}
        requiredData={data.seasonAverages}>
        <SeasonAvgComponent
          {...props} />
      </Panel>
      <Panel
        title={'Match Log'}
        requiredData={data.playerStats}>
        <MatchLogComponent
          {...props} />
      </Panel>
    </span>
  )
}

class Player extends React.Component {

  state = {
    active: 'Summary'
  }

  switchActive = active => this.setState({ active })

  render() {
    const { id } = this.props
    const { active } = this.state
    return (
      <ViewWrapper>
        <RequiresData data={id}>
          <RequiresData.WhenTrue>
            <PlayerInfoComponent
              {...this.props} />
          </RequiresData.WhenTrue>
          <RequiresData.Default><Loader /></RequiresData.Default>
        </RequiresData>
        <Tabs
          choices={['Summary', 'Shots']}
          switchActive={this.switchActive}
          active={active} />
        {
          active == 'Summary'
            ? <Summary {...this.props} />
            : active == 'Shots'
              ? <LatestSeasonShotsComponent {...this.props} />
              : null
        }
      </ViewWrapper>
    )
  }
}

import {
  getLatestSeasonMatchStatsByPlayer,
  getPlayerAverages,
  getPlayerTeamAveragesMain,
  getPlayerPercentile,
  getUpcomingMatchesByPlayer,
  getPlayerHistory
} from '@Services/api'

import { tokenParser } from '@Services/token'

Player.getInitialProps = async ctx => {

  const token = await tokenParser(ctx)

  const {
    id,
    name
  } = ctx.query

  const [
    playerStats,
    seasonAverages,
    teamSeasonAverages,
    averagesPercentile,
    upcomingMatches,
    playerHistory
  ] = await Promise.all([
    getLatestSeasonMatchStatsByPlayer(id, token),
    getPlayerAverages(id, token),
    getPlayerTeamAveragesMain(id, token),
    getPlayerPercentile(id, token),
    getUpcomingMatchesByPlayer(id, token),
    getPlayerHistory(id, token),
  ])
  return {
    id,
    name,
    data: {
      playerStats,
      seasonAverages,
      teamSeasonAverages,
      averagesPercentile,
      upcomingMatches,
      playerHistory
    }
  }
}

const mapStateToProps = (state) => {
  const { showDK } = state.draftkings
  return {
    showDK
  }
}

export default connect(mapStateToProps)(Player) 
