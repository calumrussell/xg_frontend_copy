import React from "react"

import { Section } from "@Components/section"
import { ViewWrapper } from "@Components/wrapper"
import { DefaultText } from "@Components/text"

const Contact = props => <ViewWrapper><Section><DefaultText>xgfootball@gmail.com</DefaultText></Section></ViewWrapper>

export default Contact
