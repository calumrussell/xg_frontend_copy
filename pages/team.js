import React from 'react';
import { connect } from 'react-redux'

import { MatchLogComponent } from '@Containers/team/matchlog'
import { SeasonAvgTeamComponent } from '@Containers/team/seasonavgteam'
import { TeamInfoComponent } from '@Containers/team/teaminfo'
import { SquadAveragesComponent } from '@Containers/team/squadaverages'
import { RollingTeamAveragesComponent } from '@Containers/team/rollingteamaverage'
import { UpcomingMatchesComponent } from '@Containers/team/upcoming'
import { LatestSeasonShotsComponent } from '@Containers/team/latestseasonshots'

import { RequiresData } from "@Components/monad"
import { Panel } from "@Components/panel"
import { ViewWrapper } from "@Components/wrapper"
import { Tabs } from "@Components/tabs"
import { tokenParser } from '@Services/token'

const Summary = props => {
  const { data } = props
  return (
    <span>
      <Panel
        title={'Season Averages'}
        requiredData={data.seasonAverages}>
        <SeasonAvgTeamComponent {...props} />
      </Panel>
      <Panel
        title={'Match Log'}
        requiredData={data.teamStats}>
        <MatchLogComponent {...props} />
      </Panel>
      <Panel
        title={'Squad Averages'}
        requiredData={data.squadAverages}>
        <SquadAveragesComponent {...props} />
      </Panel>
      <Panel
        title={'Upcoming Matches'}
        requiredData={data.upcomingMatches}>
        <UpcomingMatchesComponent {...props} />
      </Panel>
    </span>
  )
}

class Team extends React.Component {

  state = {
    active: 'Summary'
  }

  switchActive = active => this.setState({ active })

  render() {
    const { id } = this.props
    const { active } = this.state
    return (
      <ViewWrapper>
        <RequiresData data={id}>
          <RequiresData.WhenTrue>
            <TeamInfo {...this.props} />
          </RequiresData.WhenTrue>
          <RequiresData.Default><div /></RequiresData.Default>
        </RequiresData>
        <Tabs
          choices={['Summary', 'Shots', 'Rolling Averages']}
          switchActive={this.switchActive}
          active={active} />
        {
          active == 'Summary'
            ? <Summary {...this.props} />
            : active == 'Shots'
              ? <LatestSeasonShotsComponent {...this.props} />
              : active == 'Rolling Averages'
                ? <RollingTeamAverageComponent {...this.props} />
                : null
        }
      </ViewWrapper>
    )
  }
}

import {
  getLatestSeasonTeamStatsByTeam,
  getPlayerAveragesByTeam,
  getTeamAveragesByTeam,
  getTeamLeaguePercentileByTeam,
  getUpcomingMatchesByTeam,
} from '@Services/api'

Team.getInitialProps = async (ctx) => {

  const token = await tokenParser(ctx)

  const {
    id,
    name
  } = ctx.query

  const [
    teamStats,
    squadAverages,
    seasonAverages,
    leaguePercentile,
    upcomingMatches,
  ] = await Promise.all([
    getLatestSeasonTeamStatsByTeam(id, token),
    getPlayerAveragesByTeam(id, token),
    getTeamAveragesByTeam(id, token),
    getTeamLeaguePercentileByTeam(id, token),
    getUpcomingMatchesByTeam(id, token),
  ])

  return {
    id,
    name,
    data: {
      teamStats,
      squadAverages,
      seasonAverages,
      leaguePercentile,
      upcomingMatches,
    }
  }
}

const mapStateToProps = (state) => {
  const { showDK } = state.draftkings
  return {
    showDK
  }
}

export default connect(mapStateToProps)(Team) 
