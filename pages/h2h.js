import React from 'react'

import { SummaryComponent } from '@Containers/h2h/summary'
import { SquadsComponent } from '@Containers/h2h/squads'
import { PredictionComponent } from '@Containers/h2h/prediction'
import { HeaderComponent } from '@Containers/h2h/header'
import { MaybeData } from "@Components/monad"
import { Loader } from "@Components/loader"
import { Section } from "@Components/section"
import { Tabs } from "@Components/tabs"
import { ViewWrapper } from "@Components/wrapper"
import { DefaultText } from "@Components/text"

class H2H extends React.Component {

  state = {
    active: 'L10 Avg'
  }

  switchActive = (active) => this.setState({ active })

  render() {
    const { active } = this.state
    const { data } = this.props

    return (
      <ViewWrapper>
        <Section>
          <HeaderComponent {...this.props} />
          <Tabs
            choices={['L10 Avg', 'Model', 'Squads']}
            switchActive={this.switchActive}
            active={active} />

          <MaybeData data={data.homeTeamStats}>
            <MaybeData.WhenTrue>
              {
                active == 'L10 Avg'
                  ? <SummaryComponent {...data} />
                  : active == 'Model'
                    ? <PredictionComponent {...data} />
                    : active == 'Squads'
                      ? <SquadsComponent {...data} home={this.props.home} away={this.props.away} />
                      : null
              }
            </MaybeData.WhenTrue>
            <MaybeData.Default><Loader /></MaybeData.Default>
            <MaybeData.WhenEmpty><DefaultText>Data not found</DefaultText></MaybeData.WhenEmpty>
          </MaybeData>
        </Section>
      </ViewWrapper>
    )
  }
}

import {
  getLatestSeasonTeamStatsByTeam,
  getPlayerAveragesByTeam,
  getMatchSummary,
  getMatchPrediction
} from "@Services/api"
import { tokenParser } from "@Services/token"

H2H.getInitialProps = async ctx => {

  const token = await tokenParser(ctx)

  const {
    homeid,
    awayid,
    home,
    away,
    id
  } = ctx.query

  const [
    homeTeamStats,
    awayTeamStats,
    homeTeamPlayerAvg,
    awayTeamPlayerAvg,
    matchPrediction,
    matchSummary
  ] = await Promise.all([
    getLatestSeasonTeamStatsByTeam(homeid, token),
    getLatestSeasonTeamStatsByTeam(awayid, token),
    getPlayerAveragesByTeam(homeid, token),
    getPlayerAveragesByTeam(awayid, token),
    getMatchPrediction(id, token),
    getMatchSummary(id, token)
  ])

  return {
    id,
    homeid,
    awayid,
    home,
    away,
    data: {
      homeTeamStats,
      awayTeamStats,
      homeTeamPlayerAvg,
      awayTeamPlayerAvg,
      matchPrediction,
      matchSummary
    }
  }

}

export default H2H
