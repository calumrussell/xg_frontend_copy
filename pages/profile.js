import React from "react"

import SubscriptionDetails from '@Containers/Stripe/Subscription'

export const Subscription = (props) => {
  return (
    <React.Fragment>
      <SubscriptionDetails />
    </React.Fragment>
  )
}