import React from 'react'

import CreateForm from '@Containers/User/Create'
import { ViewWrapper } from "@Components/wrapper"

const CreateUser = ({ redirect }) => <ViewWrapper><CreateForm redirect={redirect} /></ViewWrapper>

CreateUser.getInitialProps = async ctx => {

  const {
    redirect
  } = ctx.query

  const redirectLogic = redirect == undefined
    ? '/'
    : redirect

  return {
    redirect: redirectLogic
  }

}

export default CreateUser
