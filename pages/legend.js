import React from "react"
import { connect } from "react-redux"

import { SubTitle, DefaultText } from "@Components/text"
import { Section } from "@Components/section"
import { ViewWrapper } from "@Components/wrapper"
import { titleConverter, headerConverter } from "@Functions/transform"

const Legend = (props) => {
  return (
    <ViewWrapper>
      <Section>
        {
          Object.keys(props.category).map((cat, i) => {
            let catObj = props.category[cat]
            return (
              <div key={i}>
                <SubTitle>{catObj.name}</SubTitle>
                {
                  catObj.categories.map((v, i) => <div key={i}><DefaultText>{`${headerConverter(v)}: ${titleConverter(v)}`}</DefaultText></div>)
                }
              </div>
            )
          })
        }
      </Section>
    </ViewWrapper>
  )
}
const mapStateToProps = ({ category }) => ({ category })
export default connect(mapStateToProps)(Legend)
