import React from "react"
import Link from "next/link"

import { Section } from "@Components/section"
import { Accordion } from "@Components/accordion"
import { ViewWrapper } from "@Components/wrapper"

const DefRatingExplain = {
  title: "Why does X have a defensive rating of Y? That is wrong.",
  text: <div>
    <p>Calculating defensive ratings is difficult. A team's defensive performance is quite a large function of the opposing team: both their skill and the strategy they adopt.</p>
    <p>We have made some alterations to our model recently that have tried to take this into account. This can result in ratings that seem out of line with the score but should better represent a team's overall performance adjusted for the quality of opponent.</p>
  </div>
}

const StatExplain = {
  title: "What does X stat mean?",
  text: <div>
    <p>If you are on desktop, the meaning of any category can be found by hovering your cursor over the column title</p>
    <p>If you are on mobile, we provide a <Link to='/legend'>Legend</Link>.</p>
  </div>
}

const KeyPass = {
  title: "What is a Key Pass? How is it calculated?",
  text: <div>
    <p>The standard defintion of key pass is any pass which leads to a shot that isn't scored (i.e. an assist) or blocked.</p>
    <p>We don't use this definition and add back shots were scored. To get to a comparable definition, you need to substract assists from Key Passes</p>
  </div>
}

const Faq = props => {
  return (
    <ViewWrapper>
      <Section>
        <Accordion {...StatExplain} />
        <Accordion {...KeyPass} />
        <Accordion {...DefRatingExplain} />
      </Section>
    </ViewWrapper>
  )
}

export default Faq
