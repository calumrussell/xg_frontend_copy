import React from "react"
import { connect } from 'react-redux'
import styled from "styled-components"

import { Section } from "@Components/section"
import { Button } from "@Components/button"
import { ViewWrapper } from "@Components/wrapper"
import { supportedCategories } from '@Functions/transform'
import { toggleDK } from '@Redux/actions/Draftkings'
import { addCategory, deleteCategory, deleteDK, addDK } from '@Redux/actions/Category'
import { colorScheme } from '@Theme'

const CategoryAdderWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  button{
    padding: 0px;
    font-weight: bold;
  }
  img {
    cursor:pointer;
    height:30px;
    margin-left:10px;
    margin-bottom:5px;
  }
`

const CategoryAdder = ({ category, handleSelect, addCategory }) => {
  return (
    <CategoryAdderWrapper>
      <label>
        <b>Pick Category</b>
        <select value={category}
          onChange={handleSelect}>
          {
            Object.keys(supportedCategories)
              .map((v, i) => <option key={i} value={v}>{supportedCategories[v]['long']}</option>)
          }
        </select>
      </label>
      <img onClick={addCategory} src='https://xg.football/static/img/icons/plus.svg' />
    </CategoryAdderWrapper>
  )
}

const Panel = styled.div`
  margin: 0 auto;
  max-width: 600px;
  margin-top: 2rem;
  border-top: 2px solid ${colorScheme.bluegrey[700]};
  background: ${colorScheme.grey[200]};
  padding:20px;
  label{
    display: flex;
    flex-direction: column;
    max-width:50%;
  }
`

const FormWrapper = styled.div`
  padding: 10px;
`

const DeleteWrapper = styled.div`
  display: flex;
  align-items: center;
  img{
    cursor:pointer;
    height:30px;
    margin-right:10px;
  }
`

class Settings extends React.Component {

  state = {
    title: '',
    categories: [],
    category: '',
    error: ''
  }

  handleTitle = (event) => {
    this.setState({ title: event.target.value })
  }

  handleSelect = (event) => {
    if (event.target.value != '') {
      this.setState({ category: event.target.value })
    }
  }

  addCategory = (event) => {
    event.preventDefault()
    this.setState({ categories: [...this.state.categories, this.state.category], category: '' })
  }

  deleteCategory = event => {
    event.preventDefault()
    this.props.deleteCategory(event.target.dataset.value)
  }

  createNewCategory = (event) => {
    event.preventDefault()
    //call Redux function adding
    if (this.state.title === '') {
      this.setState({ error: 'Title cannot be empty' })
    } else {
      this.props.addCategory(this.state.title, this.state.categories)
      this.setState({ error: '', title: '', category: [], category: '' })
    }
  }

  getCustomCategories = categories => {
    const defaultCategories = ['Off', 'Def',
      'OffTeam', 'DefTeam', 'LeagueTable',
      'Passes', 'Shots', 'Touches']
    return categories.filter(v => !defaultCategories.includes(v))
  }

  render() {
    return (
      <ViewWrapper>
        <Section>
          <Panel>
            <h1>Toggle DraftKings data</h1>
            <Button onClick={this.props.toggleDK}>ToggleDK</Button>
          </Panel>
          <Panel>
            <h1>Add New Category</h1>
            <p>This creates a new category of stats that will be available across components in the Match,
            Player, and Team views.</p>
            <form>
              <FormWrapper>
                <label>
                  <b>Title</b>
                  <input type="text"
                    value={this.state.title}
                    onChange={this.handleTitle}
                  />
                </label>
                <CategoryAdder
                  category={this.state.category}
                  handleSelect={this.handleSelect}
                  addCategory={this.addCategory} />
                {
                  this.state.categories.length === 0
                    ? null
                    : <div>
                      <p>Selected Categories:</p>
                      <p>{this.state.categories.join(', ')}</p>
                    </div>
                }
              </FormWrapper>
              {
                this.state.error != ''
                  ? <p>{this.state.error}</p>
                  : null
              }
              <Button onClick={this.createNewCategory}>Create new category</Button>
            </form>
            <div>
              <h2>Custom Categories</h2>
              {
                this.getCustomCategories(Object.keys(this.props.category))
                  .map((v, i) => <DeleteWrapper key={i}><img onClick={this.deleteCategory} data-value={v} src='https://xg.football/static/img/icons/minus.svg' />{v}</DeleteWrapper>)
              }
            </div>
          </Panel>
        </Section>
      </ViewWrapper>
    )
  }
}

const mapStateToProps = ({ category }) => {
  return { category }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleDK: () => dispatch(toggleDK()),
    addCategory: (title, categories) => dispatch(addCategory(title, categories)),
    deleteCategory: title => dispatch(deleteCategory(title)),
    deleteDK: () => dispatch(deleteDK()),
    addDK: () => dispatch(addDK())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings) 
