import React from "react"

import { Section } from "@Components/section"
import { Accordion } from "@Components/accordion"
import { SectionTitle } from "@Components/text"
import { ApiDefinition } from "@Components/apidefinition"
import { ViewWrapper } from "@Components/wrapper"

const QueryExplain = {
  title: "How to query the API",
  text: <div>
    <p>All of the data that we use on the website is available publicly through our API</p>
    <p>We will be providing more detail on the routes that can be queried but right now
      we would direct you to the Developer Tools feature of your browser. Using this, you can view
      the requests made by your browser to our API.</p>
    <p>Code showing how to query a certain route: <a href='https://www.codepile.net/pile/QZzrzLZL'>Here</a></p>
  </div>
}

const UserExplain = {
  title: "How do I login and access subscriber-only routes?",
  text: <div>
    <p>If you are a subscriber, you can login programmatically and request data.</p>
    <p>Additionally, there are certain routes that index the main data types that are available
      to subscribers only.</p>
    <p>Code showing how to login, request a token, and make an authenticated request: <a href='https://www.codepile.net/pile/QZzrzLZL'>Here</a></p>
  </div>
}

const ApiDoc = (props) => {
  return (
    <ViewWrapper>
      <Section>
        <SectionTitle>API Documentation</SectionTitle>
        <Accordion {...QueryExplain} />
        <Accordion {...UserExplain} />
        <ApiDefinition />
      </Section>
    </ViewWrapper>
  )
}

export default ApiDoc
