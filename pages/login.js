import React from 'react'

import { ViewWrapper } from "@Components/wrapper"
import { LoginForm } from '@Containers/user/login'

const Login = ({ redirect }) => {
  return (
    <ViewWrapper>
      <LoginForm redirect={redirect} />
    </ViewWrapper>
  )
}

Login.getInitialProps = ({ query }) => {
  const redirect = query.redirect
  return {
    redirect
  }
}

export default Login
