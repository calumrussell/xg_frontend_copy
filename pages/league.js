import React from 'react';
import orderBy from 'lodash.orderby'
import Router from 'next/router'

import { Header } from '@Containers/league/header'
import { RecentMatches } from '@Containers/league/recentmatches'
import { LeagueTable } from '@Containers/league/leaguetable'
import { TopPlayers } from '@Containers/league/topplayers'
import { SeasonSelector } from '@Containers/league/seasonselector'
import { RequiresData } from "@Components/monad"
import { Panel } from "@Components/panel"
import { ViewWrapper } from "@Components/wrapper"
import { Section } from "@Components/section"

class League extends React.Component {

  getSeasonIdx = () => {
    const { seasons } = this.props.data
    const findSeasonId = v => v.seasonid == this.props.seasonid
    return seasons.findIndex(findSeasonId)
  }

  seasonForward = () => {
    const { seasons } = this.props.data
    if (this.getSeasonIdx() < (seasons.length - 1)) {
      const { seasonid, tournamentid } = seasons[this.getSeasonIdx() + 1]
      Router.pushRoute(`/league/${this.props.name}/tournament/${tournamentid}/season/${seasonid}`)
    }
  }

  seasonBackward = () => {
    const { seasons } = this.props.data
    if (this.getSeasonIdx() > 0) {
      const { seasonid, tournamentid } = seasons[this.getSeasonIdx() - 1]
      Router.pushRoute(`/league/${this.props.name}/tournament/${tournamentid}/season/${seasonid}`)
    }
  }

  render() {
    const { name, data, year } = this.props
    return (
      <ViewWrapper>
        <RequiresData data={name}>
          <RequiresData.WhenTrue>
            <Section>
              <Header {...this.props} />
              <SeasonSelector
                current={year}
                moveForward={this.seasonForward}
                moveBackward={this.seasonBackward} />
            </Section>
          </RequiresData.WhenTrue>
          <RequiresData.Default><div /></RequiresData.Default>
        </RequiresData>
        <Panel
          title={'Schedule'}
          requiredData={data.sortedMatches}>
          <RecentMatches
            {...this.props} />
        </Panel>
        <Panel
          title={'Season Leaders'}
          requiredData={data.topPlayers}>
          <TopPlayers
            {...this.props} />
        </Panel>
        <Panel
          title={'League Table'}
          requiredData={data.leagueTable}>
          <LeagueTable
            {...this.props} />
        </Panel>
      </ViewWrapper>
    )
  }
}

import {
  getMatchesBySeason,
  getLeagueTableBySeason,
  getTopPlayersBySeason,
  getSeasonsByTournament
} from '@Services/api'
import { tokenParser } from '@Services/token'

League.getInitialProps = async ctx => {

  const token = await tokenParser(ctx)

  const {
    name,
    tournamentid,
    seasonid
  } = ctx.query

  const [
    leagueTable,
    matches,
    topPlayers,
    seasons
  ] = await Promise.all([
    getLeagueTableBySeason(seasonid, token),
    getMatchesBySeason(seasonid, token),
    getTopPlayersBySeason(seasonid, token),
    getSeasonsByTournament(tournamentid, token)
  ])

  const sortedMatches = orderBy(matches, 'starttime', 'asc')
  const sortedSeasons = orderBy(seasons, 'year', 'asc')

  return {
    seasonid,
    tournamentid,
    year: seasons.find(v => v.seasonid == seasonid).year,
    name,
    data: {
      leagueTable,
      matches: sortedMatches,
      sortedMatches: sortedMatches.filter(m => m.seasonid == seasonid),
      topPlayers,
      seasons: sortedSeasons
    }
  }
}

export default League
