FROM node:lts

WORKDIR ./
COPY . .
RUN yarn install
RUN yarn run build