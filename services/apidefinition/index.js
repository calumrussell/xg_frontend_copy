/*
  Group:
    Route:
      title
      description
      route
      link
      params
*/

const baseUrl = "https://api.xg.football"

export const apiDefinition = [
  {
    groupName: "Match",
    routes: [
      {
        title: "Basic Match Info",
        description: "Basic info for that match such as who played, where they played, and the score for season and tournaments that the user has access to.",
        route: "/match/match/<matchid>",
        link: `${baseUrl}/match/match/85910`,
        params: "None"
      },
      {
        title: "Player-level stats",
        description: "Stats by player for the given match. Only for season and tournaments that the user has access to.",
        route: "/match/playerstats/<matchid>",
        link: `${baseUrl}/match/playerstats/85910`,
        params: "None"
      },
      {
        title: "Team-level stats",
        description: "Stats by team for the given match. Only for season and tournaments that the user has access to.",
        route: "/match/teamstats/<matchid>",
        link: `${baseUrl}/match/teamstats/85910`,
        params: "None"
      },
      {
        title: "Prediction",
        description: "Returns our model prediction across the main bet categories. Only for seasons and tournaments that the user has access to.",
        route: "/match/prediction/<matchid>",
        link: `${baseUrl}/match/prediction/85910`,
        params: "None"
      },
      {
        title: "Odds",
        description: "Historical odds for this match leading up to the event start. Only for seasons and tournaments that the user has access to.",
        route: "/match/liveodds/<matchid>",
        link: `${baseUrl}/match/liveodds/85910`,
        params: "None"
      }
    ]
  },
  {
    groupName: "Team",
    routes: [
      {
        title: "Team averages",
        description: "All P90 averages for a given team over every season and tournament that the user has access to.",
        route: "/team/averages/<teamid>",
        link: `${baseUrl}/team/averages/2`,
        params: "None"
      },
      {
        title: "Team rolling averages",
        description: "Rolling P90 Averages for a given team over all tournaments",
        route: "/team/averagesrolling/<teamid>",
        link: `${baseUrl}/team/averagesrolling/2`,
        params: "None"
      },
      {
        title: "Player averages for a given team",
        description: "Averages for all players currently registered with a given team",
        route: "/team/playeraverages/<teamid>",
        link: `${baseUrl}/team/playeraverages/2`,
        params: "None"
      },
      {
        title: "Team rank",
        description: "Rank across all stats over every season and tournament that the user has access to",
        route: "/team/leaguerank/<teamid>",
        link: `${baseUrl}/team/leaguerank/2`,
        params: "None"
      },
      {
        title: "Team percentile",
        description: "Percentile across all stats over every season and tournament that the user has access to",
        route: "/team/leaguepercentile/<teamid>",
        link: `${baseUrl}/team/leaguepercentile/2`,
        params: "None"
      },
      {
        title: "Team current league rank",
        description: "Rank across all stats for the latest season",
        route: "/team/currentleaguerank/<teamid>",
        link: `${baseUrl}/team/currentleaguerank/2`,
        params: "None"
      },
      {
        title: "Next five matches",
        description: "Next five matches for this team",
        route: "/team/upcomingmatches/<teamid>",
        link: `${baseUrl}/team/upcomingmatches/2`,
        params: "None"
      },
      {
        title: "Team match stats current season",
        description: "All match stats for a team over the current season",
        route: "/team/latestseasonstats/<teamid>",
        link: `${baseUrl}/team/latestseasonstats/2`,
        params: "None"
      }
    ]
  },
  {
    groupName: "Matches",
    routes: [
      {
        title: "Recent",
        description: "Returns all matches within the last three days. Only for the season and tournaments that the user has access to.",
        route: "/matches/recent",
        link: `${baseUrl}/matches/recent`,
        params: "None"
      },
      {
        title: "Upcoming",
        description: "Returns all matches within the next three days. Only for the seasons and tournaments that the user has access to.",
        route: "/matches/upcoming",
        link: `${baseUrl}/matches/upcoming`,
        params: "None"
      }
    ]
  },
  {
    groupName: "Player",
    routes: [
      {
        title: "Player averages",
        description: "Get P90 player averages for every season and tournament that the user has access to.",
        route: "/player/averages/<playerid>",
        link: `${baseUrl}/player/averages/19686`,
        params: "None"
      },
      {
        title: "Player percentile",
        description: "Get percentile for all stats across all players in a given season/tournament combination",
        route: "/player/percentile/<playerid>",
        link: `${baseUrl}/player/percentile/19686`,
        params: "None"
      },
      {
        title: "Upcoming matches",
        description: "Next five matches for player across all teams and tournaments that the user has access to.",
        route: "/player/upcomingmatches/<playerid>",
        link: `${baseUrl}/player/upcomingmatches/19686`,
        params: "None"
      },
      {
        title: "Last team",
        description: "The last non-international team that this player is registered with.",
        route: "/player/lastteam/<playerid>",
        link: `${baseUrl}/player/lastteam/19686`,
        params: "None"
      },
      {
        title: "Team averages",
        description: "Gets the team averaages for the last non-internatoinal team that the player is registered with, across all season and tournaments that the user has access to.",
        route: "/player/info/<playerid>",
        link: `${baseUrl}/player/info/19686`,
        params: "None"
      },
      {
        title: "Match stats",
        description: "Match stats for the most reason season across all tournaments and teams.",
        route: "/player/latestseasonstats/<playerid>",
        link: `${baseUrl}/player/latestseasonstats/19686`,
        params: "None"
      }
    ]
  },
  {
    groupName: "Season",
    routes: [
      {
        title: "Season averages",
        description: "Averages for all matches within a given season for seasons that the user has access to.",
        route: "/season/averages/<seasonid>",
        link: `${baseUrl}/season/averages/357`,
        params: "None"
      },
      {
        title: "Top players",
        description: "Returns the sum for stats for all players in a given season. Note that this is nonsensical for some categories. Only for seasons that the user has access to.",
        route: "/season/topplayers/<seasonid>",
        link: `${baseUrl}/season/topplayers/357`,
        params: "None"
      },
      {
        title: "League Table",
        description: "Returns the sum for stats for all teams in a given season. Note that this is nonsensical for some categories. Only for seasons that the user has access to.",
        route: "/season/leaguetable/<seasonid>",
        link: `${baseUrl}/season/leaguetable/357`,
        params: "None"
      },
      {
        title: "Matches",
        description: "Returns basic match info for all matches in a given season. Only for seasons that the user has access to.",
        route: "/season/matches/<seasonid>",
        link: `${baseUrl}/season/matches/357`,
        params: "None"
      }
    ]
  },
  {
    groupName: "Tournament",
    routes: [
      {
        title: "Current matches",
        description: "Get basic match info for a given tournament. Only those tournaments that the user has access to.",
        route: "/tournament/currentmatches/<tournamentid>",
        link: `${baseUrl}/tournament/currentmatches/1`,
        params: "None"
      },
      {
        title: "Current league table",
        description: "Get league table for the latest season for a given tournament. Only those seasons that the user has access to.",
        route: "/tournament/currentleaguetable/<tournamentid>",
        link: `${baseUrl}/tournament/currentleaguetable/1`,
        params: "None"
      },
      {
        title: "Current top players",
        description: "Get the sum of player season stats for the latest seasons for a given tournament. Only those seasons that the user has access to.",
        route: "/tournament/currenttopplayers/<tournamentid>",
        link: `${baseUrl}/tournament/currenttopplayers/1`,
        params: "None"
      },
      {
        title: "Seasons",
        description: "Get all available seasons for a given tournament. Only those tournaments that the user has access to.",
        route: "/tournament/seasons/<tournamentid>",
        link: `${baseUrl}/tournament/seasons/1`,
        params: "None"
      },
      {
        title: "Current matches",
        description: "Get basic match info for a given tournament. Only those tournaments that the user has access to.",
        route: "/tournament/currentmatches/<tournamentid>",
        link: `${baseUrl}/tournament/currentmatches/1`,
        params: "None"
      },
      {
        title: "League table",
        description: "Get league table for a given tournament for a given year. Only those seasons that the user has access to.",
        route: "/tournament/leaguetable/<tournamentid>/year/<year>",
        link: `${baseUrl}/tournament/leaguetable/1/year/2018`,
        params: "None"
      },
      {
        title: "All",
        description: "Get all tournaments that are available to the user.",
        route: "/tournament/all",
        link: `${baseUrl}/tournament/all`,
        params: "None"
      }
    ]
  },
  {
    groupName: "Tournaments",
    routes: [
      {
        title: "Current tournaments",
        description: "Get current tournaments that the user has access to.",
        route: "/tournaments/current",
        link: `${baseUrl}/tournament/current`,
        params: "None"
      },
      {
        title: "All",
        description: "Get all tournaments that the user has access to.",
        route: "/tournaments/all",
        link: `${baseUrl}/tournaments/all`,
        params: "None"
      },
    ]
  }
]
