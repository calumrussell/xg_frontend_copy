export const endpoint = 'https://calumrussell.prismic.io/api/v2';

export const linkResolver = (doc) => {
  if(doc.type === 'blog_post') return '/blog/' + doc.uid;
  return '/';
}
