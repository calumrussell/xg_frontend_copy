import jwt_decode from 'jwt-decode'
import Cookies from 'universal-cookie'

import { createMessage } from './Message'
import sortby from 'lodash.sortby'
import { apiPlain, getCurrentTournaments, getLiveTournaments } from '@Services/api'

export const LOGIN_USER = 'LOGIN_USER'
export const LOGOUT_USER = 'LOGOUT_USER'
export const SUBSCRIBE_USER = 'SUBSCRIBE_USER'
export const CREATE_USER = 'CREATE_USER'
export const CONFIRM_USER = 'CONFIRM_USER'
export const UPDATE_TOURNS = 'UPDATE_TOURNS'

const checkForError = resp => {
  if (resp.ok) return resp
  return resp.json().then(r => Promise.reject(new Error(r.message)))
}

export const loginAction = isSubscribed => dispatch => dispatch({ type: 'LOGIN_USER', isSubscribed })

export const loginUser = (email, password) => dispatch => {

  const request = {
    endpoint: '/user/login',
    type: 'POST',
    dataobj: { email, password }
  }

  apiPlain(request)
    .then(checkForError)
    .then(r => r.json())
    .then(json => {
      const decodedAccess = jwt_decode(json.accessToken)
      dispatch({ type: "LOGIN_USER", isSubscribed: decodedAccess['cognito:groups'].includes("Basic") || decodedAccess['cognito:groups'].includes("Admin") })

      const cookies = new Cookies()
      const refreshOptions = { maxAge: 518400, path: '/' }
      const otherOptions = { maxAge: 3300, path: '/' }
      cookies.set('xg_uat', json.accessToken, otherOptions)
      cookies.set('xg_uit', json.idToken, otherOptions)
      cookies.set('xg_urt', json.refreshToken, refreshOptions)

      dispatch(updateTourns(json.accessToken))
      dispatch(createMessage("Logged in", 1))
    })
    .catch(err => dispatch(createMessage(err.message, -1)))
}

export const createUser = (email, password) => dispatch => {

  const request = {
    endpoint: '/user/create',
    type: 'POST',
    dataobj: { email, password }
  }

  apiPlain(request)
    .then(checkForError)
    .then(resp => resp.text())
    .then(text => {
      dispatch({ type: "CREATE_USER" })
      dispatch(createMessage('User created. Check email for verification code.', 1))
    })
    .catch(err => dispatch(createMessage(err.message, -1)))
}

export const confirmUser = (email, confirmationCode) => dispatch => {

  const request = {
    endpoint: '/user/confirm',
    type: 'POST',
    dataobj: { email, confirmationCode }
  }

  apiPlain(request)
    .then(checkForError)
    .then(resp => resp.text())
    .then(text => {
      dispatch({ type: "CONFIRM_USER" })
      dispatch(createMessage('User confirmed', 1))
    })
    .catch(err => dispatch(createMessage(err.message, -1)))
}

export const logoutUser = accessToken => dispatch => {

  const request = {
    endpoint: '/user/logout',
    type: 'POST',
    dataobj: { accessToken }
  }

  apiPlain(request)
    .then(checkForError)
    .then(resp => {
      const cookies = new Cookies()
      cookies.remove('xg_uat')
      cookies.remove('xg_uit')
      cookies.remove('xg_urt')
      dispatch({ type: 'LOGOUT_USER' })
      dispatch(createMessage("Logged out", 1))
      updateTourns(undefined)
    })
    .catch(err => dispatch(createMessage("Logout failed", -1)))
}

export const updateTourns = token => async dispatch => {
  const [
    liveTourns,
    currentTourns,
  ] = await Promise.all([
    getLiveTournaments(token),
    getCurrentTournaments(token)
  ])

  dispatch({
    type : 'UPDATE_TOURNS',
    liveTourns: sortby(liveTourns, ['regionname','countrycode']),
    currentTourns: sortby(currentTourns, ['regionname', 'countrycode'])
  })
}
