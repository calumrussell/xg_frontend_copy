export const TOGGLE_NAV = 'TOGGLE_NAV'

export const toggleNav = () => dispatch => {
  dispatch({ type: "TOGGLE_NAV" })
}