export const ADD_CATEGORY = 'ADD_CATEGORY'
export const DELETE_CATEGORY = 'DELETE_CATEGORY'
export const ADD_DK = 'ADD_DK'
export const DELETE_DK = 'DELETE_DK'

export const addCategory = (title, categories) => dispatch => {
  const newCategory = {
    name: title,
    categories,
    components: ['MatchLogPlayer', 'SeasonAvgPlayer', 'SquadAvgTeam', 'MatchLogTeam', 'SeasonAvgTeam']
  }
  dispatch({ type: 'ADD_CATEGORY', newCategory })
}

export const getComponentDataCategories = (statGroup, component) => (dispatch, getState) => {
  const { category } = getState()

  const statGroupNames = Object.keys(category)
    .filter(cat => category[cat].components.includes(component))
    .map(cat => category[cat].name)

  const dataKeys = statGroup != null ? category[statGroup].categories : []
  return {
    statGroupNames,
    dataKeys
  }
}

export const deleteCategory = title => dispatch => dispatch({ type: 'DELETE_CATEGORY', title })
export const addDK = () => dispatch => dispatch({ type: "ADD_DK" })
export const deleteDK = () => dispatch => dispatch({ type: "DELETE_DK" })
export const getCategoriesObject = () => (dispatch, getStore) => console.log(getStore())