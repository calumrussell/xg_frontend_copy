export const CREATE_MESSAGE = 'CREATE_MESSAGE'
export const CLEAR_MESSAGE = 'CLEAR_MESSAGE'

export const createMessage = (text, mood) => dispatch => {
  dispatch({ type: 'CREATE_MESSAGE', text, mood })
  setTimeout(() => dispatch({ type: 'CLEAR_MESSAGE' }), 5000)
}