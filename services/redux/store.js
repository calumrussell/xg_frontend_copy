import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'

import { draftkings, initialDKState } from './reducers/Draftkings'
import { message, initialMessageState } from './reducers/Message'
import { user, initialUserState } from './reducers/User'
import { category, initialCategoryState } from './reducers/Category'
import { nav, initialNavState } from './reducers/Nav'

const combinedInitialState = {
  draftkings: initialDKState,
  message: initialMessageState,
  user: initialUserState,
  category: initialCategoryState,
  nav: initialNavState
}

const reducers = combineReducers({
  draftkings,
  message,
  user,
  category,
  nav
})

export const initializeStore  = (initialState = combinedInitialState) => {
  return createStore(reducers, initialState, applyMiddleware(thunkMiddleware))
}
  
