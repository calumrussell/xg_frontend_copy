import { CREATE_MESSAGE, CLEAR_MESSAGE } from '../actions/Message'

export const initialMessageState = {
  text: null,
  mood: null,
}

export const message = (state = initialMessageState, action) => {
  switch (action.type) {
    case CREATE_MESSAGE:
      return Object.assign({}, state, {
        text: action.text,
        mood: action.mood
      })
    case CLEAR_MESSAGE:
      return Object.assign({}, state, {
        text: null,
        mood: null
      })
    default:
      return state
  }
}

