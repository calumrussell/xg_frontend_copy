import { TOGGLE_NAV } from '../actions/Nav'

export const initialNavState = {
  isOpen: false
}

export const nav = (state = initialNavState, action) => {
  switch (action.type) {
    case TOGGLE_NAV:
      return Object.assign({}, state, {
        isOpen: !state.isOpen
      })
    default:
      return state
  }
}

