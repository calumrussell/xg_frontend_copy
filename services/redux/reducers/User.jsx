import { 
  LOGIN_USER, 
  LOGOUT_USER,
  SUBSCRIBE_USER,
  CREATE_USER,
  CONFIRM_USER,
  UPDATE_TOURNS 
} from '../actions/User'

export const initialUserState = {
  isLoggedIn: false,
  isSubscribed: false,
  isVerified: false,
  isCreated: false,
  liveTourns: [],
  currentTourns: []
}
//Every user has access to a list of tournaments
//this defaults to the free tier

export const user = (state = initialUserState, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return Object.assign({}, state, {
        isLoggedIn: true,
        isSubscribed: action.isSubscribed
      })
    case LOGOUT_USER:
      return Object.assign({}, state, {
        isLoggedIn: false,
        isSubscribed: false
      })
    case SUBSCRIBE_USER:
      return Object.assign({}, state, {
        isSubscribed: true
      })
    case CREATE_USER:
      return Object.assign({}, state, {
        isCreated: true
      })
    case CONFIRM_USER:
      return Object.assign({}, state, {
        isVerified: true
      })
    case UPDATE_TOURNS:
      return Object.assign({}, state, {
        liveTourns: action.liveTourns,
        currentTourns: action.currentTourns
      })
    default:
      return state
  }
}

