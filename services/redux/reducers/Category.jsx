import {
  ADD_CATEGORY,
  DELETE_CATEGORY,
  ADD_DK,
  DELETE_DK
} from "../actions/Category";

const Off = {
  name: "Off",
  categories: ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal',
    'passkey', 'assist'],
  components: ['MatchLogPlayer', 'SeasonAvgPlayer', 'SquadAvgTeam']
}

const OffTeam = {
  name: "OffTeam",
  categories: Off.categories.concat('offrating'),
  components: ['MatchLogTeam', 'SeasonAvgTeam']
}

const Def = {
  name: "Def",
  categories: ['foulcommitted', 'tacklewon', 'interceptiontotal',
    'clearanceeffective', 'duelaerialwon', 'yellowcard', 'redcard'],
  components: ['MatchLogPlayer', 'SeasonAvgPlayer', 'SquadAvgTeam']
}

const DefTeam = {
  name: "DefTeam",
  categories: ['goalsagainst', 'oppxg', ...Def.categories, 'defrating'],
  components: ['MatchLogTeam', 'SeasonAvgTeam']
}

const Shots = {
  name: "Shots",
  categories: ['shotbox', 'shotsixyard', 'shotoutsidebox', 'shothead', 'shotsetpiece',
    'shotmisshigh', 'shotmisslow'],
  components: ['MatchLogPlayer', 'SeasonAvgPlayer', 'SquadAvgTeam', 'MatchLogTeam', 'SeasonAvgTeam']
}

const Touches = {
  name: "Touches",
  categories: ['touchtotal', 'touchinbox', 'touchleftwing', 'touchrightwing',
    'touchcenter'],
  components: ['MatchLogPlayer', 'SeasonAvgPlayer', 'SquadAvgTeam', 'MatchLogTeam', 'SeasonAvgTeam']
}

const Passes = {
  name: "Passes",
  categories: ['passtotal', 'passshorttotal', 'passlongtotal', 'passcrosstotal',
    'passthroughballtotal', 'passintobox', 'passintofinalthird'],
  components: ['MatchLogPlayer', 'SeasonAvgPlayer', 'SquadAvgTeam', 'MatchLogTeam', 'SeasonAvgTeam']
}

const LeagueTable = {
  name: "LeagueTable",
  categories: ['matchesplayed', 'goalsfor', 'goalsagainst', 'goalsdifference', 'xg', 'oppxg', 'points'],
  components: ['LeagueTable']
}

export const initialCategoryState = { Off, OffTeam, Def, DefTeam, Shots, Touches, Passes, LeagueTable }

export const category = (state = initialCategoryState, action) => {
  switch (action.type) {
    case ADD_CATEGORY:
      return {
        ...state,
        [action.newCategory.name]: action.newCategory
      }
    case DELETE_CATEGORY:
      const { [action.title]: value, ...without } = state
      return {
        ...without
      }
    case ADD_DK:
      return {
        ...state,
        Off: {
          ...state.Off,
          categories: [...state.Off.cateogries, 'xg']
        }
      }
    case DELETE_DK:
      return {
        ...state,
        Off: {
          ...state.Off,
          categories: [...state.Off.categories.filter(v => v != 'xg')]
        }
      }
    default:
      return state
  }
}
