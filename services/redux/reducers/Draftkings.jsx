import { TOGGLE_DK } from '../actions/Draftkings'

export const initialDKState = {
  showDK: false
}

export const draftkings = (state = initialDKState, action) => {
  switch (action.type) {
    case TOGGLE_DK:
      return Object.assign({}, state, {
        showDK: !state.showDK
      })
    default:
      return state
  }
}

