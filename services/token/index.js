import Cookies from 'universal-cookie'
import jwt_decode from 'jwt-decode'

import { apiPlain } from '@Services/api'
import { loginAction } from '@Services/redux/actions/User'

const checkIfLoggedIn = reduxStore => reduxStore.getState().user.isLoggedIn == true
const checkIfSubscriber = reduxStore => reduxStore.getState().user.isSubscribed == true
const checkForAccessToken = cookies => cookies.get("xg_uat") == undefined
const checkForRefreshToken = cookies => cookies.get("xg_urt") == undefined
const getRefreshToken = cookies => cookies.get("xg_urt")
const getAccessToken = cookies => cookies.get("xg_uat")
const checkTokenForSubscription = decodedToken => decodedToken['cognito:groups'].includes("Basic") || decodedToken['cognito:groups'].includes("Admin")
const dispatchLoggedIn = reduxStore => reduxStore.dispatch(loginAction(false))
const dispatchSubscribed = reduxStore => reduxStore.dispatch(loginAction(true))

const updateReduxWithUserState = (cookies, reduxStore) => {
  if (reduxStore == undefined) return
  if (!checkForAccessToken(cookies) && !checkIfLoggedIn(reduxStore)) {
    const decodedToken = jwt_decode(getAccessToken(cookies))
    if (checkTokenForSubscription(decodedToken) && !checkIfSubscriber(reduxStore)) {
      //The latter condition should never occur but just to be safe
      dispatchSubscribed(reduxStore)
    }
    else {
      dispatchLoggedIn(reduxStore)
    }
  }
}

const refreshLogic = async cookies => {
  if (checkForAccessToken(cookies)) {
    const refreshToken = getRefreshToken(cookies)
    if (refreshToken === undefined) return undefined
    await refreshTokenFetch(refreshToken)
  }
}

const refreshTokenFetch = async refreshToken => {
  await apiPlain({ endpoint: '/user/refresh', type: 'POST', dataobj: { refreshToken } })
    .then(resp => {
      if (resp.ok) {
        resp.json()
          .then(json => {
            const cookies = new Cookies()
            const options = { maxAge: 3300, path: '/' }
            cookies.set('xg_uat', json.accessToken, options)
            cookies.set('xg_uit', json.idToken, options)

          })
      }
    })
}

export const tokenParser = async (ctx = undefined, triggerRefresh = false) => {
  if (triggerRefresh == true) {
    const cookies = new Cookies(document.cookie)
    const refreshToken = getRefreshToken(cookies)
    await refreshTokenFetch(refreshToken)
    updateReduxWithUserState(cookies, ctx.reduxStore)
    return getAccessToken(cookies)
  } else {
    if (ctx && ctx.req && ctx.req.headers) {
      const cookies = new Cookies(ctx.req.headers.cookie)
      await refreshLogic(cookies)
      updateReduxWithUserState(cookies, ctx.reduxStore)
      return getAccessToken(cookies)
    } else {
      const cookies = new Cookies(document.cookie)
      await refreshLogic(cookies)
      updateReduxWithUserState(cookies, undefined)
      return getAccessToken(cookies)
    }
  }
}
