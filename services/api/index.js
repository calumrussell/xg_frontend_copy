import fetch from 'isomorphic-unfetch'
import Headers from 'fetch-headers'

const baseurl = 'https://api.xg.football'

const apiReq = ({ endpoint, type, dataobj, token }) => {

  const myHeaders = new Headers()
  myHeaders.append('Content-Type', 'application/json')
  if (token) {
    myHeaders.append('Authorization', `Bearer ${token}`)
  }

  let config = {
    method: type,
    headers: myHeaders,
  }

  if (dataobj) {
    config.body = JSON.stringify(dataobj)
  }

  return fetch(baseurl + endpoint, config)
}

export const apiPlain = request => {
  return apiReq(request)
    .then(resp => resp)
    .catch(err => console.log(err))
}

export const apiJson = request => {
  return apiReq(request)
    .then(resp => resp.json())
    .catch(err => console.log(err))
}

export const getRecentMatches = (token = undefined) => apiJson({ endpoint: '/matches/recent', type: 'GET', token })
export const getUpcomingMatches = (token = undefined) => apiJson({ endpoint: '/matches/upcoming', type: 'GET', token })
export const getMatchStatsByMatch = (id, token = undefined) => apiJson({ endpoint: `/match/playerstats/${id}`, type: 'GET', token })
export const getTeamStatsByMatch = (id, token = undefined) => apiJson({ endpoint: `/match/teamstats/${id}`, type: 'GET', token })
export const getLiveOddsByMatch = (id, token = undefined) => apiJson({ endpoint: `/match/liveodds/${id}`, type: 'GET', token })
export const getMatchPrediction = (id, token = undefined) => apiJson({ endpoint: `/match/prediction/${id}`, type: 'GET', token })
export const getShotsByMatch = (id, token = undefined) => apiJson({ endpoint: `/match/shots/${id}`, type: 'GET', token })

export const getLeagueTableBySeason = (id, token = undefined) => apiJson({ endpoint: `/season/leaguetable/${id}`, type: 'GET', token })
export const getTopPlayersBySeason = (id, token = undefined) => apiJson({ endpoint: `/season/topplayersfrontend/${id}`, type: 'GET', token })
export const getMatchesBySeason = (id, token = undefined) => apiJson({ endpoint: `/season/matches/${id}`, type: 'GET', token })
export const getSeasonAveragesBySeason = (id, token = undefined) => apiJson({ endpoint: `/season/averages/${id}`, type: 'GET', token })
export const getTopPlayersByTopLeagues = (token = undefined) => apiJson({ endpoint: `/season/topplayers/mainleagues`, type: 'GET', token })

export const getMatchSummary = (id, token = undefined) => apiJson({ endpoint: `/match/match/${id}`, type: 'GET', token })
export const getTeamStatsByTeam = (id, last, token = undefined) => apiJson({ endpoint: `/team/stats/${id}/${last}`, type: 'GET', token })
export const getTeamStatsByTeamWithVenue = (id, last, venue, token = undefined) => apiJson({ endpoint: `/team/stats/${id}/${last}/venue/${venue}`, type: 'GET', token })
export const getTeamRollingAveragesByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/averagesrolling/${id}`, type: 'GET', token })
export const getPlayerAveragesByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/playeraverages/${id}`, type: 'GET', token })
export const getUpcomingMatchesByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/upcomingmatches/${id}`, type: 'GET', token })
export const getTeamLeaguePercentileByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/leaguepercentile/${id}`, type: 'GET', token })
export const getTeamAveragesByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/averages/${id}`, type: 'GET', token })
export const getLatestSeasonTeamStatsByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/latestseasonstats/${id}`, type: 'GET', token })
export const getLatestSeasonShotsByTeam = (id, token = undefined) => apiJson({ endpoint: `/team/shots/${id}`, type: 'GET', token })

export const getUpcomingMatchesByPlayer = (id, token = undefined) => apiJson({ endpoint: `/player/upcomingmatches/${id}`, type: 'GET', token })
export const getMatchStatsByPlayer = (id, last, token = undefined) => apiJson({ endpoint: `/player/stats/${id}/${last}`, type: 'GET', token })
export const getMatchStatsByPlayerWithVenue = (id, last, venue, token = undefined) => apiJson({ endpoint: `/player/stats/${id}/${last}/venue/${venue}`, type: 'GET', token })
export const getPlayerAverages = (id, token = undefined) => apiJson({ endpoint: `/player/averages/${id}`, type: 'GET', token })
export const getPlayerTeamAveragesMain = (id, token = undefined) => apiJson({ endpoint: `/player/teamaveragesmain/${id}`, type: 'GET', token })
export const getPlayerPercentile = (id, token = undefined) => apiJson({ endpoint: `/player/percentile/${id}`, type: 'GET', token })
export const getLatestSeasonMatchStatsByPlayer = (id, token = undefined) => apiJson({ endpoint: `/player/latestseasonstats/${id}`, type: 'GET', token })
export const getPlayerHistory = (id, token = undefined) => apiJson({ endpoint: `/player/history/${id}`, type: 'GET', token })
export const getLatestSeasonShotsByPlayer = (id, token = undefined) => apiJson({ endpoint: `/player/shots/${id}`, type: 'GET', token })

export const getCurrentTournaments = (token = undefined) => apiJson({ endpoint: `/tournaments/current`, type: 'GET', token })
export const getSeasonsByTournament = (id, token = undefined) => apiJson({ endpoint: `/tournament/seasons/${id}`, type: 'GET', token })
export const getLiveTournaments = (token = undefined) => apiJson({ endpoint: `/tournaments/live`, type: 'GET', token })

export const getUpcomingLiveOdds = (token = undefined) => apiJson({ endpoint: `/liveodds/upcoming`, type: 'GET', token })
