import React from 'react'

import { Section } from "@Components/section"
import { PageTitle } from "@Components/text"
import { stripDashes } from "@Functions/transform"

export class PlayerInfoComponent extends React.Component {

  render() {
    return (
      <div data-testid="playerinfo">
        <Section>
          <PageTitle>{stripDashes(this.props.name)}</PageTitle>
        </Section>
      </div>
    )
  }
}
