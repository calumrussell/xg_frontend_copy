import React from 'react'
import { render } from 'react-testing-library'

import { PlayerInfoComponent } from './index.js'

describe('define PlayerInfo', () => {

  it('renders without error', () => {
    const { getByTestId } = render(<PlayerInfoComponent name={"Test"}/>)
    expect(getByTestId('playerinfo')).toBeTruthy()
  })

})
