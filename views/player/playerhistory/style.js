import styled from 'styled-components'

import { TickBox } from "@Components/tickbox"

export const StyledTickBox = styled(TickBox)`
  display: flex;
  justify-content: flex-end;
  padding: 0.5rem 0rem;
`
