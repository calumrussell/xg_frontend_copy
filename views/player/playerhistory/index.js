import React from 'react'
import { connect } from 'react-redux';

import { getPlayerHistory } from '@Services/api'
import { Filter } from "@Components/filter"
import { isMainFilter } from "@Functions/filter"

import { StyledTickBox } from "./style.js"
import { Table } from './components/table'

export class PlayerHistoryComponent extends React.Component {

  state = {
    isMain: false,
    filters: []
  }

  componentDidMount = () => {
    //isMainFilter is a toggle, so running on mount will flip it to ON
    this.isMainFilter()
  }

  isMainFilter = event => this.state.isMain == true
    ? this.setState(state => ({ ...state, isMain: false, filters: [...state.filters.filter(f => f.id != 2)] }))
    : this.setState(state => ({ ...state, isMain: true, filters: [...state.filters, isMainFilter(true)] }))

  render() {
    const { playerHistory } = this.props.data
    const { isMain, filters } = this.state
    //This has hardcoded columns because these shouldn't change
    const dataKeys = ['year', 'league', 'shirtno', 'position']

    return (
      <div data-testid="playerhistory">
        <StyledTickBox
          label={'Main Tournaments Only:'}
          isChecked={isMain}
          handleCheck={this.isMainFilter} />
        <Filter
          names={['data']}
          filters={filters.map(e => e.impl)} >
          <Table
            data={playerHistory}
            columns={dataKeys} />
        </Filter>
     </div>
    )
  }
}
