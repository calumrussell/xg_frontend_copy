import React from 'react'
import { Table as STable, Header, Column, Row as SRow, Body } from 'simpletable'

import { TableContainer } from '@Components/globals'
import { colorScheme } from '@Theme'

import { definition } from './definition.js'

export const Table = ({ data, columns }) => {

  const {
    cols,
    rows
  } = definition(data, columns)

  return (
    <React.Fragment>
      <TableContainer>
        <STable
          background={colorScheme.grey[100]}
          isStriped={colorScheme.grey[200]}
          isFullWidth={true}
          freezeFirstCol={{ headerHeight: '20px', rowHeight: '20px' }}
          cellSpacing="0">
          <Header columns={cols} titles={columns} />
          <Body>
            {[rows]}
          </Body>
        </STable>
      </TableContainer>
    </React.Fragment>
  )
}
