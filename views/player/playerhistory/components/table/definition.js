import React from "react"
import { Table as STable, Header, Column, Row as SRow, Body } from 'simpletable'

import { headerConverter, titleConverter } from "@Functions/transform"
import { TeamLink } from "@Components/link"

const rowCreator = (data, cols) => [
  ...data.map((v, i) => (
    <SRow
      key={i}
      columns={cols}
      dataRow={v} />
  ))
]

const teamCol = (data, columns) => new Column("")
  .setLayout(row => <TeamLink teamid={row.teamid} team={row.team}>{row['team']}</TeamLink>)

const colCreator = (data, columns) => [
  teamCol(data, columns),
  ...columns.map(v => new Column(v)
    .setColHeader(() => headerConverter(v))
    .setColHeaderTitle(() => titleConverter(v)))
]

export const definition = (data, columns) => {
  const cols = colCreator(data, columns)
  return ({
    cols,
    rows: rowCreator(data, cols)
  })
}
