import React from 'react'
import { render } from 'react-testing-library'

import { playerHistory } from '@TestData/playerhistory.json'

import { PlayerHistoryComponent } from './index.js'

describe('define PlayerHistory', () => {

  it('renders without error', () => {
    const data = {"playerHistory": playerHistory}
    const { getByTestId } = render(<PlayerHistoryComponent data={data}/>)
    expect(getByTestId('playerhistory')).toBeTruthy()
  })

})
