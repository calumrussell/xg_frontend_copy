import React from 'react'
import { render } from 'react-testing-library'

import { playerAverages } from '@TestData/playeraverages.json'
import { teamAverages } from '@TestData/teamaverages.json'
import { playerPercentile } from '@TestData/playerpercentile.json'

import { InnerSeasonAvgComponent } from './index.js'

let mockCategories

describe('define PlayerSeasonAverage', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal', 'passkey', 'assist'], 
        "statGroupNames": ['Test']
      }
    })
  })

  it('renders without error', () => {
    const data = {
      "seasonAverages": playerAverages,
      "teamSeasonAverages": teamAverages,
      "averagesPercentile": playerPercentile
    }
    const { getByTestId } = render(<InnerSeasonAvgComponent getComponentDataCategories={mockCategories} data={data}/>)
    expect(getByTestId('playerseasonaverage')).toBeTruthy()
  })

})
