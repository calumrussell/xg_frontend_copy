import React from 'react'
import { Table as STable, Header, Body } from 'simpletable'

import { TableContainer } from '@Components/globals'
import { Glossary } from "@Components/glossary"
import { TableNote } from "@Components/tablenote"
import { colorScheme } from "@Theme"

import { definition } from './definition.js'

export const Table = ({ playerData, teamData, percentileData, columns }) => {

  const {
    rows,
    cols
  } = definition(playerData, teamData, percentileData, columns)

  return (
    <TableContainer>
      <STable
        background={colorScheme.grey[100]}
        isStriped={colorScheme.grey[200]}
        isFullWidth={true}
        freezeFirstCol={{ headerHeight: '20px', rowHeight: '20px' }}
        cellSpacing="0">
        <Header
          columns={cols}
          titles={columns} />
        <Body>
          {[rows]}
        </Body>
      </STable>
      <TableNote>All stats per 90 minutes</TableNote>
      <Glossary columns={columns} />
    </TableContainer>
  )
}
