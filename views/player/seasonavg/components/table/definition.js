import React from "react"
import groupby from "lodash.groupby"
import styled from "styled-components"
import { Column, Row as SRow } from 'simpletable'

import { 
  headerConverter,
  titleConverter,
  toFixedAny
} from '@Functions/transform'
import { PercentileBorder } from "@Components/percentileborder"
import { colorScheme } from '@Theme'

const rowCreator = (groupedPlayer, cols) => [
  ...Object.keys(groupedPlayer).map((v, i) => (
    <SRow
      key={i}
      columns={cols}
      dataRow={v} />
  ))
]

const First = styled.div`
  display: flex;
  align-items: center;
  color: ${colorScheme.grey[800]};
`

const firstCol = playerData => new Column("")
  .setColHeader(() => "")
  .setLayout(row => {
    return (
      <First>
        <div>{row}</div>
        <div style={{ marginLeft: '0.5rem' }}>
          {playerData[row].map((d, i) => <p key={i}>{d['league']}</p>)}
          <p>Team</p>
        </div>
      </First>
    )
  })

const colCreator = (playerData, teamData, percentileData, columns) => [
  firstCol(playerData),
  ...columns.map(v => new Column(v)
    .setLayout(row => {
      const players = playerData[row].map((d, i) => <p key={i}>{toFixedAny(d[v], 2)}</p>)
      return (
        <div >
          {players}
          <p>{teamData[row] ? toFixedAny(teamData[row][0][v], 2) : "-"}</p>
        </div>
      )
    }).setColHeader(() => headerConverter(v))
    .setColHeaderTitle(() => titleConverter(v))
  )
]

export const definition = (playerData, teamData, percentileData, columns) => {
  const groupedPlayer = groupby(playerData, 'year')
  const groupedTeam = groupby(teamData, 'year')
  const groupedPercentile = groupby(percentileData, 'year')

  const cols = colCreator(
    groupedPlayer,
    groupedTeam,
    groupedPercentile,
    columns
  )

  return {
    rows: rowCreator(groupedPlayer, cols),
    cols
  }
}
