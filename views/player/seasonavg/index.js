import React from 'react'
import { connect } from "react-redux"
import orderby from "lodash.orderby"

import { ButtonsWrapper } from "@Components/button"
import { SelectAlt } from "@Components/select"
import { Filter } from "@Components/filter"
import { isMainFilter } from "@Functions/filter"
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'
import { StyledTabs, StyledTickBox, RowWrapper } from "./style.js"

export class InnerSeasonAvgComponent extends React.Component {

  state = {
    statGroup: 'Off',
    isMain: false,
    filters: []
  }

  componentDidMount = () => {
    this.isMainFilter()
  }

  isMainFilter = event => this.state.isMain == true
    ? this.setState(state => ({ ...state, isMain: false, filters: [...state.filters.filter(f => f.id != 2)] }))
    : this.setState(state => ({ ...state, isMain: true, filters: [...state.filters, isMainFilter(true)] }))

  switchStatGroup = event => this.setState({ statGroup: event.target.value })

  switchActive = active => this.setState({ active })

  render() {
    const {
      statGroup,
      filters,
      isMain,
      active
    } = this.state

    const {
      seasonAverages,
      teamSeasonAverages,
      averagesPercentile
    } = this.props.data

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)
    dataKeys.concat([
      'minutesplayed',
      'year',
      'league',
      'ismain',
      'formatname',
      'team'
    ])

    return (
      <div data-testid="playerseasonaverage"> 
        <ButtonsWrapper>
          <SelectAlt
            values={statGroupNames}
            onChange={this.switchStatGroup}
            selected={statGroup}
            title={"Stat Category"} />
          <StyledTickBox
            label={'Main Tourns:'}
            isChecked={isMain}
            handleCheck={this.isMainFilter} />
        </ButtonsWrapper>
        <Filter
          names={['teamData', 'playerData', 'percentileData']}
          filters={filters.map(e => e.impl)} >
          <Table
            columns={dataKeys}
            playerData={seasonAverages}
            teamData={teamSeasonAverages}
            percentileData={averagesPercentile} />
        </Filter>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "SeasonAvgPlayer"))
})
export const SeasonAvgComponent = connect(null, mapDispatchToProps)(InnerSeasonAvgComponent)
