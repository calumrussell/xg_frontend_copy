import React from 'react'
import { connect } from 'react-redux';

import { ButtonsWrapper } from "@Components/button"
import { StatGroupSelect } from "@Components/select"
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'

export class InnerMatchLogComponent extends React.Component {

  state = {
    statGroup: 'Off'
  }

  switchStatGroup = (event) => {
    this.setState({ statGroup: event.target.value })
  }

  render() {
    const { isHome, lastN, statGroup } = this.state
    const { playerStats } = this.props.data

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)

    dataKeys.concat(['minutesplayed', 'position'])

    return (
      <div data-testid="playermatchlog">
        <ButtonsWrapper>
          <StatGroupSelect
            options={statGroupNames}
            changeFunc={this.switchStatGroup}
            selected={statGroup} />
        </ButtonsWrapper>
        <Table
          data={playerStats}
          columns={dataKeys} />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "MatchLogPlayer"))
})
export const MatchLogComponent = connect(null, mapDispatchToProps)(InnerMatchLogComponent)
