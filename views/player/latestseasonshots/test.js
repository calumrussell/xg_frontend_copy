import React from 'react'
import { render } from 'react-testing-library'

import { shots } from '@TestData/shots.json'
import { getLatestSeasonShotsByPlayer } from '@Services/api'

import { LatestSeasonShotsComponent } from './index.js'

jest.mock('@Services/api')

describe('define PlayerShots', () => {

  it('renders without error', () => {
    getLatestSeasonShotsByPlayer.mockReturnValue(Promise.resolve(shots))

    const { getByTestId } = render(<LatestSeasonShotsComponent />)
    expect(getByTestId('playershots')).toBeTruthy()
  })

})
