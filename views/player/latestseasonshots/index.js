import React from "react"

import { HalfPitchWithShots } from "@Components/pitch"
import { Filter, FilterButton } from "@Components/filter"
import { ButtonsWrapper } from "@Components/button"
import { Panel } from "@Components/panel"
import { isGoalFilter } from "@Functions/filter"
import { getLatestSeasonShotsByPlayer } from '@Services/api'
import { tokenParser } from '@Services/token'

export class LatestSeasonShotsComponent extends React.Component {

  state = {
    filters: [isGoalFilter()],
    latestSeasonShots: null
  }

  componentDidMount = async () => {
    const { id } = this.props
    const token = await tokenParser(undefined)
    const [
      latestSeasonShots
    ] = await Promise.all([
      getLatestSeasonShotsByPlayer(id, token)
    ])
    this.setState({ latestSeasonShots })
  }

  addFilter = filter => this.setState(state => ({ ...state, filters: [...state.filters, filter] }))

  removeFilter = id => {
    this.setState(state => ({
      ...state,
      filters: [
        ...state.filters.filter(f => f.id != id)
      ]
    }))
  }

  render() {
    const {
      filters,
      latestSeasonShots
    } = this.state

    return (
      <div data-testid="playershots">
        <Panel
          title={'Latest Season Shots'}
          requiredData={latestSeasonShots}>
          <ButtonsWrapper selector={"stat-panel-latestseasonshots-text"}>
            <FilterButton
              selector={"stat-panel-latestseasonshots-text"}
              statData={latestSeasonShots}
              existingFilters={filters}
              addFilter={this.addFilter}
              removeFilter={this.removeFilter} />
          </ButtonsWrapper>
          <Filter
            names={['data']}
            filters={filters.map(e => e.impl)} >
            <HalfPitchWithShots
              filters={filters}
              data={latestSeasonShots} />
          </Filter>
        </Panel>
      </div>
    )
  }
}
