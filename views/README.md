Containers wrap around tables. They control the data that is presented to tables.

In some cases, there is no additional logic but this will mostly pertain to filtering, sorting, and
gathering input from the user to perform either of these tasks on the data. None of these
operations should modify the original data but Containers can call for additional data, they just
cannot modify or remove the data they already have.


Data =>
Layout => -- This should
Component => -- 
Table/Chart
