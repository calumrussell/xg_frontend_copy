import React from 'react'
import jest from 'jest'
import { render, fireEvent } from 'react-testing-library'
import orderBy from "lodash.orderby"

import { matches } from '@TestData/matches.json'

import { RecentMatches } from './index.js'

describe('define RecentMatches', () => {

  it('renders without error with correct data', () => {
    const sortedMatches = orderBy(matches, 'starttime', 'asc')
    const data = {"sortedMatches": sortedMatches}
    const Component = props => <RecentMatches data={data} />
    const { getByTestId } = render(<Component />)
    expect(getByTestId('matches')).toBeTruthy()
  })

  it('renders without error with empty input', () => {
    const emptyData = {"sortedMatches": []}
    const Component = props => <RecentMatches data={emptyData} />
    const { getByTestId } = render(<Component />)
    expect(getByTestId('matches')).toBeTruthy()
  })

  it('renders without error with undefined input', () => {
    const undefinedData = {"sortedMatches": undefined}
    const Component = props => <RecentMatches data={undefinedData} />
    const { getByTestId } = render(<Component />)
    expect(getByTestId('matches')).toBeTruthy()
  })

  it('scrolls through matches on button click', () => {
    const sortedMatches = orderBy(matches, 'starttime', 'asc')
    const data = {"sortedMatches": sortedMatches}
    const Component = props => <RecentMatches data={data} />
    const { getByTestId, container } = render(<Component />)
    expect(getByTestId('matches-leftarrow')).toBeTruthy()
    expect(getByTestId('matches-rightarrow')).toBeTruthy()

    const firstLinks = container.querySelectorAll('a')
    const firstHref = Object.keys(firstLinks).map(v => firstLinks[v].href)

    const leftClick = { button: 0 }
    fireEvent.click(getByTestId('matches-leftarrow'), leftClick)

    const secondLinks = container.querySelectorAll('a')
    const secondHref = Object.keys(secondLinks).map(v => secondLinks[v].href)

    fireEvent.click(getByTestId('matches-rightarrow'), leftClick)

    const thirdLinks = container.querySelectorAll('a')
    const thirdHref = Object.keys(thirdLinks).map(v => thirdLinks[v].href)

    expect(firstHref).not.toEqual(secondHref)
    expect(thirdHref).toEqual(firstHref)
  })

  it('will reload matches when seasonid is updated', () => {
    const sortedMatches = orderBy(matches, 'starttime', 'asc')
    const data = {"sortedMatches": sortedMatches}
    const { getByTestId, container, rerender } = render(<RecentMatches data={data} seasonid={900}/>)

    const leftClick = { button: 0 }
    fireEvent.click(getByTestId('matches-leftarrow'), leftClick)

    const firstLinks = container.querySelectorAll('a')
    const firstHref = Object.keys(firstLinks).map(v => firstLinks[v].href)

    rerender(<RecentMatches data={data} seasonid={100}/>)

    const secondLinks = container.querySelectorAll('a')
    const secondHref = Object.keys(secondLinks).map(v => secondLinks[v].href)

    expect(firstHref).not.toEqual(secondHref)
  })

})

