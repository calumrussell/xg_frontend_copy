import React from 'react'

import { findCurrentDate } from "@Functions/search"
import { LeftArrowIcon, RightArrowIcon } from '@Components/icon'
import { RecentMatchesByDate as Matches } from '@Components/table'

import { SideButtons } from "./style.js"

export class RecentMatches extends React.Component {

  state = {
    position: 0,
  }

  matchBack = () => {
    const goBack = this.state.position - 10
    if (goBack >= 0) {
      this.setState({ position: goBack })
    } else {
      this.setState({ position: 0 })
    }
  }

  matchForward = () => {
    const goForward = this.state.position + 10
    if (goForward - 10 <= this.props.data.sortedMatches.length && goForward != this.props.data.sortedMatches.length) {
      this.setState({ position: goForward })
    }
  }

  componentDidMount = () => {
    const { sortedMatches } = this.props.data
    const position = findCurrentDate(sortedMatches)
    this.setState({ position })
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    if (this.props.seasonid != nextProps.seasonid) return true
    else if (this.state.position != nextState.position) return true
    return false
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.seasonid != this.props.seasonid) {
      const { sortedMatches } = this.props.data
      const position = findCurrentDate(sortedMatches)
      this.setState({ position })
    }
  }

  getMatchesByPosition = matches => {
    const { position } = this.state
    if (matches === undefined || matches.length == 0 ) {
        return [] 
    }
    return matches.slice(position, position + 10)
  }

  render() {
    const { sortedMatches } = this.props.data
    const filteredMatches = this.getMatchesByPosition(sortedMatches)

    return (
      <div data-testid="matches">
        <SideButtons>
          <LeftArrowIcon
            data-testid="matches-leftarrow"
            onClick={this.matchBack} />
          <RightArrowIcon
            data-testid="matches-rightarrow"
            onClick={this.matchForward} />
        </SideButtons>
        <Matches
          matches={filteredMatches} />
      </div>
    )
  }
}
