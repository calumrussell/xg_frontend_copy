import styled from "styled-components"

import { colorScheme } from '@Theme'

export const SideButtons = styled.div`
  width: 100%;
  margin: 0.5rem 0;
  display: flex;
  justify-content: flex-end;
  cursor:pointer;
  div{
    padding: 0.25rem;
    margin-right: 0.5rem;
  }
  svg{
    fill: ${colorScheme.grey[700]};
  }
`

