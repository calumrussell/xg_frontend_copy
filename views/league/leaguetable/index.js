import React from 'react'
import { connect } from "react-redux"

import { pointSort } from "@Functions/sort" 
import { addRank } from "@Functions/transform"
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from "./components/table"

export class InnerLeagueTable extends React.Component {

  formatLeagueTable = leagueTable => addRank(pointSort(leagueTable))

  render() {
    const { leagueTable } = this.props.data

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories("LeagueTable")

    return (
      <div data-testid="leaguetable">
        <Table
          data={this.formatLeagueTable(leagueTable)}
          keys={dataKeys} />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "LeagueTable"))
})
export const LeagueTable = connect(null, mapDispatchToProps)(InnerLeagueTable)
