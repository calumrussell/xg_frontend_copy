import React from 'react';
import styled from 'styled-components'
import { Table as STable, Header, Row as SRow, Body } from 'simpletable'

import { titleConverter } from '@Functions/transform'
import { TableContainer } from '@Components/globals'
import { colorScheme } from '@Theme'

import { definition } from './definition.js'

/*
const TableContainer = styled(BaseContainer)`
  table td, th{
    padding: 0.25rem 0;
    white-space:nowrap;
  }
`
*/

const rowBuilder = (data, cols) => data.map((v, i) => (
  <SRow
    key={i}
    dataRow={v}
    columns={cols} />
))

export const Table = ({ data, keys }) => {
  const columnsWithIntegers = ['matchesplayed', 'goalsfor', 'goalsagainst', 'goalsdifference', 'points']
  const titles = ["", ...keys.map(v => titleConverter(v))]
  const cols = definition(keys, columnsWithIntegers)
  const rows = rowBuilder(data, cols)

  return (
    <div style={{ position: 'relative', marginLeft: '-5px' }}>
      <TableContainer>
        <STable
          background={colorScheme.grey[100]}
          isStriped={colorScheme.grey[200]}
          freezeFirstCol={{ headerHeight: '20px', rowHeight: '20px' }}
          isFullWidth={true}
          cellSpacing="0">
          <Header columns={cols} titles={titles} />
          <Body>{[rows]}</Body>
        </STable>
      </TableContainer>
    </div>
  )
}
