import styled from "styled-components"
import { Column } from 'simpletable'

import { headerConverter, titleConverter, toFixedAny } from '@Functions/transform'
import { TeamLink } from '@Components/link'
import { VSmTLogo } from '@Components/icon'
import { colorScheme } from "@Theme"

const FirstColWrap = styled.div`
  display: flex;
  align-items: center;
`

const firstCol = new Column('')
  .setColHeader(() => "")
  .setLayout(row => {
    return (
      <FirstColWrap>
        <p style={{ color: colorScheme.grey[800], width: '1.5rem' }}>{row.rank}</p>
        <span
          style={{ marginLeft: '5px', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
          <React.Fragment>
            <TeamLink
              {...row}>
              <VSmTLogo
                {...row} />
            </TeamLink>
          </React.Fragment>
          <span
            style={{ marginLeft: '5px' }}>
            <React.Fragment>
              <TeamLink {...row}>{row['brief'] ? row['brief'] : row['team']}</TeamLink>
            </React.Fragment>
          </span>
        </span>
      </FirstColWrap >
    )
  })

const otherCols = (keys, hasInteger) => [...keys.map(v => new Column(v)
  .setLayout(row => hasInteger.includes(v) ? row[v] : toFixedAny(row[v], 2))
  .setColHeaderTitle(() => `${titleConverter(v)}:  `)
  .setColHeader(() => headerConverter(v)))]

export const definition = (cols, hasInteger) => [firstCol, ...otherCols(cols, hasInteger)]
