import React from 'react'
import { createStore } from 'redux'
import { render } from 'react-testing-library'

import { leagueTable } from '@TestData/leaguetable.json'

import { LeagueTable, InnerLeagueTable } from './index.js'

import * as sortFuncs from '@Functions/sort'
import * as transformFuncs from '@Functions/transform'

let mockCategories;

describe('define LeagueTable ', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['matchesplayed', 'goalsfor', 'goalsagainst', 'goalsdifference', 'xg', 'oppxg', 'points'], 
        "statGroupNames": {}
      }
    })
  })

  it('renders without error with correct data', () => {
    const data = {"leagueTable": leagueTable}
    const { getByTestId } = render(<InnerLeagueTable data={data} getComponentDataCategories={mockCategories} />)
    expect(getByTestId('leaguetable')).toBeTruthy()
  })

  it('sorts by points and adds rank', () => {
    let pointSort = jest.spyOn(sortFuncs, 'pointSort')
    let addRank = jest.spyOn(transformFuncs, 'addRank')
    const data = {"leagueTable": leagueTable}
    const { getByTestId } = render(<InnerLeagueTable data={data} getComponentDataCategories={mockCategories} />)
    expect(pointSort).toHaveBeenCalledTimes(1)
    expect(addRank).toHaveBeenCalledTimes(1)
  })

})
