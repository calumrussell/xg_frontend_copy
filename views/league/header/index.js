import React from 'react';

import { LgWidth, LeagueWrapper } from "./style.js"

export const Header = ({ name }) => {
  return (
    <LeagueWrapper data-testid='leagueheader'>
      <LgWidth src={`https://xg.football/static/img/leagues/${name}.png`} />
    </LeagueWrapper>
  )
}
