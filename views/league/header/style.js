import styled from 'styled-components'

export const LgWidth = styled.img`
  width: 150px;
`

export const LeagueWrapper = styled.div`
  text-align: center;
  padding: 10px 0;
`
