import React from 'react'
import { render } from 'react-testing-library'

import { Header } from './index.js'

describe('define LeagueHeader', () => {

  it('renders without error', () => {
    const { getByTestId } = render(<Header />)
    expect(getByTestId('leagueheader')).toBeTruthy()
  })

})
