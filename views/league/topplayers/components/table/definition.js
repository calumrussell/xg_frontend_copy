import React from "react"
import { Column } from 'simpletable'

import { PlayerLink } from '@Components/link'

const nameCol = title => new Column('formatname')
  .setColHeader(() => title)
  .setLayout(row => <PlayerLink {...row}>{row['formatname']}</PlayerLink>)
const valCol = stat => new Column(stat)
  .setColHeader(() => "")

export const definition = (title, stat) => [nameCol(title), valCol(stat)]
