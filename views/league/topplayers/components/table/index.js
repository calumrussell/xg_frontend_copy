import React from 'react'
import orderby from 'lodash.orderby'
import { Table as STable, Header, Row as SRow, Body } from 'simpletable'

import { colorScheme } from '@Theme'

import { TableContainer } from './style.js'
import { definition } from './definition.js'

export const Table = ({ data, stat, title }) => {

  const ordered = orderby(data, stat, 'desc').slice(0, 10)
  const cols = definition(title, stat)
  return (
    <TableContainer>
      <STable
        background={colorScheme.grey[100]}
        isStriped={colorScheme.grey[100]}
        isFullWidth={true}
        cellSpacing="0">
        <Header columns={cols} />
        <Body>
          {
            ordered.map((v, i) => <SRow key={i} columns={cols} dataRow={v} />)
          }
        </Body>
      </STable>
    </TableContainer>
  )
}

