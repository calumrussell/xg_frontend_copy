import styled from 'styled-components'

import { BaseContainer } from "@Components/globals"

export const TableContainer = styled(BaseContainer)`
  table {
    padding-right: 0.5rem;
    min-width: 250px;
  }

  table tr th{
    text-align: left;
  }

  table td:nth-child(1){
    padding-left: 0px;
  }

  table td{
    padding: 0.25rem;
  }

  table tbody tr:nth-child(1) td{
    font-weight:bold;
  }
`
