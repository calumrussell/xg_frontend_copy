import React from 'react'

import { Table } from './components/table'
import { TablesWrapper } from "./style.js"

export class TopPlayers extends React.Component {

  render() {
    const { topPlayers } = this.props.data
    return (
      <TablesWrapper data-testid='topplayers' data={topPlayers}>
        <Table data={topPlayers} stat={'shotontarget'} title={'On Target Shots'} />
        <Table data={topPlayers} stat={'assist'} title={'Assists'} />
        <Table data={topPlayers} stat={'goalnormal'} title={'Goals'} />
        <Table data={topPlayers} stat={'passkey'} title={'Key Passes'} />
      </TablesWrapper>
    )
  }
}
