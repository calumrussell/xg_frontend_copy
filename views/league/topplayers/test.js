import React from 'react'
import { render } from 'react-testing-library'

import { topPlayers } from '@TestData/topplayers.json'

import { TopPlayers } from './index.js'

describe('define TopPlayers', () => {

  it('renders without error', () => {
    const data = { "topPlayers": topPlayers }

    const { getByTestId } = render(<TopPlayers data={data} />)
    expect(getByTestId('topplayers')).toBeTruthy()
  })

})
