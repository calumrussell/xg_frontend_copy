import styled from "styled-components"

import { mq } from "@Theme"

export const TablesWrapper = styled.div`
  display: flex;
  flex-direction: row;
  overflow-x: hidden;
  justify-content: space-between;
  &&:hover{
    overflow-x: auto;
  }
  ${mq[1]}{
    &&{
      overflow-x: auto;
    }
  }
`
