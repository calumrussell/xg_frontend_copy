import React from "react"

import {
  StyledLeftArrowIcon,
  StyledRightArrowIcon,
  Wrap,
  StyledTitle
} from "./style.js"

export const SeasonSelector = ({ current, moveForward, moveBackward }) => {
  return (
    <Wrap data-testid='seasonselector'>
      <StyledLeftArrowIcon
        onClick={moveBackward} />
      <StyledTitle>{current}</StyledTitle>
      <StyledRightArrowIcon
        onClick={moveForward} />
    </Wrap>
  )
}
