import React from 'react'
import { render } from 'react-testing-library'

import { SeasonSelector } from './index.js'

describe('define SeasonSelector', () => {

  it('renders without error', () => {
    const { getByTestId } = render(<SeasonSelector />)
    expect(getByTestId('seasonselector')).toBeTruthy()
  })

})
