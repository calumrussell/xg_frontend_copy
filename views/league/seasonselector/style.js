import styled from "styled-components"

import { colorScheme } from '@Theme'
import { LeftArrowIcon, RightArrowIcon } from "@Components/icon"
import { SectionTitle } from "@Components/text"

export const StyledLeftArrowIcon = styled(LeftArrowIcon)`
  svg{
    fill: ${colorScheme.grey[700]};
  }
`

export const StyledRightArrowIcon = styled(RightArrowIcon)`
  svg{
    fill: ${colorScheme.grey[700]};
  }
`

export const Wrap = styled.div`
  display:flex;
  justify-content: space-between;
  width: 9rem;
  padding: 0.5rem;
  align-items:center;
  border-radius: 0.5rem;
`

export const StyledTitle = styled(SectionTitle)`
  color: ${colorScheme.grey[600]};
  margin: 0;
`
