import React from 'react'
import { render } from 'react-testing-library'

import { liveOdds } from '@TestData/liveodds.json'

import { UpcomingLiveOdds } from './index.js'

describe('define HomeUpcomingLiveOdds', () => {

  it('renders without error', () => {
    const data = {"liveOdds": liveOdds}
    const { getByTestId } = render(<UpcomingLiveOdds data={data} />)
    expect(getByTestId('homeupcomingliveodds')).toBeTruthy()
  })

})
