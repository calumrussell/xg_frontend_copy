import sortby from "lodash.sortby"

export const LiveOddsSorter = ({ data }) => {
  const sortedodds = sortby(data, 'mb_starttime')
  const openingodds = sortedodds[0]
  const closingodds = sortedodds[sortedodds.length - 1]
  return <LiveOddsRow {...closingodds} />
}
