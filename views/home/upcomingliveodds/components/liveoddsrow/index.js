import React from "react"
import moment from 'moment'

import { OffsetText, DefaultText } from "@Components/text"
import { TeamLink } from "@Components/link"

import {
  LiveOddsRowWrapper,
  LiveOddsRowWrapperTop,
  TeamWrapper,
  DateWrapper
} from './style.js'

export const LiveOddsRow = ({
  home,
  homeid,
  away,
  awayid,
  time,
  hwin,
  awin,
  dwin,
  mb_starttime
}) => {
  return (
    <LiveOddsRowWrapper>
      <LiveOddsRowWrapperTop>
        <TeamWrapper>
          <TeamLink
            team={home}
            teamid={homeid}>
            {home}
          </TeamLink>
          <span>{hwin}</span>
        </TeamWrapper>
        <TeamWrapper>
          <span><DefaultText>Draw</DefaultText></span>
          <span>{dwin}</span>
        </TeamWrapper>
        <TeamWrapper>
          <TeamLink
            team={away}
            teamid={awayid}>
            {away}
          </TeamLink>
          <span>{awin}</span>
        </TeamWrapper>
      </LiveOddsRowWrapperTop>
      <DateWrapper>
        <OffsetText>Starting: {moment(mb_starttime * 1000).format('MMMM Do YYYY, h:mm')}</OffsetText>
        <OffsetText>Last Updated: {moment(time * 1000).format('h:mm')}</OffsetText>
      </DateWrapper>
    </LiveOddsRowWrapper>
  )
}
