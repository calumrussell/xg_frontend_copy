import styled from "styled-components"

export const DateWrapper = styled.div`
  display: flex;
  justify-content: row;
  justify-content: space-between;
`

export const LiveOddsRowWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const LiveOddsRowWrapperTop = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const TeamWrapper = styled.div`
  display: flex;
  flex-direction: column;
`
