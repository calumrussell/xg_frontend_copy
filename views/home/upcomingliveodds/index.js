import React from "react"
import styled from "styled-components"
import groupby from "lodash.groupby"

import { LiveOddsSorter } from './components/liveoddssorter'

export class UpcomingLiveOdds extends React.Component {

  render() {
    const {
      liveOdds
    } = this.props.data

    const oddsbymatch = groupby(liveOdds, 'matchid')

    return (
      <div data-testid="homeupcomingliveodds">
        <React.Fragment>
          {Object.keys(oddsbymatch).map((odd, i) => <LiveOddsSorter key={i} data={oddsbymatch[odd]} />)}
        </React.Fragment>
      </div>
    )
  }
}
