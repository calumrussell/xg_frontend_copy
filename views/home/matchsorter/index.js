import React from "react"
import groupBy from 'lodash.groupby'
import sortBy from 'lodash.sortby'

import { RecentMatchesByLeague as RecentMatches } from '@Components/table'

import { DateSelector } from './components/dateselector'
import { LeagueSelector } from './components/leagueselector'

export class MatchSorter extends React.Component {

  state = {
    leagueFilter: () => true,
    dateFilter: () => true
  }

  setLeagueFilter = filter =>
    this.setState({ leagueFilter: filter })

  setDateFilter = filter =>
    this.setState({ dateFilter: filter })

  runFilters = matches => {
    const temp = matches
      .filter(this.state.leagueFilter)
      .filter(this.state.dateFilter)

    if (temp == undefined || temp.length == 0) return []
    return groupBy(sortBy(temp, ['starttime']), v => v.league)
  }

  filterNonActiveLeagues = (matches, leagues) => {
    const matchLeagues = new Set(matches.map(v => v.tournamentid))
    return leagues.filter(v => matchLeagues.has(v.tournamentid))
  }

  render() {
    const { matches, leagues } = this.props.data
    const filteredLeagues = this.filterNonActiveLeagues(matches, leagues)
    const filteredMatches = this.runFilters(matches)
    return (
      <div data-testid="homematchsorter">
        <LeagueSelector
          {...this.props}
          {...this.state}
          leagues={filteredLeagues}
          setFilter={this.setLeagueFilter} />
        <DateSelector
          {...this.props}
          {...this.state}
          setFilter={this.setDateFilter} />
        <RecentMatches matches={filteredMatches} />
      </div>
    )
  }
}
