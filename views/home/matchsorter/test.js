import React from 'react'
import { render } from 'react-testing-library'

import { matches } from '@TestData/matches.json'
import { tournaments } from '@TestData/tournaments.json'

import { MatchSorter } from './index.js'

describe('define HomeMatchSorter', () => {

  it('renders without error', () => {
    const data = {"matches": matches, "leagues": tournaments}
    const { getByTestId } = render(<MatchSorter data={data} />)
    expect(getByTestId('homematchsorter')).toBeTruthy()
  })

})
