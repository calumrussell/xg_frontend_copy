import styled from "styled-components"

import { ButtonRounded } from '@Components/button'
import { colorScheme } from '@Theme'

export const LeagueSelectorWrap = styled.div`
  display: flex;
  cursor: pointer;
  flex-direction:row;
  overflow-x: hidden;
  &&:hover{
    overflow-x: auto;
  }
`

export const StyledButtonLeague = styled(ButtonRounded)`
  display: flex;
  align-items: center;
  margin: 0.5rem 0.25rem;
  cursor: pointer;
  background: ${({ isActive }) => isActive ? colorScheme.grey[400] : colorScheme.grey[200]};
  img{
    cursor:pointer;
    margin-right: 0.5rem;
    width:1rem;
    height:1rem;
    border-radius:50%;
  }
  header{
    cursor:pointer;
    color: ${colorScheme.grey[600]}
  }
`
