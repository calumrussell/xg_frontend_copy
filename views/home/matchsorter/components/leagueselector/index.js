import React from "react"

import { SubTitle } from '@Components/text'

import { LeagueSelectorWrap, StyledButtonLeague } from './style.js'

export class LeagueSelector extends React.Component {

  state = {
    leagues: [
      'EPL',
      'BUN',
      'LIGA',
      'LIG1',
      'ISA'
    ]
  }

  componentDidMount() {
    this.props.setFilter(l => this.state.leagues.includes(l.league))
  }

  toggleLeague = league => {
    const { leagues } = this.state

    const newLeagues = leagues.includes(league)
      ? leagues.filter(l => l != league)
      : [...leagues, league]

    if (newLeagues.length == 0) {
      this.props.setFilter(() => true)
    } else {
      this.props.setFilter(l => newLeagues.includes(l.league))
    }
    this.setState({ leagues: newLeagues })
  }

  renderLeagueButton = (key, { league, countrycode }) => {
    return (
      <StyledButtonLeague
        key={key}
        onClick={() => this.toggleLeague(league)}
        isActive={this.state.leagues.includes(league)}>
        <img src={`https://xg.football/static/img/flags/${countrycode}_sq.svg`} />
        <SubTitle>{league}</SubTitle>
      </ StyledButtonLeague>
    )
  }

  render() {
    const { leagues } = this.props
    return (
      <LeagueSelectorWrap>
        {
          leagues.map((v, i) => this.renderLeagueButton(i, { ...v }))
        }
      </LeagueSelectorWrap >
    )
  }
}
