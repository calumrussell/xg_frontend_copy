import React from "react"

import { SubTitle } from '@Components/text'

import { DateSelectorWrap, StyledButton } from './style.js'

const getFilters = dates => {

  const dateFilters = {
    "Live": v => v.period == "FirstHalf" || v.period == "SecondHalf" || v.period == "Live",
    "Upcoming": v => v.period == "PreMatch",
    "Ended": v => v.period == "FullTime"
  }

  if (dates.length == 0) return () => true
  return v => {
    const bool_test = dates.map(date => dateFilters[date](v))
    return bool_test.includes(true)
  }
}

export class DateSelector extends React.Component {

  state = {
    dates: [],
  }

  setFilter = date => {
    const { dates } = this.state

    const newDates = dates.includes(date)
      ? dates.filter(d => d != date)
      : [...dates, date]
    
    this.props.setFilter(getFilters(newDates))
    this.setState({ dates: newDates })
  }

  render() {
    const {
      dates
    } = this.state

    return (
      <DateSelectorWrap>
        {
          ['Ended', 'Upcoming', 'Live'].map((t, i) => (
            <StyledButton
              key={i}
              onClick={() => this.setFilter(t)}
              isActive={dates.includes(t)}>
              <SubTitle>
                {t}
              </SubTitle>
            </StyledButton>
          ))
        }
      </DateSelectorWrap>
    )
  }
}

export default DateSelector
