import styled from "styled-components"

import { colorScheme } from '@Theme'
import { ButtonRounded } from '@Components/button'

export const DateSelectorWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`

export const StyledButton = styled(ButtonRounded)`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin: 0.5rem 0.25rem;
  background: ${({ isActive }) => isActive ? colorScheme.grey[400] : colorScheme.grey[200]};
  img{
    margin-right: 0.5rem;
    width:1rem;
    height:1rem;
    border-radius:50%;
  }
  header{
    color: ${colorScheme.grey[600]}
  }
`
