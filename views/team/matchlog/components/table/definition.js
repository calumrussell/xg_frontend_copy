import React from "react"
import styled from "styled-components"
import orderby from "lodash.orderby"
import { Column, Row as SRow } from 'simpletable'

import { SmTLogo } from '@Components/icon'
import { MatchLinkFromTeam, TeamLink } from '@Components/link'
import { mq, colorScheme } from "@Theme"
import { 
  toFixedAny,
  headerConverter,
  titleConverter,
  numberConverter,
  dateConverter
} from '@Functions/transform'
import { createAverage } from '@Functions/calculate'

const FirstColWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;`

const Second = styled.span`
  padding-right: 0.5rem;
  ${mq[0]}{
    display:none;
  }`

const First = styled.span`
  padding-right: 0.5rem;`

const NoWrap = styled.p`
  white-space: nowrap;`

const firstCol = new Column('')
  .setLayout(row => {
    return (
      <React.Fragment>
        <FirstColWrapper>
          <First>
            <p>
              <MatchLinkFromTeam {...row}>
                {dateConverter(row.starttime)}
              </MatchLinkFromTeam>
            </p>
            <NoWrap>
              <TeamLink teamid={row.oppid} team={row.opp}>
                {row.oppbrief ? row.oppbrief : row.opp}
              </TeamLink>
            </NoWrap>
          </First>
        </FirstColWrapper>
      </React.Fragment>
    )
  })

const secondCol = new Column('')
  .setLayout(row => {
    return (
      !row.opplogo ||
      <Second>
        <TeamLink teamid={row.oppid} team={row.opp}>
          <SmTLogo logo={row['opplogo']} teamid={row['oppid']} />
        </TeamLink>
      </Second>
    )
  })

const thirdCol = new Column('')
  .setLayout(row => {
    return (<NoWrap>{`${row['goalsfor']} - ${row['goalsagainst']} ${row['ishome'] == 1 ? '(H)' : '(A)'}`}</NoWrap>)
  })

const tournCol = new Column('')
  .setLayout(row => <span>{row['league']}</span>)

const colCreator = columns => {
  return [
    firstCol,
    tournCol,
    secondCol,
    thirdCol,
    ...columns.map(v => new Column(v)
      .setLayout(row => numberConverter(row[v], v, false))
      .setColHeader(() => headerConverter(v))
      .setColHeaderTitle(() => `${titleConverter(v)}:  `))
  ]
}

const rowCreator = (data, cols) => data.map((v, i) => {
  return (
    <SRow
      key={i}
      columns={cols}
      dataRow={v} />
  )
})

const avgColCreator = columns => [
  new Column("").setLayout(row => <p style={{ color: colorScheme.grey[800] }}>Average</p>),
  new Column(""),
  new Column(""),
  new Column(""),
  ...columns.map(v => new Column(v)
    .setLayout(row => toFixedAny(row[v], 2))
    .setColHeaderTitle(() => `${titleConverter(v)}:  `))]

const averagesRowCreator = (data, columns) => (
  <SRow
    key={0}
    dataRow={createAverage(data)}
    columns={avgColCreator(columns)} />
)

export const definition = (data, columns) => {

  const sortedData = orderby(data, ['starttime'], ['desc'])
  const cols = colCreator(columns)
  const averagesRows = averagesRowCreator(sortedData, columns)
  return {
    rows: rowCreator(sortedData, cols),
    cols,
    averagesRows
  }
}
