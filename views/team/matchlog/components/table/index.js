import React from 'react'
import { Paginator, Table as STable, Header, Row as SRow, Body } from 'simpletable'

import { BaseContainer } from '@Components/globals'
import { Glossary } from "@Components/glossary"
import { PaginatorButtons } from "@Components/paginatorbuttons"
import { colorScheme } from '@Theme'

import { definition } from './definition.js'

export class Table extends React.Component {

  state = {
    start: 0,
    end: 5,
    gap: 5
  }

  /*
  Only calculate on initial render
  */

  onSelectChange = event => {
    const newGap = event.target.value
    this.setState({
      end: this.state.start + newGap,
      gap: newGap
    })
  }

  moveForward = () => {
    const {
      end,
      gap
    } = this.state
    const dataLength = this.props.data.length
    if (end + 1 < dataLength) {
      this.setState(state => ({ start: state.start + gap, end: state.end + gap }))
    } else {
      this.setState({ start: dataLength - gap, end: dataLength })
    }
  }

  moveBackward = () => {
    const {
      start,
      gap
    } = this.state
    if (start - 1 > 0) {
      this.setState(state => ({ start: state.start - gap, end: state.end - gap }))
    } else {
      this.setState({ start: 0, end: gap })
    }
  }

  render() {
    const { gap } = this.state
    const { columns, data } = this.props

    const {
      rows,
      cols,
      averagesRows
    } = definition(data, columns)

    return (
      <div
        style={{ position: 'relative' }}>
        <BaseContainer>
          <STable
            background={colorScheme.grey[100]}
            isStriped={colorScheme.grey[200]}
            isFullWidth={true}
            freezeFirstCol={{ headerHeight: '20px', rowHeight: '30px' }}
            cellSpacing="0">
            <Header
              columns={cols}
              titles={columns} />
            <Body>
              <Paginator
                start={this.state.start}
                end={this.state.end} >
                {[rows]}
              </Paginator>
              {[averagesRows]}
            </Body>
          </STable>
        </BaseContainer>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}>
          <Glossary
            columns={columns} />
          <PaginatorButtons
            moveBackward={this.moveBackward}
            moveForward={this.moveForward}
            gap={gap}
            onSelectChange={this.onSelectChange} />
        </div>
      </div >
    )
  }
}
