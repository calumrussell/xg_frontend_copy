import React from 'react'
import { render } from 'react-testing-library'

import { teamStats } from '@TestData/teamstats.json'

import { InnerMatchLogComponent } from './index.js'

let mockCategories;

describe('define TeamMatchLog', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal', 'passkey', 'assist'], 
        "statGroupNames": ['Test']
      }
    })
  })

  it('renders without error', () => {
    const data = {"teamStats": teamStats}
    const { getByTestId } = render(<InnerMatchLogComponent getComponentDataCategories={mockCategories} data={data} />)
    expect(getByTestId('teammatchlog')).toBeTruthy()
  })

})
