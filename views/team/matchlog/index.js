import React from 'react'
import { connect } from 'react-redux';

import { ButtonsWrapper } from "@Components/button"
import { Filter, FilterButton } from "@Components/filter"
import { StatGroupSelect } from "@Components/select"
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'

export class InnerMatchLogComponent extends React.Component {

  state = {
    matchStats: [],
    statGroup: 'OffTeam',
    filters: []
  }

  addFilter = filter => this.setState(state => ({ ...state, filters: [...state.filters, filter] }))

  removeFilter = id => {
    this.setState(state => ({
      ...state,
      filters: [
        ...state.filters.filter(f => f.id != id)
      ]
    }))
  }

  switchStatGroup = event => this.setState({ statGroup: event.target.value })

  render() {
    const { statGroup, filters } = this.state
    const { teamStats } = this.props.data

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)

    return (
      <div data-testid="teammatchlog">
        <ButtonsWrapper selector={"stat-panel-matchlog-text"}>
          <StatGroupSelect
            options={statGroupNames}
            changeFunc={this.switchStatGroup}
            selected={statGroup} />
          <FilterButton
            selector={"stat-panel-matchlog-text"}
            statData={teamStats}
            existingFilters={filters}
            addFilter={this.addFilter}
            removeFilter={this.removeFilter} />
        </ButtonsWrapper>
        <Filter
          filters={filters.map(e => e.impl)} >
          <Table
            data={teamStats}
            columns={dataKeys} />
        </Filter>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "MatchLogTeam"))
})
export const MatchLogComponent = connect(null, mapDispatchToProps)(InnerMatchLogComponent)
