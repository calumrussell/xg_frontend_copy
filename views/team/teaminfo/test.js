import React from 'react'
import { render } from 'react-testing-library'

import { teamStats } from '@TestData/teamstats.json'

import { TeamInfoComponent } from './index.js'

let mockCategories;

describe('define TeamTeamInfo', () => {

  it('renders without error', () => {
    const { getByTestId } = render(<TeamInfoComponent id={"test"} name={"test"} logo={"test"} />)
    expect(getByTestId('teamteaminfo')).toBeTruthy()
  })

})
