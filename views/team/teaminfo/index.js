import React from 'react'

import { Section } from "@Components/section"
import { LgLogo } from "@Components/icon"
import { stripDashes } from '@Functions/transform'

import { PageTitleMod } from "./style.js"

export const TeamInfoComponent = ({ id, logo, name }) => {

  return (
    <div data-testid="teamteaminfo">
      <Section>
        <PageTitleMod name={name}>{stripDashes(name)}</PageTitleMod>
        {!logo || <LgLogo teamId={id} src={`https://xg.football/static/img/logos/${id}.png`} />}
      </Section>
    </div>
  )
}
