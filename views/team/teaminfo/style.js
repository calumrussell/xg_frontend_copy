import styled from 'styled-components'

import { PageTitle } from "@Components/text"

const sizeProp = (text, original) => text.length > 15 ? original * 0.80 : original

export const PageTitleMod = styled(PageTitle)`
  font-size: ${props => sizeProp(props.name, 25)}
`
