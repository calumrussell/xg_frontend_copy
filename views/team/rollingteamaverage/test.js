import React from 'react'
import { render } from 'react-testing-library'

import { rollingAverages } from '@TestData/rollingaverages.json'
import { getTeamRollingAveragesByTeam } from '@Services/api'

import { RollingTeamAveragesComponent } from './index.js'

jest.mock('@Services/api')

describe('define TeamRollingAverages', () => {

  it('renders without error', () => {
    getTeamRollingAveragesByTeam.mockReturnValue(Promise.resolve(rollingAverages))

    const { getByTestId } = render(<RollingTeamAveragesComponent />)
    expect(getByTestId('teamrollingaverages')).toBeTruthy()
  })

})
