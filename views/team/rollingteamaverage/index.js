import React from 'react'
import orderby from "lodash.orderby"
import dynamic from 'next/dynamic'

const LineChart = dynamic(import('@Components/chart'),{ssr:false})
import { Panel } from "@Components/panel"
import { getTeamRollingAveragesByTeam } from '@Services/api'
import { tokenParser } from '@Services/token'

export class RollingTeamAveragesComponent extends React.Component {

  state = {
    rollingAverages: null
  }

  componentDidMount = async () => {
    const { id } = this.props
    const token = await tokenParser(undefined)
    const [
      rollingAverages
    ] = await Promise.all([
      getTeamRollingAveragesByTeam(id, token)
    ])
    this.setState({ rollingAverages })
  }

  render() {
    let columns = []

    const { rollingAverages } = this.state
    if (rollingAverages && rollingAverages.length != 0) {
      const keys = Object.keys(rollingAverages[0]).sort()
      const exclude = ['team', 'starttime', 'teamid']
      columns = keys.filter(k => !exclude.includes(k))
    }

    const sortedAverages = orderby(rollingAverages, ['starttime'], ['asc'])

    return (
      <div data-testid="teamrollingaverages">
        <Panel
          requiredData={rollingAverages}>
          <LineChart
            columns={columns}
            data={sortedAverages} />
        </Panel>
      </div>
    )
  }
}
