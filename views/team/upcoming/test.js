import React from 'react'
import { render } from 'react-testing-library'

import { matches } from '@TestData/matches.json'

import { UpcomingMatchesComponent } from './index.js'

describe('define TeamUpcoming', () => {

  it('renders without error', () => {
    const data = {"upcomingMatches": matches}
    const { getByTestId } = render(<UpcomingMatchesComponent id={"Test"} data={data} />)
    expect(getByTestId('teamupcoming')).toBeTruthy()
  })

})
