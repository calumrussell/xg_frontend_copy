import React from 'react'

import { Table } from './components/table'

export const UpcomingMatchesComponent = ({ id, data }) => {
  return (
    <div data-testid={"teamupcoming"}>
      <Table
        data={data.upcomingMatches}
        teamid={id} />
    </div>
  )
}
