import styled from "styled-components"

import { colorScheme, mq } from '@Theme'

export const Wrapper = styled.div`
  margin: 10px 0;
`

export const RowWrapper = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between; 
  &:nth-child(even){
    background: ${colorScheme.grey[100]}
  }
  span:nth-child(1){
    display: flex;
    align-items: center;
    p{
      margin-left: 0.5rem;
    }
    p:nth-child(1){
      width: 40px;
    }
  }
`

export const OppWrapper = styled.span`
  display: flex;
  align-items: center;
  a{
    margin-left:10px;
  }
  ${mq[1]}{
    img{
      display: none;
    }
  }
`
