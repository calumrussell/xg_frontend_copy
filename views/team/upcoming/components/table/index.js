import React from 'react'
import moment from "moment"

import { TeamLink, UpcomingMatchLink } from "@Components/link"
import { DefaultText } from "@Components/text"
import { SmTLogo } from "@Components/icon"

import { Wrapper, RowWrapper, OppWrapper } from "./style.js"

const Opp = ({ oppid, oppbrief, opplogo, oppname }) => {
  return (
    <OppWrapper>
      <TeamLink teamid={oppid} team={oppname}>
        <SmTLogo logo={opplogo} teamid={oppid} />
      </TeamLink>
      <TeamLink teamid={oppid} team={oppname}>
        {oppbrief ? oppbrief : oppname}
      </TeamLink>
    </OppWrapper>
  )
}

const isHome = (row, teamid) => row['homeid'] == teamid

const pickOppSideFunc = (row, ishome) => {
  return ishome
    ? ({ ishome, oppid: row.awayid, opplogo: row.alogo, oppname: row.away, oppbrief: row.abrief, starttime: row.starttime })
    : ({ ishome, oppid: row.homeid, opplogo: row.hlogo, oppname: row.home, oppbrief: row.hbrief, starttime: row.starttime })
}

const renderRow = (row, teamid) => {
  const atHome = isHome(row, teamid)
  const oppData = pickOppSideFunc(row, atHome)
  return (
    <RowWrapper key={row.matchid}>
      <span>
        <DefaultText>{moment.unix(row.starttime).format('D/M')}</DefaultText>
        <DefaultText>{atHome ? 'H' : 'A'}</DefaultText>
        <Opp {...oppData} />
      </span>
      <UpcomingMatchLink {...row}>Expected</UpcomingMatchLink>
    </RowWrapper>
  )
}

export const Table = ({ data, teamid }) => {

  const rows = data.map(r => renderRow(r, teamid))
  return <Wrapper>{rows}</Wrapper>
}
