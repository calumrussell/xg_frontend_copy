import styled from "styled-components"

import { BaseContainer } from '@Components/globals'

export const TableContainer = styled(BaseContainer)`
  table td{
    padding: 0.5rem 0rem;
  }
  table td:nth-child(1){
    width: 80px;
  }
  .simple-table-main-table{
    min-width: 600px;
  }
`
