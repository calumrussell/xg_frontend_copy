import React from 'react'
import { Table as STable, Header, Body } from 'simpletable'

import { colorScheme } from '@Theme'
import { Glossary } from "@Components/glossary"

import definition from './definition.js'
import { TableContainer } from "./style.js"

export const Table = ({ teamData, leagueData, columns }) => {
  
  const {
    rows,
    cols
  } = definition(teamData, leagueData, columns)

  return (
    <div>
      <TableContainer>
        <STable
          background={colorScheme.grey[100]}
          isStriped={colorScheme.grey[200]}
          isFullWidth={true}
          freezeFirstCol={{ headerHeight: '20px', rowHeight: '20px' }}
          cellSpacing="0">
          <Header
            columns={cols}
            titles={columns} />
          <Body>
            {[rows]}
          </Body>
        </STable>
      </TableContainer>
      <Glossary columns={columns} />
    </div>
  )
}
