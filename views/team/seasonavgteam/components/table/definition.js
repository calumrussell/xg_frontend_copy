import React from "react"
import styled from "styled-components"
import groupby from "lodash.groupby"
import { Column, Row as SRow } from 'simpletable'

import {
  toFixedAny,
  headerConverter,
  titleConverter
} from '@Functions/transform'
import { colorScheme } from "@Theme"

const FirstColWrapper = styled.div`
  display: flex;
  flex-direction: row;
  color: ${colorScheme.grey[800]};
  span{
    padding-right: 5px;
  }`

const firstCol = groupedTeam => new Column('').setLayout(row => {
  const years = groupedTeam[row]
  return <FirstColWrapper><span>{row}</span><span>{years.map((y, i) => <p key={i}>{y['league']}</p>)}</span></FirstColWrapper>
})

const colCreator = (groupedTeam, cols) => [
  firstCol(groupedTeam),
  ...cols.map(v => new Column(v)
    .setLayout(row => {
      const years = groupedTeam[row]
      return years.map((y, i) => <p key={i}>{toFixedAny(y[v], 1)}</p>)
    })
    .setColHeader(() => headerConverter(v))
    .setColHeaderTitle(() => `${titleConverter(v)}:  `))
]

const rowCreator = (groupedTeam, cols) => Object.keys(groupedTeam).map((v, i) => (
  <SRow
    key={i}
    columns={colCreator(groupedTeam, cols)}
    dataRow={v} />
))

export default (teamData, leagueData, columns) => {
  const groupedTeam = groupby(teamData, 'year')
  return {
    rows: rowCreator(groupedTeam, columns),
    cols: colCreator(groupedTeam, columns)
  }
}
