import React from 'react'
import { connect } from 'react-redux';
import orderby from "lodash.orderby"

import { ButtonsWrapper } from "@Components/button"
import { SelectAlt } from "@Components/select"
import { Filter } from "@Components/filter"
import { isMainFilter } from "@Functions/filter"
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'
import { StyledTabs, StyledTickBox } from "./style.js"

export class InnerSeasonAvgTeamComponent extends React.Component {
  /*
    Controls the filter on the data
  */

  state = {
    statGroup: 'Off',
    isMain: false,
    filters: []
  }

  componentDidMount = () => {
    //isMainFilter is a toggle, we run it once on load to set to true
    this.isMainFilter()
  }

  switchStatGroup = event => this.setState({ statGroup: event.target.value })

  switchActive = active => this.setState({ active })

  isMainFilter = event => this.state.isMain == true
    ? this.setState(state => ({ ...state, isMain: false, filters: [...state.filters.filter(f => f.id != 2)] }))
    : this.setState(state => ({ ...state, isMain: true, filters: [...state.filters, isMainFilter(true)] }))

  render() {
    const {
      statGroup,
      filters,
      isMain,
      active
    } = this.state

    const {
      seasonAverages,
      leaguePercentile
    } = this.props.data

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)

    return (
      <div data-testid="teamseasonavgteam">
        <ButtonsWrapper>
          <SelectAlt
            values={statGroupNames}
            onChange={this.switchStatGroup}
            selected={statGroup}
            title={"Stat Category"} />
          <StyledTickBox
            label={'Main Tourns:'}
            isChecked={isMain}
            handleCheck={this.isMainFilter} />
        </ButtonsWrapper>
        <Filter
          names={['teamData', 'leagueData']}
          filters={filters.map(e => e.impl)} >
          <Table
            columns={dataKeys}
            leagueData={leaguePercentile}
            teamData={seasonAverages} />
        </Filter>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "SeasonAvgTeam"))
})
export const SeasonAvgTeamComponent = connect(null, mapDispatchToProps)(InnerSeasonAvgTeamComponent)
