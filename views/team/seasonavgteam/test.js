import React from 'react'
import { render } from 'react-testing-library'

import { seasonAverages } from '@TestData/seasonaverages.json'
import { leaguePercentile } from '@TestData/leaguepercentile.json'

import { InnerSeasonAvgTeamComponent } from './index.js'

let mockCategories;

describe('define TeamSeasonAvgTeam', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal', 'passkey', 'assist'], 
        "statGroupNames": ['Test']
      }
    })
  })

  it('renders without error', () => {
    const data = {"seasonAverages": seasonAverages, "leaguePercentile": leaguePercentile}
    const { getByTestId } = render(<InnerSeasonAvgTeamComponent getComponentDataCategories={mockCategories} data={data} />)
    expect(getByTestId('teamseasonavgteam')).toBeTruthy()
  })

})
