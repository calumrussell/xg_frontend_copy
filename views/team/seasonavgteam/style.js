import styled from "styled-components"

import { Tabs } from "@Components/tabs"
import { TickBox } from "@Components/tickbox"

export const StyledTabs = styled(Tabs)`
  font-size: 0.75rem;
  margin: 0;
  header{
    min-width: 50px;
    font-size: 0.75rem;
  }
`

export const StyledTickBox = styled(TickBox)`
  display: flex;
  justify-content: flex-end;
  padding: 0.5rem 0rem;
  p{
    font-size: 0.75rem;
  }
`

