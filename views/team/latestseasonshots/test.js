import React from 'react'
import { render } from 'react-testing-library'

import { shots } from '@TestData/shots.json'
import { getLatestSeasonShotsByTeam } from '@Services/api'

import { LatestSeasonShotsComponent } from './index.js'

jest.mock('@Services/api')

describe('define TeamShots', () => {

  it('renders without error', () => {
    getLatestSeasonShotsByTeam.mockReturnValue(Promise.resolve(shots))

    const { getByTestId } = render(<LatestSeasonShotsComponent />)
    expect(getByTestId('teamshots')).toBeTruthy()
  })

})
