import React from 'react'
import { render } from 'react-testing-library'

import { playerAverages } from '@TestData/playeraverages.json'

import { InnerSquadAveragesTeamComponent } from './index.js'

let mockCategories;

describe('define TeamSquadAverages', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal', 'passkey', 'assist'], 
        "statGroupNames": ['Test']
      }
    })
  })

  it('renders without error', () => {
    const data = {"squadAverages": playerAverages}
    const { getByTestId } = render(<InnerSquadAveragesTeamComponent getComponentDataCategories={mockCategories} data={data} />)
    expect(getByTestId('teamsquadaverages')).toBeTruthy()
  })

})
