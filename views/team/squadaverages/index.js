import React from 'react'
import { connect } from 'react-redux'

import { ButtonsWrapper } from "@Components/button"
import { PositionSelect, StatGroupSelect } from "@Components/select"
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'

export class InnerSquadAveragesTeamComponent extends React.Component {

  state = {
    position: 'Midfielder',
    statGroup: 'Off'
  }

  switchPosition = event => this.setState({ position: event.target.value })
  switchStatGroup = event => this.setState({ statGroup: event.target.value })

  render() {
    const { position, statGroup } = this.state
    const { squadAverages } = this.props.data

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)

    return (
      <div data-testid="teamsquadaverages">
        <ButtonsWrapper>
          <span>
            <PositionSelect
              changeFunc={this.switchPosition}
              selected={position} />
            <StatGroupSelect
              options={statGroupNames}
              changeFunc={this.switchStatGroup}
              selected={statGroup} />
          </span>
        </ButtonsWrapper>
        <Table
          data={squadAverages}
          columns={dataKeys}
          position={position} />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "SquadAvgTeam"))
})
export const SquadAveragesComponent = connect(null, mapDispatchToProps)(InnerSquadAveragesTeamComponent)
