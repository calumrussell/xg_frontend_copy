import React from "react"

import { Button } from "@Components/button"
import { SectionTitle } from "@Components/section"
import { Input, FormStyle } from "./style.js"

export const ConfirmUserCreationForm = ({
  submitConfirm,
  confirmationCode,
  catchChange
}) => {
  return (
    <FormStyle onSubmit={submitConfirm}>
      <SectionTitle>Confirmation Code</SectionTitle>
      <Input
        type="text"
        value={confirmationCode}
        name='confirmationCode'
        placeholder='Input Code'
        onChange={catchChange} />
      <Button>Submit</Button>
    </FormStyle>
  )
}
