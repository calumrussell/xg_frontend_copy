import React from "react"
import { Link } from '@Routes'

import { Button } from "@Components/button"
import { SectionTitle, OffsetText } from "@Components/text"
import {
  Input,
  StyledButton,
  FormStyle
} from "./style.js"

export const CreateUserForm = ({ submitForm, email, password, confirm, catchChange, redirect }) => {
  return (
    <FormStyle onSubmit={submitForm}>
      <SectionTitle>Create Account</SectionTitle>
      <Input type="email" value={email} name='email' placeholder='Email' onChange={catchChange} />
      <OffsetText>Password must contain uppercase letter and number</OffsetText>
      <Input type="password" value={password} name='password' placeholder='Password' onChange={catchChange} />
      <Input type="password" value={confirm} name='confirm' placeholder='Confirm Password' onChange={catchChange} />
      <StyledButton>Create</StyledButton>
      {
        redirect != '/' ?
          <div>Already a user? <Link route={`/login/${redirect}`} >Login</Link></div> :
          <div></div>
      }
    </FormStyle>
  )
}
