import React from 'react'
import { connect } from 'react-redux'
import { Router } from '@Routes'

import { Section } from '@ComponentLib'
import { createUser, confirmUser } from '@Redux/actions/User'
import { createMessage } from '@Redux/actions/Message'
import { CreateUserForm } from "./components/createuser"
import { ConfirmUserCreationForm } from "./components/confirmusercreation"

class InnerCreate extends React.Component {

  state = {
    email: '',
    password: '',
    confirm: '',
    confirmationCode: ''
  }

  catchChange = event => this.setState({ [event.target.name]: event.target.value })

  submitForm = (event) => {
    event.preventDefault()
    if (this.state.password == this.state.confirm) {
      this.props.userCreate(this.state.email, this.state.password)
    } else {
      this.props.messageCreate('Passwords don\'t match', -1)
    }
  }

  submitConfirm = (event) => {
    event.preventDefault()
    this.props.userConfirm(this.state.email, this.state.confirmationCode)
  }

  render() {
    const { isCreated, redirect, isVerified } = this.props

    if(isVerified){
      Router.pushRoute("/login/" + this.props.redirect)
    }

    return (
      <Section>
        {
          !isCreated
            ? <CreateUserForm {...this.state} redirect={redirect} catchChange={this.catchChange} submitForm={this.submitForm} />
            : null
        }
        {
          isCreated
            ? <ConfirmUserCreationForm {...this.state} submitConfirm={this.submitConfirm} catchChange={this.catchChange} />
            : null
        }
      </Section>
    )
  }
}

const mapStateToProps = ({ user }) => {
  return {
    isLoggedIn: user.isLoggedIn,
    isCreated: user.isCreated,
    isVerified: user.isVerified
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userCreate: (e, pwd) => dispatch(createUser(e, pwd)),
    messageCreate: (mess, mood) => dispatch(createMessage(mess, mood)),
    userConfirm: (e, confirm) => dispatch(confirmUser(e, confirm))
  }
}

export const Create = connect(mapStateToProps, mapDispatchToProps)(InnerCreate)
