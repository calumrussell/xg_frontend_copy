import React from "react"
import jwt_decode from "jwt-decode"

import { Section } from "@Components/section"
import { UserDetailWrapper } from "./style.js"

const UserDetail = (props) => {
  return (
    <UserDetailWrapper>
      <p>Email: {props.email}</p>
      <p>Plan: {props['cognito:groups'].join(', ')}</p>
    </UserDetailWrapper>
  )
}

export class Profile extends React.Component {

  render() {
    const idToken = localStorage.getItem("userIdToken")
    const decodedId = jwt_decode(idToken)

    return (
      <Section>
        {
          decodedId != null ?
            <UserDetail {...decodedId} /> :
            null
        }
      </Section>
    )
  }
}
