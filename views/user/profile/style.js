import styled from "styled-components"

import { colorScheme } from '@Theme'

export const UserDetailWrapper = styled.div`
  background: white;
  font-size: 1.5rem;
  padding: 20px;
  border-top: ${colorScheme.grey[700]} 2px solid;
`
