import styled from 'styled-components'

import { Button } from "@Components/button"
import { colorScheme } from '@Theme'

export const StyledButton = styled(Button)`
  margin: 0px;
  margin-top: 10px;
`

export const FormStyle = styled.form`
  display: flex;
  flex-direction: column;
  max-width: 400px;
  margin: 0 auto;
  padding: 1rem;
`

export const Input = styled.input`
  padding: 0.5rem;
  font-size: 1.2rem;
  margin-bottom: 5px;
  border: none;
  background: transparent;
  &:focus{
    outline: none;
    background: ${colorScheme.grey[200]};
    border: none;
  }
  &&&{
    border-bottom: 1px solid ${colorScheme.bluegrey[700]};
  }
`
