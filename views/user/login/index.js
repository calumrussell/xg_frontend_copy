import React from 'react'
import { Router } from '@Routes'
import { connect } from 'react-redux'

import { Section } from "@Components/section"
import { SectionTitle } from "@Components/text"
import { loginUser } from '@Redux/actions/User'
import {
  StyledButton,
  FormStyle,
  Input
} from "./style.js"

class InnerLogin extends React.Component {

  state = {
    email: '',
    password: '',
    redirect: '/'
  }

  catchChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  componentDidMount = () => {
    if (this.props.redirect) {
      this.setState({ redirect: '/' + this.props.redirect })
    }
  }

  redirect = () => Router.pushRoute(this.state.redirect)

  submitForm = (event) => {
    event.preventDefault()
    const success = this.props.userLogin(this.state.email, this.state.password)
    if (success) {
      this.redirect()
    }
  }

  render() {
    const { email, password } = this.state
    const { isLoggedIn } = this.props
    if (isLoggedIn) {
      this.redirect()
      return null
    } else {
      return (
        <Section>
          <FormStyle onSubmit={this.submitForm}>
            <SectionTitle>Login</SectionTitle>
            <Input type="text" value={email} name='email' placeholder='Email' onChange={this.catchChange} />
            <Input type="password" value={password} name='password' placeholder='Password' onChange={this.catchChange} />
            <StyledButton>Submit</StyledButton>
          </FormStyle>
        </Section >
      )
    }
  }
}

const mapStateToProps = ({ user }) => {
  return {
    isLoggedIn: user.isLoggedIn
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (e, pwd) => dispatch(loginUser(e, pwd))
  }
}

export const LoginForm = connect(mapStateToProps, mapDispatchToProps)(InnerLogin)
