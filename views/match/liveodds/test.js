import React from 'react'
import { render } from 'react-testing-library'

import { matches } from '@TestData/matches.json'

import { LiveOddsComponent } from './index.js'

describe('define MatchLiveOdds', () => {

  it('renders without error', () => {
    const data = {"matchSummary": [matches[0]], "liveOdds": []}
    const { getByTestId } = render(<LiveOddsComponent data={data}/>)
    expect(getByTestId('matchliveodds')).toBeTruthy()
  })

})
