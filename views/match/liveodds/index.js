import React from "react"
import dynamic from 'next/dynamic'

import { DefaultText } from "@Components/text"
import { RequiresData } from "@Components/monad"

const LiveOddsChart = dynamic(import('@Charts'), { ssr: false })

export const LiveOddsComponent = props => {

  const { liveOdds } = props.data
  const {
    hBrief,
    aBrief,
    home,
    away
  } = props.data.matchSummary[0]

  const homeTeam = hBrief ? hBrief : home
  const awayTeam = aBrief ? aBrief : away

  return (
    <div data-testid='matchliveodds'>
      <RequiresData data={liveOdds}>
        <RequiresData.WhenTrue>
          <LiveOddsChart
            homeTeam={homeTeam}
            awayTeam={awayTeam}
            data={liveOdds} />
        </RequiresData.WhenTrue>
        <RequiresData.Default>
          <DefaultText>No odds data for this match</DefaultText>
        </RequiresData.Default>
      </RequiresData>
    </div>
  )
}
