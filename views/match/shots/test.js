import React from 'react'
import { render } from 'react-testing-library'

import { shots } from '@TestData/shots.json'
import { getShotsByMatch } from '@Services/api'

import { ShotsComponent } from './index.js'

jest.mock('@Services/api')

describe('define MatchShots', () => {

  it('renders without error', () => {
    getShotsByMatch.mockReturnValue(Promise.resolve(shots))

    const { getByTestId } = render(<ShotsComponent />)
    expect(getByTestId('matchshots')).toBeTruthy()
  })

})
