import React from "react"

import { HalfPitchWithShots } from "@Components/pitch"
import { isGoalFilter } from "@Functions/filter"
import { Filter, FilterButton } from "@Components/filter"
import { RequiresData } from "@Components/monad"
import { ButtonsWrapper } from "@Components/button"
import { DefaultText } from "@Components/text"
import { getShotsByMatch } from '@Services/api'
import { tokenParser } from '@Services/token'

export class ShotsComponent extends React.Component {

  state = {
    filters: [isGoalFilter()],
    matchShots: []
  }

  componentDidMount = async () => {
    const { id } = this.props
    const token = await tokenParser(undefined)
    const [
      matchShots
    ] = await Promise.all([
      getShotsByMatch(id, token)
    ])
    this.setState({ matchShots })
  }

  addFilter = filter => this.setState(state => ({ ...state, filters: [...state.filters, filter] }))

  removeFilter = id => {
    this.setState(state => ({
      ...state,
      filters: [
        ...state.filters.filter(f => f.id != id)
      ]
    }))
  }

  render() {
    const {
      filters,
      matchShots
    } = this.state

    return (
      <div data-testid="matchshots">
        <RequiresData data={matchShots}>
          <RequiresData.WhenTrue>
            <ButtonsWrapper selector={"stat-panel-shotsbymatch-text"}>
              <FilterButton
                selector={"stat-panel-shotsbymatch-text"}
                statData={matchShots}
                existingFilters={filters}
                addFilter={this.addFilter}
                removeFilter={this.removeFilter} />
            </ButtonsWrapper>
            <Filter
              names={['data']}
              filters={filters.map(e => e.impl)} >
              <HalfPitchWithShots
                filters={filters}
                data={matchShots} />
            </Filter>
          </RequiresData.WhenTrue>
          <RequiresData.Default>
            <DefaultText>No Shots Found</DefaultText>
          </RequiresData.Default>
        </RequiresData>
      </div>
    )
  }
}
