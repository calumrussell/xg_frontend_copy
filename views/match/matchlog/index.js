import React from 'react'
import { connect } from "react-redux"

import { luSort } from '@Functions/sort'
import { DefaultText } from '@Components/text'
import { RequiresData } from '@Components/monad'
import { StatGroupSelect } from '@Components/select'
import { ButtonsWrapper } from '@Components/button'
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'

export class InnerMatchLogComponent extends React.Component {

  state = {
    statGroup: 'Off'
  }

  switchStatGroup = event =>
    this.setState({ statGroup: event.target.value })

  getCategories = statGroup =>
    this.props.category[statGroup].categories

  splitPlayerStatsByVenue = stats => ({
    homeStats: stats.filter(p => p.ishome == 1),
    awayStats: stats.filter(p => p.ishome != 1)
  })

  render() {
    const { statGroup } = this.state
    const { playerStats } = this.props.data

    const {
      homeStats,
      awayStats
    } = this.splitPlayerStatsByVenue(playerStats)

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)

    return (
      <div data-testid='matchmatchlog'>
        <RequiresData data={playerStats}>
          <RequiresData.WhenTrue>
            <ButtonsWrapper>
              <StatGroupSelect
                options={statGroupNames}
                changeFunc={this.switchStatGroup}
                selected={statGroup} />
            </ButtonsWrapper>
            <Table
              data={luSort(homeStats)}
              columns={dataKeys} />
            <Table
              data={luSort(awayStats)}
              columns={dataKeys} />
          </RequiresData.WhenTrue>
          <RequiresData.Default>
            <DefaultText>Match Log not found</DefaultText>
          </RequiresData.Default>
        </RequiresData>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "MatchLogPlayer"))
})
export const MatchLogComponent = connect(null, mapDispatchToProps)(InnerMatchLogComponent)
