import React from 'react'
import { render } from 'react-testing-library'

import { playerStats } from '@TestData/matchstats.json'

import { InnerMatchLogComponent } from './index.js'

let mockCategories;

describe('define MatchMatchLog', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal', 'passkey', 'assist'], 
        "statGroupNames": ['Test']
      }
    })
  })

  it('renders without error', () => {
    const data = {"playerStats": playerStats}
    const { getByTestId } = render(<InnerMatchLogComponent getComponentDataCategories={mockCategories} data={data} />)
    expect(getByTestId('matchmatchlog')).toBeTruthy()
  })

})
