import React from 'react'
import { Table as STable, Header, Column, Row as SRow, Body } from 'simpletable'

import { titleConverter } from '@Functions/transform'
import { createSum } from '@Functions/calculate'
import { TableContainer } from '@Components/globals'
import { colorScheme } from '@Theme'

import { definition } from './definition.js'

export const Table = ({ data, columns }) => {

  const bannedKeys = ['formatname', 'opp', 'oppid', 'matchid', 'starttime',
    'goalsfor', 'goalsagainst', 'ishome', 'team', 'teamid', 'position', 'minutesplayed']
  const filteredCols = columns.filter(v => !bannedKeys.includes(v))

  const avgFiltered = filteredCols.filter(v => !['position', 'minutesplayed'].includes(v))
  const avgCols = [new Column("Total").setLayout(row => "Total"), new Column(""), new Column(""),
  ...avgFiltered.map(v => new Column(v).setLayout(row => row[v]))]

  const cols = definition(filteredCols)

  const team = data.length != 0 ? data[0]['team'] : ''
  const titles = ["", "", "", ...filteredCols.map(v => titleConverter(v))]

  const rows = data.map((v, i) => <SRow key={i} columns={cols} dataRow={v} />)
  return (
    <React.Fragment>
      <TableContainer>
        <STable
          background={colorScheme.grey[100]}
          isStriped={colorScheme.grey[200]}
          isFullWidth={true}
          freezeFirstCol={{ headerHeight: '20px', rowHeight: '20px' }}
          cellSpacing="0">
          <Header columns={cols} titles={titles} />
          <Body>
            {[rows]}
            <SRow dataRow={createSum(data)} columns={avgCols} />
          </Body>
        </STable>
      </TableContainer>
    </React.Fragment>
  )
}
