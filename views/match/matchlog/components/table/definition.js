import React from "react"
import { Column } from 'simpletable'

import { headerConverter, titleConverter } from '@Functions/transform'
import { PlayerLink } from '@Components/link'

const nameCol = new Column('formatname')
  .setLayout(row => {
    return <PlayerLink {...row}>{row['formatname']}</PlayerLink>
  })
  .setColHeader(() => "")

const positionCol = new Column('position')
  .setColHeader(() => headerConverter('position'))
  .setColHeaderTitle(() => `${titleConverter('position')}:  `)

const minutesPlayedCol = new Column('minutesplayed')
  .setColHeader(() => headerConverter('minutesplayed'))
  .setColHeaderTitle(() => `${titleConverter('minutesplayed')}:  `)

const lastCols = cols => [...cols.map(v => new Column(v)
  .setColHeader(() => headerConverter(v))
  .setColHeaderTitle(() => `${titleConverter(v)}:  `))]

export const definition = cols => [nameCol, positionCol, minutesPlayedCol, ...lastCols(cols)]
