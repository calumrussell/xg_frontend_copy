import React from "react"

import { DefaultText } from "@Components/text"
import { RequiresData } from "@Components/monad"
import { PitchWithPlayers } from "@Components/pitch"

export const LineupComponent = props => {
  const { playerStats } = props.data

  return (
    <div data-testid='matchlineup'>
      <RequiresData data={playerStats}>
        <RequiresData.WhenTrue>
        <PitchWithPlayers
          players={playerStats} />
        </RequiresData.WhenTrue>
        <RequiresData.Default>
          <DefaultText>Lineup not found</DefaultText>
        </RequiresData.Default>
      </RequiresData>
    </div>
  )
}
