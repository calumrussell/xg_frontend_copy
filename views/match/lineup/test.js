import React from 'react'
import { render } from 'react-testing-library'

import { playerStats } from '@TestData/matchstats.json'

import { LineupComponent } from './index.js'

describe('define MatchLineup', () => {

  it('renders without error', () => {
    const data = {"playerStats": playerStats}
    const { getByTestId } = render(<LineupComponent data={data}/>)
    expect(getByTestId('matchlineup')).toBeTruthy()
  })

})
