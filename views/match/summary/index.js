import React from 'react'

import { RequiresData } from "@Components/monad"
import { 
  DefaultText,
  SubTitle,
  OffsetText
} from "@Components/text"

import { Table } from "./components/table"

export class SummaryComponent extends React.Component {

  splitTeamStatsByVenue = stats => ({
    homeTeam: stats.filter(t => t.ishome == 1)[0],
    awayTeam: stats.filter(t => t.ishome != 1)[0]
  })

  render() {
    const {
      teamStats,
      seasonAverages
    } = this.props.data
    const {
      homeTeam,
      awayTeam
    } = this.splitTeamStatsByVenue(teamStats)

    return (
      <div data-testid="matchsummary">
        <RequiresData data={homeTeam}>
          <RequiresData.WhenTrue>
            <OffsetText>Season average in centre</OffsetText>
            <Table
              homeTeam={homeTeam}
              awayTeam={awayTeam}
              seasonAverages={seasonAverages} />
          </RequiresData.WhenTrue>
          <RequiresData.Default>
            <DefaultText>Match Not Found</DefaultText>
          </RequiresData.Default>
        </RequiresData>
      </div>
    )
  }
}
