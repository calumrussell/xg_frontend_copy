import React from 'react'
import { render } from 'react-testing-library'

import { teamStats } from '@TestData/teamstats.json'
import { seasonAverages } from '@TestData/seasonaverages.json'

import { SummaryComponent } from './index.js'

describe('define MatchSummary', () => {

  it('renders without error', () => {
    const data = {
      teamStats,
      seasonAverages
    }
    const { getByTestId } = render(<SummaryComponent data={data}/>)
    expect(getByTestId('matchsummary')).toBeTruthy()
  })

})
