import React from "react"

import { 
  numberConverter,
  titleConverter,
  toFixedAny
} from "@Functions/transform"
import {
  SubTitle,
  DefaultText,
  OffsetText
} from '@Components/text'

import { getStatFromSeasonAverages } from "./helpers.js"
import { 
  StatWrapper,
  StatTitle,
  StatNumbers,
  RowWrapper
} from "./style.js"

const Row = ({ homeTeam, awayTeam, seasonAverages, stat }) => {
  const noRound = ['goalsfor']
  const homeStat = noRound.includes(stat) 
    ? homeTeam[stat] 
    : numberConverter(homeTeam[stat], stat, false)
  const awayStat = noRound.includes(stat) 
    ? awayTeam[stat] 
    : numberConverter(awayTeam[stat], stat, false)
  const seasonAverage = getStatFromSeasonAverages(stat, seasonAverages)
  return (
    <StatWrapper>
      <StatTitle>
        <SubTitle>{titleConverter(stat)}</SubTitle>
      </StatTitle>
      <StatNumbers>
        <DefaultText>{homeStat}</DefaultText>
        <OffsetText>{toFixedAny(seasonAverage, 1)}</OffsetText>
        <DefaultText>{awayStat}</DefaultText>
      </StatNumbers>
    </StatWrapper>
  )
}

export const Table = props => {
  const titles = ['goalsfor', 'xg', 'shottotal', 'shotontarget', 'foulcommitted', 'cornertaken',
    'passcrosstotal', 'passtotal', 'savetotal', 'offrating', 'defrating']

  return (
    <React.Fragment>
      <RowWrapper>
        {
          titles.map((title, i) => <Row key={i} {...props} stat={title} />)
        }
      </RowWrapper>
    </React.Fragment>
  )
}
