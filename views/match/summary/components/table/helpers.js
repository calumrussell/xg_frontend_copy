import { numberConverter } from '@Functions/transform'

export const getStatFromSeasonAverages = (stat, seasonAverages) => seasonAverages != undefined
  ? stat in seasonAverages
    ? numberConverter(seasonAverages[stat], stat, false)
    : ''
  : ''
