import styled from 'styled-components'

export const RowWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const StatWrapper = styled.div`
  div{
    width: 100%;
  }
`
export const StatTitle = styled.div`
  display: flex;
  justify-content: center;
`

export const StatNumbers = styled.div`
  margin: 0.5rem 0;
  p{
    margin: 0;
  }
  display: flex;
  justify-content: space-around;
`
