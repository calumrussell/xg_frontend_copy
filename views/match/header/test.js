import React from 'react'
import { render } from 'react-testing-library'

import { matches } from '@TestData/matches.json'
import { playerStats } from '@TestData/matchstats.json'

import { HeaderComponent } from './index.js'

describe('define MatchHeader', () => {

  it('renders without error', () => {
    const data = {"matchSummary": [matches[0]], "playerStats": playerStats}
    const { getByTestId } = render(<HeaderComponent data={data}/>)
    expect(getByTestId('matchheader')).toBeTruthy()
  })

})
