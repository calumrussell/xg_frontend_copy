import styled from "styled-components"

import { colorScheme } from '@Theme'
import { OffsetText } from "@Components/text"

export const LiveText = styled(OffsetText)`
  color: ${colorScheme.green[700]};
  font-size: 1rem;
  margin: 0;
`

export const PeriodText = styled(OffsetText)``

export const TeamWrapper = styled.span`
  margin: 0.5rem 0;
`

export const HeaderWrapper = styled.div`
  span{
    display: flex;
    align-items:center;
    span {
      padding-right: 0.5rem;
    }
  }
`
