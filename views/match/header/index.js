import React from "react"

import { SubTitle, SectionTitle, OffsetText } from "@Components/text"
import { TeamLink } from "@Components/link"
import { SmTLogo } from "@Components/icon"
import {
  LiveText,
  PeriodText,
  TeamWrapper,
  HeaderWrapper
} from "./style.js"

export const HeaderComponent = ({ data }) => {

  const {
    home,
    away,
    homeftscore,
    awayftscore,
    hlogo,
    alogo,
    homeid,
    awayid,
    period,
    match_time
  } = data.matchSummary[0]

  const maxMin = Math.max(...data.playerStats.map(v => v['minutesplayed']))

  return (
    <HeaderWrapper data-testid='matchheader'>
      <span>
        <PeriodText>{period}</PeriodText>
      </span>
      <TeamWrapper>
        <span><SectionTitle><b>{homeftscore}</b></SectionTitle></span>
        {hlogo ? <span><SmTLogo teamid={homeid} logo={hlogo} /></span> : null}
        <span>
          <SubTitle>
            <TeamLink teamid={homeid} team={home}>
              {home}
            </TeamLink>
          </SubTitle>
        </span>
      </TeamWrapper>
      <TeamWrapper>
        <span><SectionTitle><b>{awayftscore}</b></SectionTitle></span>
        {alogo ? <span><SmTLogo teamid={awayid} logo={alogo} /></span> : null}
        <span>
          <SubTitle>
            <TeamLink teamid={awayid} team={away}>
              {away}
            </TeamLink>
          </SubTitle>
        </span>
      </TeamWrapper>
      <span>
        {match_time != -1 ? <LiveText>{match_time}'</LiveText> : null}
      </span>
      <span>
        <OffsetText>Last event: {maxMin}'</OffsetText>
      </span>
    </HeaderWrapper>
  )
}
