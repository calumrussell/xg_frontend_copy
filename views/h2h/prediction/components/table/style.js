import styled from "styled-components"

export const PredictionSection = styled.div`
  h1{
    font-weight: normal;
  }
`
export const PredictionRowWrap = styled.div`
  display: flex;
  justify-content: space-evenly;
`
