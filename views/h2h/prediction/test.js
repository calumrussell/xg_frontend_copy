import React from 'react'
import { render } from 'react-testing-library'

import { PredictionComponent } from './index.js'

describe('define H2HPrediction', () => {

  it('renders without error', () => {
    const { getByTestId } = render(<PredictionComponent />)
    expect(getByTestId('h2hprediction')).toBeTruthy()
  })

})
