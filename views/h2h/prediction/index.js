import React from 'react'

import { MaybeData } from "@Components/monad"
import { DefaultText } from "@Components/text"

import { Table } from './components/table'

export const PredictionComponent = ({ matchPrediction }) => {
  return (
    <div data-testid="h2hprediction">
      <MaybeData data={matchPrediction}>
        <MaybeData.WhenTrue>
          <Table data={matchPrediction} />
        </MaybeData.WhenTrue>
        <MaybeData.WhenEmpty><DefaultText>No Prediction</DefaultText></MaybeData.WhenEmpty>
      </MaybeData>
    </div>
  )
}
