import React from 'react'
import { render } from 'react-testing-library'

import { teamAverages } from '@TestData/teamaverages.json'
import { teamStats } from '@TestData/teamstats.json'

import { SummaryComponent } from './index.js'

describe('define H2HSummary', () => {

  it('renders without error', () => {
    const data = {
      "homeTeamAvg": teamAverages,
      "awayTeamAvg": teamAverages,
      "homeTeamStats": teamStats,
      "awayTeamStats": teamStats
    }
    const { getByTestId } = render(<SummaryComponent {...data} />)
    expect(getByTestId('h2hsummary')).toBeTruthy()
  })

})
