import React from "react"

import { DefaultText } from "@Components/text"
import { numberConverter, titleConverter } from "@Functions/transform"

import { TableContainer, StyledTable } from "./style.js"

const Row = ({ homeTeamAvg, awayTeamAvg, stat }) => {
  return (
    <tr>
      <td><DefaultText>{numberConverter(homeTeamAvg[stat], stat, true)}</DefaultText></td>
      <td><DefaultText>{titleConverter(stat)}</DefaultText></td>
      <td><DefaultText>{numberConverter(awayTeamAvg[stat], stat, true)}</DefaultText></td>
    </tr>
  )
}


export const Table = (props) => {
  const titles = [
    'goalsfor', 'goalsagainst', 'xg',
    'shottotal', 'shotontarget', 'foulcommitted',
    'passcorner','passcrosstotal', 'passtotal',
    'savetotal', 'offrating', 'defrating']
  return (
    <TableContainer>
      <StyledTable>
        <tbody>
          {
            titles.map((title, i) => <Row key={i} {...props} stat={title} />)
          }
        </tbody>
      </StyledTable>
    </TableContainer>
  )
}
