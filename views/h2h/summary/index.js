import React from "react"

import { createAverage } from "@Functions/calculate"

import { Table } from "./components/table"

export class SummaryComponent extends React.Component {

  calcAverages = ({ homeTeamStats, awayTeamStats }) => ({
    homeTeamAvg: createAverage(homeTeamStats),
    awayTeamAvg: createAverage(awayTeamStats)
  })

  render() {
    const {
      homeTeamAvg,
      awayTeamAvg
    } = this.calcAverages(this.props)
    return (
      <div data-testid="h2hsummary">
        <Table
          {...this.props}
          homeTeamAvg={homeTeamAvg}
          awayTeamAvg={awayTeamAvg} />
      </div>
    )
  }
}
