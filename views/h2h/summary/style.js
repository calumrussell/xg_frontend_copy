import styled from "styled-components"
import { colorScheme } from '@Theme'

const TableContainer = styled.div`
  max-width: 300px;
  margin: 20px auto;
`

const StyledTable = styled.table`
  margin: 0 auto;
  width: 100%;
  color: ${colorScheme.grey[800]};
  td{
    text-align: center;
    vertical-align: middle;
    padding: 5px;
  }
`
