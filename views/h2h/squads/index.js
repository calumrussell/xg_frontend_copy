import React from 'react'
import { connect } from "react-redux"

import { ButtonsWrapper } from '@Components/button'
import { SectionTitle } from '@Components/text'
import { PositionSelect, StatGroupSelect } from '@Components/select'
import { stripDashes } from '@Functions/transform'
import { getComponentDataCategories } from '@Redux/actions/Category'

import { Table } from './components/table'

export class InnerSquads extends React.Component {

  state = {
    position: 'Midfielder',
    statGroup: 'Off'
  }

  switchPosition = event =>
    this.setState({ position: event.target.value })

  switchStatGroup = event =>
    this.setState({ statGroup: event.target.value })

  render() {
    const {
      position,
      statGroup
    } = this.state
    const {
      homeTeamPlayerAvg,
      awayTeamPlayerAvg,
      home,
      away
    } = this.props

    const {
      statGroupNames,
      dataKeys
    } = this.props.getComponentDataCategories(statGroup)

    return (
      <div data-testid="h2hsquads">
        <ButtonsWrapper>
          <span>
            <PositionSelect
              changeFunc={this.switchPosition}
              selected={position} />
            <StatGroupSelect
              options={statGroupNames}
              changeFunc={this.switchStatGroup}
              selected={statGroup} />
          </span>
        </ButtonsWrapper>
        <SectionTitle>{stripDashes(home)}</SectionTitle>
        <Table
          data={homeTeamPlayerAvg}
          columns={dataKeys}
          position={position} />
        <SectionTitle>{stripDashes(away)}</SectionTitle>
        <Table
          data={awayTeamPlayerAvg}
          columns={dataKeys}
          position={position} />
      </div>
    )
  }
}


const mapDispatchToProps = dispatch => ({
  getComponentDataCategories: statGroup => dispatch(getComponentDataCategories(statGroup, "SquadAvgTeam"))
})
const mapStateToProps = ({ category }) => ({ category })
export const SquadsComponent = connect(mapStateToProps, mapDispatchToProps)(InnerSquads)
