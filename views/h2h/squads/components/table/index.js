import React from 'react'
import {
  Table as STable,
  Header,
  Column,
  Row as SRow,
  Body
} from 'simpletable'

import {
  toFixedAny,
  titleConverter,
  headerConverter,
  numberConverter
} from '@Functions/transform'
import { TableContainer } from '@Components/globals'
import { PlayerLink } from '@Components/link'
import { colorScheme } from '@Theme'

export const Table = ({ data, columns, position }) => {
  const bannedKeys = ['playerid', 'dkposition', 'position', 'formatname', 'minutesplayed', 'team']
  const filteredCols = columns.filter(v => !bannedKeys.includes(v))
  const filteredData = data.filter(v => v.position == position)

  const nameCol = new Column('formatname')
    .setLayout(row => <PlayerLink {...row}>{row['formatname']}</PlayerLink>)
    .setColHeaderTitle(() => `Name:  `)
    .setColHeader(() => '')
  const minutesPlayedCol = new Column('minutesplayed')
    .setLayout(row => toFixedAny(row['minutesplayed'], 2))
    .setColHeaderTitle(() => `Minutes Played:  `)
    .setColHeader(() => 'MP')

  const titles = ["", "", ...filteredCols.map(v => titleConverter(v))]
  const cols = [nameCol, minutesPlayedCol,
    ...filteredCols.map(v => new Column(v)
      .setLayout(row => numberConverter(row[v], v, true))
      .setColHeader(() => headerConverter(v))
      .setColHeaderTitle(() => `${titleConverter(v)}:  `))]

  return (
    <div style={{ position: 'relative', marginLeft: '-5px' }}>
      <TableContainer>
        <STable
          background={colorScheme.grey[100]}
          isStriped={colorScheme.grey[200]}
          isFullWidth={true}
          freezeFirstCol={{ headerHeight: '20px', rowHeight: '10px' }}
          cellSpacing="0">
          <Header columns={cols} titles={titles} />
          <Body>
            {
              filteredData.map((v, i) => <SRow key={i} columns={cols} dataRow={v} />)
            }
          </Body>
        </STable>
      </TableContainer>
    </div>
  )
}
