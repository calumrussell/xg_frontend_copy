import React from 'react'
import { render } from 'react-testing-library'

import { playerAverages } from '@TestData/playeraverages.json'

import { InnerSquads } from './index.js'

let mockCategories

describe('define H2HSquads', () => {

  beforeEach(() => {
    mockCategories = jest.fn(() => {
      return {
        "dataKeys": ['goalnormal', 'xg', 'shottotal', 'shotontarget', 'passtotal', 'passkey', 'assist'], 
        "statGroupNames": ['Test']
      }
    })
  })

  it('renders without error', () => {
    const data = {
      "homeTeamPlayerAvg": playerAverages,
      "awayTeamPlayerAvg": playerAverages,
      "home": "Test",
      "away": "Test"
    }
    const { getByTestId } = render(<InnerSquads {...data} getComponentDataCategories={mockCategories} />)
    expect(getByTestId('h2hsquads')).toBeTruthy()
  })

})
