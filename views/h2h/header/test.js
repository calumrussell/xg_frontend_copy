import React from 'react'
import { render } from 'react-testing-library'

import { matches } from '@TestData/matches.json'

import { HeaderComponent } from './index.js'

describe('define H2HHeader', () => {

  it('renders without error', () => {
    const data = {"matchSummary": matches}
    const { getByTestId } = render(<HeaderComponent data={data} />)
    expect(getByTestId('h2hheader')).toBeTruthy()
  })

})
