import React from "react"

import { SubTitle, SectionTitle } from "@Components/text"
import { TeamLink } from "@Components/link"
import { SmTLogo } from "@Components/icon"

import { HeaderWrapper } from "./style.js"

export const HeaderComponent = ({ data }) => {

  const {
    home,
    away,
    hlogo,
    alogo,
    homeid,
    awayid
  } = data.matchSummary[0]

  return (
    <div data-testid="h2hheader">
      <HeaderWrapper>
        <span>
          <span><SmTLogo teamid={homeid} logo={hlogo} /></span>
          <span>
            <SubTitle>
              <TeamLink teamid={homeid} team={home}>
                {home}
              </TeamLink>
            </SubTitle>
          </span>
        </span>
        <span>
          <span>
            <SubTitle>
              <TeamLink teamid={awayid} team={away}>
                {away}
              </TeamLink>
            </SubTitle>
          </span>
          <span><SmTLogo teamid={awayid} logo={alogo} /></span>
        </span>
      </HeaderWrapper>
    </div>
  )
}
