import styled from "styled-components"

import { mq } from '@Theme'

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1rem 0rem;
  span{
    display: flex;
    align-items: center;
    padding: 0rem 0.5rem;
  }
  ${mq[1]}{
    flex-direction: column;
    span{
      margin: 0.25rem 0rem;
    }
    span:nth-child(2){
      flex-direction: row-reverse;
    }
  }
`
