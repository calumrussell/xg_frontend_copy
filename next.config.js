const path = require('path')

module.exports = {
  webpack: (config, { dev }) => {
    config.resolve.extensions = ['.js', '.jsx']
    config.resolve.alias['@Functions'] = path.resolve(__dirname, 'helpers/')
    config.resolve.alias['@Services'] = path.resolve(__dirname, 'services/')
    config.resolve.alias['@Redux'] = path.resolve(__dirname, 'services/redux')
    config.resolve.alias['@Components'] = path.resolve(__dirname, 'components/')
    config.resolve.alias['@Containers'] = path.resolve(__dirname, 'views/') 
    config.resolve.alias['@Theme$'] = path.resolve(__dirname, 'theme.js')
    config.resolve.alias['@Charts$'] = path.resolve(__dirname, 'components/chart/index.js')
    config.resolve.alias['@Routes$'] = path.resolve(__dirname, './routes.js' )
    config.module.rules.push({test: /\.js$/, loader: 'ify-loader'})
    return config
  }
}
