export const findCurrentDate = (arr) => {
  if (arr === undefined) return 0

  let jsdata = new Date() / 1000
  for (let i = 0; i < arr.length; i++) {
    if (arr[i]['starttime'] > jsdata) {
      return i
    }
  }
  return arr.length-10
}
