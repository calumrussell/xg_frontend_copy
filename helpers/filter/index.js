const funcBuilder = (stat, cond, condval, isInt) => {
  const valConversion = isInt ? parseInt(condval) : condval

  if (cond == "<") return d => d[stat] < valConversion
  else if (cond == ">") return d => d[stat] > valConversion
  else if (cond == "in") return d => d.includes(stat)
  else return d => d[stat] == valConversion
}

export const filterDS = (
  stat,
  cond,
  condval,
  id,
  isInt = true
) => ({
  stat,
  cond,
  condval,
  id,
  impl: funcBuilder(stat, cond, condval, isInt)
})

export const yearFilter = year => filterDS('year', "=", year, 0)
export const leagueFilter = league => filterDS('league', "=", league, 1, false)
export const isMainFilter = isMain => filterDS('ismain', '=', isMain, 2, false)
export const isGoalFilter = isGoal => filterDS('goal', '=', 1, true)
