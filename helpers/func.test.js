import { avgSort, pointSort } from './sort'
import { createAverage } from './calculate'
import { findCurrentDate } from './search'
import {
  dateConverter,
  headerConverter,
  stringConverter,
  titleConverter,
  stripDashes,
  toFixedAny
} from './transform'

test('averages are sorted correctly', () => {
  const dummy = [
    { "lookback": '2018' }, { "lookback": '3' }, { "lookback": '7' },
    { "lookback": '2015' }, { "lookback": '2016' }, { "lookback": '2017' },
    { "lookback": '5' }
  ]

  const expected = [
    { "lookback": '3' }, { "lookback": '5' }, { "lookback": '7' },
    { "lookback": '2018' }, { "lookback": '2017' }, { "lookback": '2016' },
    { "lookback": '2015' }
  ]

  expect(avgSort(dummy)).toEqual(expected)
})

test('create average calculation check', () => {
  const dummy = [
    { "test1": 3 }, { "test1": 4 }, { "test1": 7 }
  ]
  expect(createAverage(dummy)).toEqual({ "test1": 4.67 })
})

test('create average will cast on text numbers', () => {
  const dummy = [
    { "test1": '3' }, { "test1": '4' }, { "test1": '7' }
  ]
  expect(createAverage(dummy)).toEqual({ "test1": 4.67 })
})

test('create average text input returns first', () => {
  const dummy = [
    { "test1": 'test' }, { "test1": '4' }, { "test1": '7' }
  ]
  expect(createAverage(dummy)).toEqual({ "test1": NaN })
})

test('dates converted to readable', () => {
  expect(dateConverter(1488812013)).toBe("06/03/17")
})

test('find current date in list', () => {
  let now = new Date() / 1000
  const test = [
    { "starttime": now - 1000 }, { "starttime": now + 1000 }
  ]
  expect(findCurrentDate(test)).toBe(1)
})

test('headers converting, blank is unchanged', () => {
  expect(headerConverter('passtotal')).toBe("PAS")
  expect(headerConverter("None")).toBe("None")
})

test('point sort', () => {
  const test = [{ "points": 40 }, { "points": 35 }, { "points": 45 }]
  expect(pointSort(test)).toEqual([{ "points": 45 }, { "points": 40 }, { "points": 35 }])
})

test('string converter', () => {
  expect(stringConverter("test")).toBe("test")
})

test('title converter', () => {
  expect(titleConverter("passkey")).toBe("Key Pass")
  expect(titleConverter("None")).toBe("")
})

test('it returns a capitalised team name', () => {
  expect(stripDashes('man-utd')).toBe("Man Utd")
  expect(stripDashes('arsenal')).toBe("Arsenal")
})

test('it returns a tofixed num and an unchanged string', () => {
  expect(toFixedAny(6.500000, 2)).toBe("6.50")
  expect(toFixedAny('str', 2)).toEqual('str')
})
