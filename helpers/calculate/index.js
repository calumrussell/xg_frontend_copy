import round from 'lodash.round'

export const createAverage = (arr) => {
  const sum = createSum(arr)
  return Object.keys(sum).reduce((p, c) => {
    let avg = sum[c] / arr.length
    p[c] = round(avg, 2)
    return p
  }, {})
}

export const createSum = (arr) => {
  if(arr.length == 0){
    return {}
  }
  const nonAverageableCols = ['position', 'formatname', 'iswin']
  return Object.keys(arr[0]).filter(v => !nonAverageableCols.includes(v))
    .reduce((p, c) => {
      const sum = arr.reduce((prev, curr) => prev + parseFloat(curr[c]), 0)
      p[c] = round(sum, 2)
      return p
    }, {})
}

