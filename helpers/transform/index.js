import moment from "moment"

export const toFixedAny = (num, dec) => {
    return typeof num === "number" ? num.toFixed(dec) : num
}

export const stripDashes = (name) => {
  const capitalise = string => string.charAt(0).toUpperCase() + string.slice(1)
  return name.split("-").map(v => capitalise(v)).join(" ")
}

export const addRank = (data) => {
    return data.map((v, i) => {
        v['rank'] = i + 1
        return v
    })
}

export const stringConverter = (string) => {
    return string.replace(/\s+/g, '-');
}

export const dateConverter = (date, withHour) => {
  //Need to add param for hour option
  let jsdate = moment(date * 1000)
  return withHour
    ? jsdate.format('hh:mm, DD/MM/YY')
    : jsdate.format('DD/MM/YY')
}

export const dateObjConverter = date => new Date(date * 1000)

const decimalCategories = [
  'xg',
  'xa',
  'xgaverage',
  'dkp',
  'offrating',
  'defrating'
]

const percentCategories = [
  'passcrossshare',
  'passlongshare',
  'passshortshare',
  'passthroughballshare',
  'passintoboxshare',
  'passintofinalthirdshare',
  'passcrossaccuracy',
  'passlongaccuracy',
  'passaccuracy',
  'passshortaccuracy',
  'passthroughballaccuracy'
]

const stringCategories = [
  'position',
  'formatname',
  'team',
]

const isDecimal = category => decimalCategories.includes(category)
const isPercent = category => percentCategories.includes(category)
const isString = category => stringCategories.includes(category)
const convertDecimal = num => num.toFixed(2)
const convertPercent = num => (num * 100).toFixed(0)

export const numberConverter = (number, cat, isAverage) => {
  if (number == null) return number
  if (isString(cat)) return number
  if (isAverage) return isPercent(cat) ? convertPercent(number) : convertDecimal(number)
  else {
    if (isDecimal(cat)) return convertDecimal(number)
    if (isPercent(cat)) return convertPercent(number)
    return number
  }
}

export const supportedCategories = {
  name: { abbrev: 'N', long: 'Name' },
  formatname: { abbrev: 'N', long: 'Name' },
  year: { abbrev: 'Y', long: 'Year' },
  position: { abbrev: 'POS', long: 'Position' },
  dkposition: { abbrev: 'POS', long: 'DK Position' },
  starttime: { abbrev: 'D', long: 'Start Time' },
  team: { abbrev: 'T', long: 'Team' },
  opp: { abbrev: 'O', long: 'Opp' },
  minutesplayed: { abbrev: 'MIN', long: 'Minutes Played' },
  goalsfor: { abbrev: 'GF', long: 'Goals For' },
  goalsagainst: { abbrev: 'GA', long: 'Goals Against' },
  goalsdifference: { abbrev: 'GD', long: 'Goals Diff' },
  points: { abbrev: 'P', long: 'Points' },
  home: { abbrev: 'H', long: 'Home' },
  away: { abbrev: 'A', long: 'Away' },
  tournamentabbrev: { abbrev: 'L', long: 'League' },
  homeftscore: { abbrev: 'HS', long: 'Home Goals' },
  awayftscore: { abbrev: 'AS', long: 'Away Goals' },
  matchesplayed: { abbrev: 'MP', long: 'Matches Played' },
  iswin: { abbrev: 'WIN', long: 'Is Win' },
  passcrossaccurate: { abbrev: 'CAC', long: 'Accurate Crosses' },
  foulcommitted: { abbrev: 'FCM', long: 'Foul Committed' },
  shotpenaltymissed: { abbrev: 'MPN', long: 'Penalty Missed' },
  interceptiontotal: { abbrev: 'INT', long: 'Interceptions' },
  passaccurate: { abbrev: 'PAC', long: 'Accurate Pass' },
  goalown: { abbrev: 'OWN', long: 'Own Goal' },
  passlonginaccurate: { abbrev: 'LAC', long: 'Inaccurate Longball' },
  duelaerialwon: { abbrev: 'ADW', long: 'Aerial Duel Won' },
  assist: { abbrev: 'AST', long: 'Assist' },
  yellowcard: { abbrev: 'YCD', long: 'Yellow Card' },
  shottotal: { abbrev: 'SHO', long: 'Total Shots' },
  ishome: { abbrev: 'HOM', long: 'Is Home' },
  foulgiven: { abbrev: 'FGN', long: 'Foul Given' },
  redcard: { abbrev: 'RCD', long: 'Red Card' },
  clearancetotal: { abbrev: 'CLT', long: 'Clearance Total' },
  goalpenalty: { abbrev: 'PNS', long: 'Penalty Scored' },
  passcrossinaccurate: { abbrev: 'CIN', long: 'Inaccurate Cross' },
  passkey: { abbrev: 'KEY', long: 'Key Pass' },
  dribblewon: { abbrev: 'DWN', long: 'Dribble Won' },
  turnover: { abbrev: 'TRN', long: 'Turnover' },
  savetotal: { abbrev: 'KSV', long: 'Keeper Save Total' },
  passlongaccurate: { abbrev: 'LIN', long: 'Accurate Longball' },
  touchtotal: { abbrev: 'TCH', long: 'Total Touches' },
  passthroughballaccurate: { abbrev: 'TAC', long: 'Accurate Throughball' },
  goalnormal: { abbrev: 'GF', long: 'Goal scored in Normal Play' },
  shotontarget: { abbrev: 'SOT', long: 'Shot on target' },
  dispossessed: { abbrev: 'DIS', long: 'Dispossessed' },
  savepenalty: { abbrev: 'KSP', long: 'Keeper Saved Penalty' },
  yellowcardsecond: { abbrev: 'YCD', long: 'Second Yellow' },
  tacklewon: { abbrev: 'TKW', long: 'Tackle Won' },
  passcorner: { abbrev: 'CRN', long: 'Corner' },
  blockshottotal: { abbrev: 'BLK', long: 'Blocked Shot' },
  passinaccurate: { abbrev: 'PIN', long: 'Inaccurate Pass' },
  passthroughballinaccurate: { abbrev: 'TIN', long: 'Inaccurate Throughball' },
  dribblelost: { abbrev: 'DLS', long: 'Dribble Lost' },
  passshortaccurate: { abbrev: 'SAC', long: 'Short Pass Accurate' },
  errorleadstoshot: { abbrev: 'SER', long: 'Error leading to Shot' },
  foulpenaltyconceded: { abbrev: 'PCN', long: 'Penalty Conceded' },
  errorleadstogoal: { abbrev: 'GER', long: 'Error leading to Goal' },
  clearanceeffective: { abbrev: 'CLE', long: 'Successful Clearance' },
  passtotal: { abbrev: 'PAS', long: 'Total Passes' },
  tackletotal: { abbrev: 'TAC', long: 'Total Tackles' },
  passshorttotal: { abbrev: 'SRT', long: 'Total Short Passes' },
  passlongtotal: { abbrev: 'LON', long: 'Total Long Passes' },
  recovery: { abbrev: 'REC', long: 'Ball Recovery' },
  tacklelost: { abbrev: 'TKL', long: 'Tackle Lost' },
  isdraw: { abbrev: 'DRA', long: 'Is Draw' },
  blocksixyard: { abbrev: 'SIX', long: 'Block in Six Yard Area' },
  blockoutfielder: { abbrev: 'BLK', long: 'Outfielder Block' },
  passcrosstotal: { abbrev: 'CRS', long: 'Total Crosses' },
  touchinbox: { abbrev: 'BOX', long: 'Touches in Penalty Area' },
  touchleftwing: { abbrev: 'LFT', long: 'Touches in Opp Half Left Wing' },
  touchrightwing: { abbrev: 'RGT', long: 'Touches in Opp Half Right Wing' },
  touchcenter: { abbrev: 'CTR', long: 'Touches in Opp Half Center' },
  passforward: { abbrev: 'FWD', long: 'Pass towards Opp Goal' },
  passbackward: { abbrev: 'BCK', long: 'Pass away Opp Goal' },
  passforwardleft: { abbrev: 'FWDL', long: 'Pass towards Opp Goal Left Wing' },
  passforwardright: { abbrev: 'FWDR', long: 'Pass towards Opp Goal Right Wing' },
  shotbox: { abbrev: 'BOX', long: 'Shot from within Opp Penalty Area' },
  shotsixyard: { abbrev: 'SIX', long: 'Shot from within Opp Six Yard Box' },
  shotoutsidebox: { abbrev: 'LON', long: 'Shot from outside Opp Penatly Area' },
  shothead: { abbrev: 'HED', long: 'Shot using head' },
  shotsetpiece: { abbrev: 'SET', long: 'Shot from set piece' },
  shotmisshigh: { abbrev: 'MHI', long: 'Missed shot finishing over the bar' },
  shotmisslow: { abbrev: 'MLO', long: 'Missed shot finishing under the bar' },
  xg: { abbrev: 'XG', long: 'Expected Goals' },
  oppxg: { abbrev: 'OXG', long: 'Opp Expected Goals' },
  dkp: { abbrev: 'DKP', long: 'DraftKings Fantasy Points' },
  goalsfirsthalf: { abbrev: 'FHA', long: 'Goal scored in first half' },
  goalssecondhalf: { abbrev: 'SHA', long: 'Goal scored in second half' },
  goalsfirstten: { abbrev: 'F10', long: 'Goal scored in first ten minutes' },
  goalslastten: { abbrev: 'L10', long: 'Goal scored in last ten minutes' },
  goalsinjurytime: { abbrev: 'INJ', long: 'Goal scored after 90 minutes' },
  goalsfirstq: { abbrev: 'Q1', long: 'Goal scored in first quarter' },
  goalssecondq: { abbrev: 'Q2', long: 'Goal scored in second quarer' },
  goalsthirdq: { abbrev: 'Q3', long: 'Goal scored in third quarter' },
  goalsfourthq: { abbrev: 'Q4', long: 'Goal scored in fourth quarter' },
  goalsfor: { abbrev: 'GF', long: 'Goals For' },
  goalsagainst: { abbrev: 'GA', long: 'Goals Against' },
  cleansheet: { abbrev: 'CS', long: 'Clean Sheet' },
  fts: { abbrev: 'FTS', long: 'First to score' },
  btts: { abbrev: 'BTS', long: 'Both to score' },
  passthroughballtotal: { abbrev: 'THR', long: 'Total Throughballs' },
  passshortinaccurate: { abbrev: 'SIN', long: 'Inaccurate short passes' },
  passdistance: { abbrev: 'PD', long: 'Total pass distance' },
  avgpassdistance: { abbrev: 'APD', long: 'Avg pass distance' },
  passintobox: { abbrev: 'BOX', long: 'Passes into opp penalty area' },
  passintofinalthird: { abbrev: 'FIN', long: 'Passes into opp final third' },
  passcrossaccuracy: { abbrev: 'CRS%', long: 'Percentage of accurate crosses' },
  passlongaccuracy: { abbrev: 'LNG%', long: 'Percentage of accurate long passes' },
  passaccuracy: { abbrev: 'PAS%', long: 'Percentage of accurate passes' },
  passshortaccuracy: { abbrev: 'SRT%', long: 'Percentage of accurate short passes' },
  passthroughballaccuracy: { abbrev: 'THR%', long: 'Percentage of accurate throughballs' },
  xa: { abbrev: 'XA', long: 'Expected assists' },
  dribblefendoff: { abbrev: 'FND', long: 'Dribble with successful fend off' },
  dribbletackled: { abbrev: 'TAC', long: 'Dribble with attempted tackle' },
  dribbletotal: { abbrev: 'TOT', long: 'Total dribbles' },
  duelaerialtotal: { abbrev: 'ADT', long: 'Total aerial duel' },
  passcrossshare: { abbrev: 'CRS%', long: 'Crosses as share of total passes' },
  passlongshare: { abbrev: 'LNG%', long: 'Long passes as share of total passes' },
  passshortshare: { abbrev: 'SRT%', long: 'Short passes as share of total passes' },
  passthroughballshare: { abbrev: 'THR%', long: 'Throughballs as share of total passes' },
  passintoboxshare: { abbrev: 'BOX%', long: 'Passes into box as share of total passes' },
  passintofinalthirdshare: { abbrev: 'FIN%', long: 'Passes into final third as share of total passes' },
  xgaverage: { abbrev: 'xGAv', long: 'Average xG' },
  clearanceeffectiveadj: { abbrev: 'aCLE', long: 'Posession-adjusted effective clearances' },
  clearancetotaladj: { abbrev: 'aCLT', long: 'Posession-adjutsed total clearances' },
  tacklewonadj: { abbrev: 'aTKW', long: 'Posession-adjusted tackles won' },
  tackletotaladj: { abbrev: 'aTAC', long: 'Posession-adjusted total tackles' },
  offrating: { abbrev: 'OR', long: 'Offensive Rating' },
  defrating: { abbrev: 'DR', long: 'Defensive Rating' },
  goalsixyard: { abbrev: 'GSY', long: 'Goal scored from sixyard box'},
  goalbox: { abbrev: 'GBX', long: 'Goal scored from inside box'},
  goaloutsidebox: { abbrev: 'GOBX', long: 'Goal scored from outside box'},
  goalsetpiece: { abbrev: 'GSP', long: 'Goal scored from set piece'},
  goalhead: { abbrev: 'GHD', long: 'Goal scored by header'},
  goalfastbreak: { abbrev: 'GFB', long: 'Goal scored from a fast break'},
  shotfastbreak: { abbrev: 'SFB', long: 'Shot from a fast break'},
  goalopenplay: { abbrev: 'GOP', long: 'Goal scored from open play'},
  shotopenplay: { abbrev: 'SOP', long: 'Shot from open play'},
  goalbigchance: { abbrev: 'GBC', long: 'Goal scored from big chance'},
  shotbigchance: { abbrev: 'SBC', long: 'Shot from big chance'},
  shotindividualplay: { abbrev: 'SIP', long: 'Shot from individual play'},
  goalindividualplay: { abbrev: 'GIP', long: 'Goal from individual play'},
  cornerwon: { abbrev: 'CNRW', long: 'Offsensive credit for action that result in a corner for team'},
  cornerlost: { abbrev: 'CNRL', long: 'Defensive credit for action that resulted in a corner against team'},
  cornertaken: { abbrev: 'CNRT', long: 'Corner'},
  cornerinswinger: { abbrev: 'CNRIN', long: 'Corner swung towards goal'},
  corneroutswinger: { abbrev: 'CNROT', long: 'Corner swung away from goal'},
  cornerstraight: { abbrev: 'CNRS8', long: 'Corner flies straight'},
  goalfromcorner: { abbrev: 'GFC', long: 'Goal scored from corner'},
  shotfromcorner: { abbrev: 'SFC', long: 'Shot from corner'},
  shotoneonone: { abbrev: 'SONE', long: 'Shot from one-on-one situation'},
  goaloneonone: { abbrev: 'GONE', long: 'Goal from one-on-one situation'},
  shotsolorun: { abbrev: 'SSLO', long: 'Shot from solo run'},
  goalsolorun: { abbrev: 'GSLO', long: 'Goal from solo run'},
  shotfirsttouch: { abbrev: 'SFT', long: 'Shot taken with first touch'},
  goalfirsttouch: { abbrev: 'GFT', long: 'Goal scored with first touch'},
  passputthrough: { abbrev: 'PPT', long: 'Pass put through'},
  shotputthrough: { abbrev: 'SPT', long: 'Shot resulting from put through'},
  goalputthrough: { abbrev: 'GPT', long: 'Goal resulting from put through'},
  foulgiventackle: { abbrev: 'FGT', long: 'Foul conceded due to tackle'},
  foulgivenaerial: { abbrev: 'FGA', long: 'Foul conceded due to aerial'},
  goalvolley: { abbrev: 'GVOL', long: 'Goal scored from volley'},
  goaloverhead: { abbrev: 'GOVR', long: 'Goal scored from overhead'},
  goalstrong: { abbrev: 'GSTG', long: 'Goal from strong power shot'},
  goalweak: { abbrev: 'GWK', long: 'Goal from weak power shot'},
  goallob: { abbrev: 'GLOB', long: 'Goal from lob'},
  shotvolley: { abbrev: 'SVOL', long: 'Shot from volley'},
  shotoverhead: { abbrev: 'SOVR', long: 'Shot from overhead'},
  shotstrong: { abbrev: 'SSTG', long: 'Strong power shot'},
  shotweak: { abbrev: 'SWK', long: 'Strong weak shot'},
  shotlob: { abbrev: 'SLOB', long: 'Lobbed shot'},
  passblocked: { abbrev: 'PBLK', long: 'Attempted pass that was blocked'},
  passcrossblocked: { abbrev: 'PCBLK', long: 'Attempted cross that was blocked'},
  blockpass: { abbrev: 'BLKP', long: 'Blocked pass'},
  dribbleoverrun: { abbrev: 'DORN', long: 'Overran dribble and lost control'},
  passswitchofplay: { abbrev: 'PSWT', long: 'Pass that switches play'},
  passchipped: { abbrev: 'PCHI', long: 'Chipped pass'},
  passlayoff: { abbrev: 'PLAY', long: 'Layoff pass'},
  passlaunched: { abbrev: 'PLAU', long: 'Launched pass'},
  goalassisted: { abbrev: 'GAST', long: 'Goal with assist'},
  shotassisted: { abbrev: 'SAST', long: 'Shot with assist (technically, keypass if not scored)'},
  goalassistedintentional: { abbrev: 'GASTI', long: 'Goal with intentional assist'},
  shotassistedintentional: { abbrev: 'SASTI', long: 'Shot with intentional assist'},
  assistintentional: { abbrev: 'ASTI', long: 'Intentional assist'},
  shotpenalty: { abbrev: 'SPEN', long: 'Shot from penalty'},
  passpullback: { abbrev: 'PPUL', long: 'Pull back pass'},
  blockgoalline: { abbrev: 'BLKG', long: 'Block from goal line'},
  duelwon: { abbrev: 'DWON', long: 'Duel won'},
  duellost: { abbrev: 'DLOST', long: 'Duel lost'},
  gkhoof: { abbrev: 'GKHF', long: 'GK clears ball from ground'},
  gkkickfromhands: { abbrev: 'GKKI', long: 'GK clears ball from hands'},
  savefingertip: { abbrev: 'STIP', long: 'Fingertip save'},
  savecaught: { abbrev: 'SCAU', long: 'Caught save'},
  savecollected: { abbrev: 'SCOL', long: 'Collected save'},
  savestanding: { abbrev: 'SSTA', long: 'Standing save'},
  savediving: { abbrev: 'SDIV', long: 'Diving save'},
  savestooping: { abbrev: 'SSTO', long: 'Stooping save'},
  savereaching: { abbrev: 'SREA', long: 'Reaching save'},
  savehands: { abbrev: 'SHAN', long: 'Save with hands'},
  savefeet: { abbrev: 'SFT', long: 'Save with feet'},
  saveparriedsafe: { abbrev: 'SSAF', long: 'Save that is parried safe'},
  saveparrieddanger: { abbrev: 'SDAN', long: 'Save that is parried into danger'},
}

const unCapitalise = string => string.charAt(0).toLowerCase() + string.slice(1)

export const headerConverter = cat => {
  if (cat in supportedCategories == false) return cat
  if (cat == "") return cat

  return cat.slice(0, 3) == "opp"
    ? `o${supportedCategories[unCapitalise(cat.slice(3))]['abbrev']}`
    : supportedCategories[cat]['abbrev']
}

export const titleConverter = cat => {
  if ( cat in supportedCategories == false) return ""
  return cat.slice(0, 3) == "opp" ?
    cat :
    supportedCategories[cat]['long']
}
