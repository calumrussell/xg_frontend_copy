import sortBy from 'lodash.sortby'
import orderBy from 'lodash.orderby'

export const avgSort = (arr) => {
  const order = {
    '3': 1,
    '5': 2,
    '7': 3,
    '2018': 4,
    '2017': 5,
    '2016': 6,
    '2015': 7
  }
  return sortBy(arr, (el) => {
    return order[el.lookback]
  })
}

export const pointSort = (arr) => {
  return orderBy(arr, ['points', 'goalsdifference', 'goalsfor'], ['desc', 'desc', 'desc'])
}

//sort on left
export const luSort = (arr) => {
  if(arr==null || arr==undefined){
    return arr
  }
  const order = {
    GK: 1,
    DR: 2,
    DC: 3,
    DL: 4,
    DMR: 5,
    DMC: 6,
    DML: 7,
    MR: 8,
    MC: 9,
    ML: 10,
    AMR: 11,
    AMC: 12,
    AML: 13,
    FWR: 14,
    FW: 15,
    FWL: 16,
    CF:17,
    SUB: 18
  }
  return sortBy(arr, (el) => {
    return order[el.position]
  })
}

//sort on right 
export const luSortRight = (arr) => {
  if(arr==null || arr==undefined){
    return arr
  }
  const order = {
    GK: 1,
    DR: 4,
    DC: 3,
    DL: 2,
    DMR: 7,
    DMC: 6,
    DML: 5,
    MR: 10,
    MC: 9,
    ML: 8,
    AMR: 13,
    AMC: 12,
    AML: 11,
    FWR: 16,
    FW: 15,
    FWL: 14,
    CF:17,
    SUB: 18
  }
  return sortBy(arr, (el) => {
    return order[el.position]
  })
}
