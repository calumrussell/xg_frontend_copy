import { goalsCalcs } from './index.js'

const {
  cleanSheetCount, over15Count, over25Count, over35Count,
  over45Count, wrapper, bttsCount, ftsCount
} = goalsCalcs

const matches = [
  {
    "goalsfor": 4, "goalsagainst": 1, "goalslastten": 1, "goalsfirstten": 1, "goalsfirstq": 1,
    "goalssecondq": 1, "goalsthirdq": 1, "goalsfourthq": 1, "goalsfirsthalf": 2, "goalssecondhalf": 2,
    "goalsinjurytime": 0
  },
  {
    "goalsfor": 10, "goalsagainst": 1, "goalslastten": 2, "goalsfirstten": 2, "goalsfirstq": 2,
    "goalssecondq": 2, "goalsthirdq": 2, "goalsfourthq": 4, "goalsfirsthalf": 4, "goalssecondhalf": 6,
    "goalsinjurytime": 0
  },
  {
    "goalsfor": 0, "goalsagainst": 0, "goalslastten": 0, "goalsfirstten": 0, "goalsfirstq": 0,
    "goalssecondq": 0, "goalsthirdq": 0, "goalsfourthq": 0, "goalsfirsthalf": 0, "goalssecondhalf": 0,
    "goalsinjurytime": 0
  }
]

const justGoals = Array.from(Array(5).keys()).map(v => {
  return { "goalsfor": v, "goalsagainst": v }
})

test('return structure', () => {
  expect(wrapper(matches)).toHaveProperty('Count')
  expect(wrapper(matches)).toHaveProperty('Scoring')
  expect(wrapper(matches)).toHaveProperty('Timing1')
  expect(wrapper(matches)).toHaveProperty('Timing2')
})

test('cleansheet calc', () => {
  expect(cleanSheetCount(justGoals)).toBe(0.2);
})

test('over15count calc', () => {
  expect(over15Count(justGoals)).toBe(0.8);
})

test('over25count calc', () => {
  expect(over25Count(justGoals)).toBe(0.6);
})

test('over35count calc', () => {
  expect(over35Count(justGoals)).toBe(0.6);
})

test('over45count calc', () => {
  expect(over45Count(justGoals)).toBe(0.4);
})

test('btts calc', () => {
  expect(bttsCount(justGoals)).toBe(0.8);
})

test('fts calc', () => {
  expect(ftsCount(justGoals)).toBe(0.2);
})

test('timings calc', () => {
  const timing1 = [{ "stat": "1st Ten", "value": 0.21428571428571427 },
  { "stat": "Last Ten", "value": 0.21428571428571427 },
  { "stat": "1st Half", "value": 0.42857142857142855 },
  { "stat": "2nd Half", "value": 0.5714285714285714 },
  { "stat": "Injury Time", "value": 0 }]

  const timing2 = [{ "stat": "1st Q", "value": 0.21428571428571427 },
  { "stat": "2nd Q", "value": 0.21428571428571427 },
  { "stat": "3rd Q", "value": 0.21428571428571427 },
  { "stat": "4th Q", "value": 0.35714285714285715 }]
  
  expect(wrapper(matches)['Timing1']).toEqual(timing1)
  expect(wrapper(matches)['Timing2']).toEqual(timing2)
})
