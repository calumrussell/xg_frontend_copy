const cleanSheetCount = (matches) => {
  return matches.reduce((prev, curr) => curr['goalsagainst'] == 0 ? prev += 1 : prev, 0) / matches.length
}

const over15Count = (matches) => {
  return matches.reduce((prev, curr) => (curr['goalsfor'] + curr['goalsagainst']) > 1.5 ? prev += 1 : prev, 0) / matches.length
}

const over25Count = (matches) => {
  return matches.reduce((prev, curr) => curr['goalsfor'] + curr['goalsagainst'] > 2.5 ? prev += 1 : prev, 0) / matches.length 
}

const over35Count = (matches) => {
  return matches.reduce((prev, curr) => curr['goalsfor'] + curr['goalsagainst'] > 3.5 ? prev += 1 : prev, 0) / matches.length
}

const over45Count = (matches) => {
  return matches.reduce((prev, curr) => curr['goalsfor'] + curr['goalsagainst'] > 4.5 ? prev += 1 : prev, 0) / matches.length 
}

const bttsCount = (matches) => {
  return matches.reduce((prev, curr) => curr['goalsfor'] != 0 && curr['goalsagainst'] != 0 ? prev += 1 : prev, 0) / matches.length
}

const ftsCount = (matches) => {
  return matches.reduce((prev, curr) => curr['goalsfor'] == 0 ? prev += 1 : prev, 0) / matches.length
}

const count = (matches, cat, totalgoals) => {
  return matches.reduce((prev, curr) => prev + curr[cat], 0) / totalgoals
}

const wrapper = (matches) => {
  const totalgoals = matches.reduce((prev, curr) => prev + curr['goalsfor'], 0)
  return {
    "Count": [
      { stat: "Over 1.5", value: over15Count(matches) },
      { stat: "Over 2.5", value: over25Count(matches) },
      { stat: "Over 3.5", value: over35Count(matches) },
      { stat: "Over 4.5", value: over45Count(matches) }
    ],
    "Scoring": [
      { stat: "Clean Sheet", value: cleanSheetCount(matches) },
      { stat: "BTTS", value: bttsCount(matches) },
      { stat: "FTS", value: ftsCount(matches) }
    ],
    "Timing1": [
      { stat: "1st Ten", value: count(matches, 'goalsfirstten', totalgoals) },
      { stat: "Last Ten", value: count(matches, 'goalslastten', totalgoals) },
      { stat: "1st Half", value: count(matches, 'goalsfirsthalf', totalgoals) },
      { stat: "2nd Half", value: count(matches, 'goalssecondhalf', totalgoals) },
      { stat: "Injury Time", value: count(matches, 'goalsinjurytime', totalgoals) }
    ],
    "Timing2": [
      { stat: "1st Q", value: count(matches, 'goalsfirstq', totalgoals) },
      { stat: "2nd Q", value: count(matches, 'goalssecondq', totalgoals) },
      { stat: "3rd Q", value: count(matches, 'goalsthirdq', totalgoals) },
      { stat: "4th Q", value: count(matches, 'goalsfourthq', totalgoals) }
    ]
  }
}

export const goalsCalcs = {
  wrapper,
  count,
  ftsCount,
  bttsCount,
  cleanSheetCount,
  over15Count,
  over25Count,
  over35Count,
  over45Count
}
