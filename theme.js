const breakpoints = [576, 768, 992, 1200]

export const mq = breakpoints.map(bp => `@media (max-width: ${bp}px)`)

export const textFont = 'Open Sans'
export const titleFont = 'Roboto'

export const colorScheme = {
  neutral: '#eceef2',
  bluegrey: {
    50: "#E3F2FD",
    100: "#BBDEFB",
    200: "#90CAF9",
    300: "#64B5F6",
    400: "#42A5F5",
    500: "#2196F3",
    600: "#1E88E5",
    700: "#1976D2",
    800: "#1565C0",
    900: "#0D47A1",
    A100: '#82B1FF',
    A200: '#448AFF',
    A400: '#2979FF',
    A700: '#2962FF'
  },
  grey: {
    50: '#FAFAFA',
    100: '#F5F5F5',
    200: '#EEEEEE',
    300: '#E0E0E0',
    400: '#BDBDBD',
    500: '#9E9E9E',
    600: '#757575',
    700: '#616161',
    800: '#424242',
    900: '#212121'
  },
  green: {
    50: "#e8f5e9",
    100: "#c8e6c9",
    200: "#a5d6a7",
    300: "#81c784",
    400: "#66bb6a",
    500: "#4caf50",
    600: "#43a047",
    700: "#388e3c",
    800: "#2e7d32",
    900: "#1b5e20"
  },
  success: '#32936F',
  failure: '#F45B69',
  neutral: '#62929E',
  warning: '#FFE74C',
  orange: '#FF8300',
  chart: ["#5f86b7", "#9fd841", "#d85ee1", "#94d2cf", "#9671d2", "#699023"]
}

export const chartColourPicker = position => colorScheme.chart[position % colorScheme.chart.length]
